﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class DynamicObstructionOcclusion : MonoBehaviour
{
    public GameObject emitter;
    public int listenerIndex;
    public bool linkObstructionAndOcclusion;

    private Vector3 originPoint;
    private Transform playerPos;
    private StareHandler playerStareHandler;

    private float maxAtt;

    private float distToPlayer1;
    private float distToPlayer2;

    private Vector3 targetDirection1;
    private Vector3 targetDirection2;

    private Vector3 target1;
    private Vector3 target2;

    public float fadeTimeTarget = 0.6f;
    private float valueTarget;
    private float currentTotalValue;
    private float currenValue1;
    private float currenValue2;
    private float valueChange;
    private float currentTime;

    private float obstructionValue;
    private float occlusionValue;

    public float refreshTime = 0.1f;

    private bool isObstructed1;
    private bool isObstructed2;

    private bool matchHasStarted;
    private bool canCalculate;

    public float xOffset = 0.8f;
    public float zOffset = 0.8f;

    private void Awake()
    {
        GameMode.OnStartOfMatch += OnMatchStart;
    }

    private void OnDisable()
    {
        GameMode.OnStartOfMatch -= OnMatchStart;
    }

    private void OnMatchStart(object sender, EventArgs eventArgs)
    {
        if (emitter != null)
            gameObject.transform.position = emitter.transform.position;
        else
            emitter = gameObject;

        if (AudioManager.instance.GetPlayerAudioManager(listenerIndex) != null)
        {
            playerPos = AudioManager.instance.GetPlayerAudioManager(listenerIndex).wwiseListener.camera.transform;
            playerStareHandler = AudioManager.instance.GetPlayerAudioManager(listenerIndex).player.stareHandler;
            matchHasStarted = true;
        }
    }

    private void Start()
    {
        valueTarget = 1f;
        currentTotalValue = 0f;
        //print("total refresh" + (fadeTimeTarget / refreshTime));
        valueChange = valueTarget / (fadeTimeTarget / refreshTime);
        //print(valueChange);
    }

    private void Update()
    {
        if (matchHasStarted)
        {
            /*
            target1 = playerPos.TransformPoint(xOffset, 0, zOffset);
            target2 = playerPos.TransformPoint(-xOffset, 0, zOffset);

            originPoint = transform.position;

            targetDirection1 = target1 - transform.position;
            targetDirection2 = target2 - transform.position;

            distToPlayer1 = Vector3.Distance(target1, originPoint);
            distToPlayer2 = Vector3.Distance(target2, originPoint);
            */

            maxAtt = AkSoundEngine.GetMaxRadius(emitter) * 0.8f;

            if (distToPlayer1 <= maxAtt)
            {
                canCalculate = true;
                //AkSoundEngine.SetObjectObstructionAndOcclusion(emitter, AudioManager.instance.Listeners[0], currentTotalValue, currentTotalValue);
            }
        }
    }



    public void CheckObstruction()
    {
        if (canCalculate)
        {
            target1 = playerPos.TransformPoint(xOffset, 0, zOffset);
            target2 = playerPos.TransformPoint(-xOffset, 0, zOffset);

            originPoint = transform.position;

            targetDirection1 = target1 - transform.position;
            targetDirection2 = target2 - transform.position;

            distToPlayer1 = Vector3.Distance(target1, originPoint);
            distToPlayer2 = Vector3.Distance(target2, originPoint);
            obstructionValue = 0;

            RaycastHit hitInfo1;
            RaycastHit hitInfo2;

            //Rayon 1
            if (Physics.Raycast(originPoint, targetDirection1, out hitInfo1, distToPlayer1, playerStareHandler.mask, QueryTriggerInteraction.Ignore))
            {
                isObstructed1 = true;
            }
            else isObstructed1 = false;

            //Rayon 2
            if (Physics.Raycast(originPoint, targetDirection2, out hitInfo2, distToPlayer2, playerStareHandler.mask, QueryTriggerInteraction.Ignore))
            {
                isObstructed2 = true;
            }
            else isObstructed2 = false;

            //Obstruction 1
            if (isObstructed1)
            {
                Debug.DrawRay(originPoint, targetDirection1, Color.red, refreshTime);
                obstructionValue += 0.5f;
            }
            else
            {
                Debug.DrawRay(originPoint, targetDirection1, Color.blue, refreshTime);
            }

            //Obstruction 2
            if (isObstructed2)
            {
                Debug.DrawRay(originPoint, targetDirection2, Color.red, refreshTime);
                obstructionValue += 0.5f;
            }
            else
            {
                Debug.DrawRay(originPoint, targetDirection2, Color.blue, refreshTime);
            }

            CheckOcclusion();
            UpdateObstructionOcclusion();
        }
    }

    public void CheckOcclusion()
    {
        if (canCalculate && linkObstructionAndOcclusion)
        {
            occlusionValue = obstructionValue;
        }
    }

    private void UpdateObstructionOcclusion()
    {
        AkSoundEngine.SetObjectObstructionAndOcclusion(emitter, AudioManager.instance.Listeners[listenerIndex], obstructionValue, occlusionValue);
    }










    private void Fade()
    {
        currentTime += Time.deltaTime;
        if (currentTime > refreshTime)
        {
            currentTime = 0;

            RaycastHit hitInfo1;
            RaycastHit hitInfo2;

            //Rayon 1
            if (Physics.Raycast(originPoint, targetDirection1, out hitInfo1, distToPlayer1, playerStareHandler.mask, QueryTriggerInteraction.Ignore))
            {
                isObstructed1 = true;
            }
            else isObstructed1 = false;

            //Rayon 2
            if (Physics.Raycast(originPoint, targetDirection2, out hitInfo2, distToPlayer2, playerStareHandler.mask, QueryTriggerInteraction.Ignore))
            {
                isObstructed2 = true;
            }
            else isObstructed2 = false;

            //Obstruction 1
            if (!isObstructed1)
            {
                Debug.DrawRay(originPoint, targetDirection1, Color.blue, refreshTime);
                if (currenValue1 > 0)
                {
                    currenValue1 -= valueChange;
                    currenValue1 = Mathf.Clamp(currenValue1, 0, 0.5f);
                }
            }
            else
            {
                Debug.DrawRay(originPoint, targetDirection1, Color.red, refreshTime);
                if (currenValue1 < 0.5f)
                {
                    currenValue1 += valueChange;
                    currenValue1 = Mathf.Clamp(currenValue1, 0, 0.5f);
                }
            }

            //Obstruction 2
            if (!isObstructed2)
            {
                Debug.DrawRay(originPoint, targetDirection2, Color.blue, refreshTime);
                if (currenValue2 > 0)
                {
                    currenValue2 -= valueChange;
                    currenValue2 = Mathf.Clamp(currenValue2, 0, 0.5f);
                }
            }
            else
            {
                Debug.DrawRay(originPoint, targetDirection2, Color.red, refreshTime);
                if (currenValue2 < 0.5f)
                {
                    currenValue2 += valueChange;
                    currenValue2 = Mathf.Clamp(currenValue2, 0, 0.5f);
                }
            }
        }
        currentTotalValue = currenValue1 + currenValue2;
    }
}
