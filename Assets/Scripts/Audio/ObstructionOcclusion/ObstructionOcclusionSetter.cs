﻿using System;
using UnityEngine;
using System.Collections;

public class ObstructionOcclusionSetter : MonoBehaviour
{
    private AudioEmittersPool audioEmittersPool;
    private GameObject[] emitters;
    private bool startingObstruction;
    private bool startingOcclusion;
    private float strength;

    private ObstructionOcclusionReference reference;
    public int listenerIndex;

    private float fadeTimeTarget;
    private float valueChange;
    private float valueTarget;
    private float refreshTime = 0.05f;
    private float timer;

    private float obstructionValue;
    private float occlusionValue;
    
    private bool isObstructed;
    private bool isOccluded;

    private bool canCalculate;

    private void Awake()
    {
        reference = gameObject.GetComponent<ObstructionOcclusionReference>();
        reference.obstructionOcclusionSetters[listenerIndex] = this;
        fadeTimeTarget = reference.fadeTimeTarget;
        audioEmittersPool = reference.audioEmittersPool;
        startingObstruction = reference.startingObstruction;
        startingOcclusion = reference.startingOcclusion;
        strength = reference.strength;

        isObstructed = startingObstruction;
        isOccluded = startingOcclusion;
        //print(emitters[0] + " isObstructed: " + isObstructed);
        valueTarget = 1f;
        obstructionValue = 0f;
        //print("total refresh" + (fadeTimeTarget / refreshTime));
        valueChange = valueTarget / (fadeTimeTarget / refreshTime);
        //print(valueChange);

        emitters = audioEmittersPool.emittersPool;

        GameMode.OnStartOfMatch += OnStartOfMatch;
    }

    private void OnDisable()
    {
        GameMode.OnStartOfMatch -= OnStartOfMatch;
    }

    private void OnStartOfMatch(object sender, System.EventArgs e)
    {
        canCalculate = true;

        if (isObstructed)
        {
            obstructionValue = 1;
        }
        else
        {
            obstructionValue = 0;
        }

        if (isOccluded)
        {
            occlusionValue = 1;
        }
        else
        {
            occlusionValue = 0;
        }

        for (int i = 0; i < emitters.Length; i++)
        {
            if (AudioManager.instance.Listeners[listenerIndex])
                AkSoundEngine.SetObjectObstructionAndOcclusion(emitters[i], AudioManager.instance.Listeners[listenerIndex], obstructionValue * strength, occlusionValue * strength);
        }
    }

    private void Start()
    {
        if (emitters == null)
        {
            emitters.SetValue(gameObject,0);
        }
    }

    private void Update()
    {
        if (canCalculate)
        {
            if ((isObstructed && obstructionValue < 1) || (!isObstructed && obstructionValue > 0))
            {
                UpdateObstructionValue();
            }

            if ((isOccluded && occlusionValue < 1) || (!isObstructed && occlusionValue > 0))
            {
                UpdateOcclusionValue();
            }

            for (int i = 0; i < emitters.Length; i++)
            {
                if (AudioManager.instance.Listeners[listenerIndex])
                    AkSoundEngine.SetObjectObstructionAndOcclusion(emitters[i], AudioManager.instance.Listeners[listenerIndex], obstructionValue * strength, occlusionValue * strength);
            }
        }
    }

    private void UpdateObstructionValue()
    {
        timer += Time.deltaTime;
        if (timer > refreshTime)
        {
            timer = 0;
            if (!isObstructed)
            {
                if (obstructionValue > 0)
                {
                    obstructionValue -= valueChange;
                    obstructionValue = Mathf.Clamp(obstructionValue, 0, 1f);
                }
            }
            else
            {
                if (obstructionValue < 1f)
                {
                    obstructionValue += valueChange;
                    obstructionValue = Mathf.Clamp(obstructionValue, 0, 1f);
                }
            }
        }
    }

    private void UpdateOcclusionValue()
    {
        timer += Time.deltaTime;
        if (timer > refreshTime)
        {
            timer = 0;
            if (!isOccluded)
            {
                if (occlusionValue > 0)
                {
                    occlusionValue -= valueChange;
                    occlusionValue = Mathf.Clamp(occlusionValue, 0, 1f);
                }
            }
            else
            {
                if (occlusionValue < 1f)
                {
                    occlusionValue += valueChange;
                    occlusionValue = Mathf.Clamp(occlusionValue, 0, 1f);
                }
            }
        }
    }

    public void CheckObstruction(PlayerAudioManager playerAudioManager)
    {
        if (playerAudioManager.player.GetPlayerIndex() == listenerIndex)
        {
            if (!isObstructed)
            {
                isObstructed = true;
                //print(emitters[0] + " obstructed");
            }
            else
            {
                isObstructed = false;
                //print(emitters[0] + " no more obstructed");
            }
        }
    }

    public void CheckOcclusion(PlayerAudioManager playerAudioManager)
    {
        if (playerAudioManager.player.GetPlayerIndex() == listenerIndex)
        {
            if (!isOccluded)
            {
                isOccluded = true;
                //print(emitters[0] + " occluded");
            }
            else
            {
                isOccluded = false;
                //print(emitters[0] + " no more occluded");
            }
        }
    }

    public void ResetObstruction(int playerIndex)
    {
        if (playerIndex == listenerIndex)
        {
            isObstructed = startingObstruction;
            //print("Obstruction reseted");
        }
    }

    public void ResetOcclusion(int playerIndex)
    {
        if (playerIndex == listenerIndex)
        {
            isOccluded = startingOcclusion;
            //print("Occlusion reseted");
        }
    }
}
