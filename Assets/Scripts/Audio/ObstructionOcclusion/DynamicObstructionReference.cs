﻿using UnityEngine;
using System.Collections;

public class DynamicObstructionReference : MonoBehaviour
{

    public DynamicObstructionOcclusion[] obstructorsList;

    private void Awake()
    {
        obstructorsList = gameObject.GetComponents<DynamicObstructionOcclusion>();
    }

    public void CheckObstruction()
    {
        foreach(DynamicObstructionOcclusion obstructor in obstructorsList)
        {
            obstructor.CheckObstruction();
        }
    }
}
