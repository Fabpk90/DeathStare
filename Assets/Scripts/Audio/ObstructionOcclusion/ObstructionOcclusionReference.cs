﻿using UnityEngine;
using System.Collections;
using System;

public class ObstructionOcclusionReference : MonoBehaviour
{
    public ObstructionOcclusionSetter[] obstructionOcclusionSetters;

    [Header("Settings")]
    public AudioEmittersPool audioEmittersPool;
    public bool startingObstruction;
    public bool startingOcclusion;
    public float fadeTimeTarget = 0.6f;
    public float strength = 1f;

    private void Awake()
    {
        TestMode.OnRespawn += ResetObstruction;
        TestMode.OnRespawn += ResetOcclusion;
    }

    public void CheckObstruction(PlayerAudioManager playerAudioManager)
    {
        for (int i=0; i<obstructionOcclusionSetters.Length; i++)
        {
            obstructionOcclusionSetters[i].CheckObstruction(playerAudioManager);
        }
    }

    public void CheckOcclusion(PlayerAudioManager playerAudioManager)
    {
        for (int i = 0; i < obstructionOcclusionSetters.Length; i++)
        {
            obstructionOcclusionSetters[i].CheckOcclusion(playerAudioManager);
        }
    }

    private void ResetObstruction(object sender, int playerIndex)
    {
        for (int i = 0; i < obstructionOcclusionSetters.Length; i++)
        {
            obstructionOcclusionSetters[i].ResetObstruction(playerIndex);
        }
    }

    private void ResetOcclusion(object sender, int playerIndex)
    {
        for (int i = 0; i < obstructionOcclusionSetters.Length; i++)
        {
            obstructionOcclusionSetters[i].ResetOcclusion(playerIndex);
        }
    }
}
