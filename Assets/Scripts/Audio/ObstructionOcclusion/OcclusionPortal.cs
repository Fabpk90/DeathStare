﻿using UnityEngine;
using System.Collections;

public class OcclusionPortal : MonoBehaviour
{
    public ObstructionOcclusionReference occlusionReference;
    public bool onTriggerStay;

    private void OnTriggerEnter(Collider other)
    {
        PlayerAudioManager playerAudioManager = other.gameObject.GetComponent<PlayerAudioManager>();
        if (playerAudioManager)
        {
            //print("Player " + playerAudioManager.player.GetPlayerIndex() + "enter");
            occlusionReference.CheckOcclusion(playerAudioManager);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (onTriggerStay)
        {
            PlayerAudioManager playerAudioManager = other.gameObject.GetComponent<PlayerAudioManager>();
            if (playerAudioManager)
            {
                //print("Player " + playerAudioManager.player.GetPlayerIndex() + "exit");
                occlusionReference.CheckOcclusion(playerAudioManager);
            }
        }
    }
}
