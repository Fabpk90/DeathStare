﻿using UnityEngine;
using System.Collections;

public class ObstructionPortal : MonoBehaviour
{
    public ObstructionOcclusionReference obstructionReference;
    public bool onTriggerStay;

    private void OnTriggerEnter(Collider other)
    {
        PlayerAudioManager playerAudioManager = other.gameObject.GetComponent<PlayerAudioManager>();
        if (playerAudioManager)
        {
            obstructionReference.CheckObstruction(playerAudioManager);
            //print("Player " + playerAudioManager.player.GetPlayerIndex() + "exit");
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (onTriggerStay)
        {
            PlayerAudioManager playerAudioManager = other.gameObject.GetComponent<PlayerAudioManager>();
            if (playerAudioManager)
            {
                obstructionReference.CheckObstruction(playerAudioManager);
                //print("Player " + playerAudioManager.player.GetPlayerIndex() + "exit");
            }
        }
    }
}
