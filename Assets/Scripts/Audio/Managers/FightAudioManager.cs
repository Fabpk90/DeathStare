﻿using UnityEngine;
using System.Collections;

public class FightAudioManager : MonoBehaviour
{
    public static GameObject clocher;
    private bool firstBell = false;
    private bool secondBell = false;
    private bool thirdBell = false;
    private CooldownTimer firstTimerBell;
    private CooldownTimer secondTimerBell;
    private CooldownTimer thirdTimerBell;
    private bool EndMusicTriggered = false;

    public static FightAudioManager instance;

    private void Awake()
    {
        instance = this;
    }

    private void Start()
    {
        AkSoundEngine.PostEvent("AMB_Bed_Sea", AudioManager.instance.gameObject);
        AkSoundEngine.PostEvent("AMB_Bed_Wind", AudioManager.instance.gameObject);

        BellTimer();
    }

    private void Update()
    {
        firstTimerBell.Update(Time.deltaTime);
        secondTimerBell.Update(Time.deltaTime);
        thirdTimerBell.Update(Time.deltaTime);
    }

    public void CheckBellRing(uint score, int playerIndex)
    {
        Debug.Log("Bell checked for player " + playerIndex + "with a score of " + score);

        switch (playerIndex)
        {
            case (0):
                AkSoundEngine.SetState("STATE_General_BellPlayer", "Player1");
                break;
            case (1):
                AkSoundEngine.SetState("STATE_General_BellPlayer", "Player2");
                break;
            case (2):
                AkSoundEngine.SetState("STATE_General_BellPlayer", "Player3");
                break;
            case (3):
                AkSoundEngine.SetState("STATE_General_BellPlayer", "Player4");
                break;
        }

        if (score == Mathf.CeilToInt(TestMode.GetInstance().GetScoreToWin() / 4.0f) && !firstBell)
        {
            firstBell = true;
            clocher.GetComponent<PostWwiseEvent>().PostEvent("FOLEYS_Props_Bell");
            //Debug.Log("The Bell ring");
        }

        if (score == Mathf.CeilToInt(TestMode.GetInstance().GetScoreToWin() / 4.0f * 2) && !secondBell)
        {
            secondBell = true;
            clocher.GetComponent<PostWwiseEvent>().PostEvent("FOLEYS_Props_Bell");
            //Debug.Log("The Bell ring");
        }

        if (score == Mathf.CeilToInt(TestMode.GetInstance().GetScoreToWin() / 4.0f * 3) && !thirdBell)
        {
            thirdBell = true;
            clocher.GetComponent<PostWwiseEvent>().PostEvent("FOLEYS_Props_Bell");
            //Debug.Log("The Bell ring");
        }
    }

    private void BellTimer()
    {
        //print("AudioTimer launched");
        var setting = TestMode.GetInstance().SettingSerialized;
        float secondsInRound = setting.secondsInRound;

        firstTimerBell = new CooldownTimer(secondsInRound * 1 / 4);
        firstTimerBell.TimerCompleteEvent += () =>
        {
            print("Timer for first Bell reached");
            if (!firstBell)
            {
                clocher.GetComponent<PostWwiseEvent>().PostEvent("FOLEYS_Props_Bell");
                firstBell = true;
            }
        };
        firstTimerBell.Start();

        secondTimerBell = new CooldownTimer(secondsInRound * 2 / 4);
        secondTimerBell.TimerCompleteEvent += () =>
        {
            print("Timer for second Bell reached");
            if (!secondBell)
            {
                clocher.GetComponent<PostWwiseEvent>().PostEvent("FOLEYS_Props_Bell");
                secondBell = true;
            }
            MusicManager.instance.SetMusicState(MusicState.Fight);
        };
        secondTimerBell.Start();

        thirdTimerBell = new CooldownTimer(secondsInRound * 3 / 4);
        thirdTimerBell.TimerCompleteEvent += () =>
        {
            print("Timer for third Bell reached");
            if (!thirdBell)
            {
                clocher.GetComponent<PostWwiseEvent>().PostEvent("FOLEYS_Props_Bell");
                thirdBell = true;
            }
        };
        thirdTimerBell.Start();
    }

    public void CheckFightMusicState(uint score)
    {
        if (!EndMusicTriggered && score == Mathf.CeilToInt(TestMode.GetInstance().GetScoreToWin() / 2))
        {
            EndMusicTriggered = true;
            MusicManager.instance.SetMusicState(MusicState.Fight);
        }
    }
}
