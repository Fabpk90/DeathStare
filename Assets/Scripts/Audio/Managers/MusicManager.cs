﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public enum MusicState { Menu, Loading, Intro, Fight, Outro };
public class MusicManager : MonoBehaviour
{
    public static MusicManager instance;
    public static event EventHandler<string> OnIntroCue;
    public static event EventHandler OnMatchStart;
    public static event EventHandler OneSecBeforeMatchStart;

    private void Awake()
    {
        if (instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;
        }

        AkSoundEngine.SetState("STATE_Music_DuelState_J1", "False");
        AkSoundEngine.SetState("STATE_Music_DuelState_J2", "False");
        AkSoundEngine.SetState("STATE_Music_DuelState_J3", "False");
        AkSoundEngine.SetState("STATE_Music_DuelState_J4", "False");
    }

    private void Start()
    {
        PostMusicEventWithCallbacks("Post_Music_All");
    }

    public void PostMusicEvent(string eventName)
    {
        AkSoundEngine.PostEvent("Post_Music_All", gameObject);
    }

    private void PostMusicEventWithCallbacks(string eventName)
    {
        AkSoundEngine.PostEvent("Post_Music_All", gameObject, (uint)AkCallbackType.AK_MusicSyncAll, MusicCallbackFunction, (uint)AkCallbackType.AK_MusicSyncAll);
    }

    public void SetMusicState(MusicState stateName)
    {
        switch (stateName)
        {
            case (MusicState.Menu):
                AkSoundEngine.SetState("STATE_Music_Main", "MainMenu");
                AkSoundEngine.PostEvent("AMB_Menu_Bed", gameObject);
                break;
            case (MusicState.Loading):
                AkSoundEngine.SetState("STATE_Music_Main", "MainMenu_End");
                break;
            case (MusicState.Intro):
                AkSoundEngine.SetState("STATE_Music_Main", "Fight_Intro");
                break;
            case (MusicState.Fight):
                AkSoundEngine.SetState("STATE_Music_Main", "Fight_End");
                break;
            case (MusicState.Outro):
                AkSoundEngine.SetState("STATE_Music_Main", "Outro");
                break;
        }
    }

    private void MusicCallbackFunction(object in_cookie, AkCallbackType in_type, object in_info)
    {
        if (in_type == AkCallbackType.AK_MusicSyncUserCue)
        {
            AkMusicSyncCallbackInfo musicInfo = in_info as AkMusicSyncCallbackInfo;

            //El famoso
            string cueName = musicInfo.userCueName;
            OnIntroCue?.Invoke(this, cueName);
            
            switch (cueName) //Mettez ce que vous voulez faire à chaque étape du décompte dans chaque case
            {
                case "Intro":
                    //Debug.Log("Intro");
                    //Call what you want here
                    break;

                case "3":
                    //print("Intro: 3");
                    //Call what you want here
                    break;

                case "2":
                    //print("Intro: 2");
                    //Call what you want here
                    break;

                case "1":
                    //print("Intro: 1");
                    //Call what you want here
                    OneSecBeforeMatchStart?.Invoke(this, EventArgs.Empty);
                    break;

                case "Fight":
                    //print("Intro: Fight");
                    //Call what you want here
                    OnMatchStart?.Invoke(this, EventArgs.Empty);
                    break;

                case "Outro":
                    Debug.Log("Outro");
                    AkSoundEngine.PostEvent("Stop_All_Soft", gameObject);
                    break;
            }
            
        }
    }
}
