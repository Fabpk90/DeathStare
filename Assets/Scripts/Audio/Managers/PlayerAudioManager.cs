﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAudioManager : MonoBehaviour
{
    public PlayerController player;
    public GameObject leftFoot;
    public GameObject rightFoot;
    public GameObject chest;
    private int playerIndex;
    public string characterName;
    public WwiseListener wwiseListener;

    public static List<string> StatesBuffer = new List<string>();

    public bool isOnGrass;

    private RaycastHit hit;
    private float refreshTime = 0.08f;
    private float timer;
    private string previoustexture;
    private string newSurface;

    public void AddStateToBuffer(string state)
    {
        StatesBuffer.Insert(0, state);
    }

    public void RemoveStateToBuffer(string state)
    {
        StatesBuffer.Remove(state);
    }

    public void SetSurfaceFromBuffer()
    {
        SetTexture(StatesBuffer[0]);
    }

    public void CheckExitSurface()
    {
        if (StatesBuffer.Count > 0)
        {
            SetSurfaceFromBuffer();
        }
        else
        {
            SetTexture("Sand");
        }
    }



    public void SetTexture(string textureName)
    {
        AkSoundEngine.SetSwitch("SWITCHES_Foleys_Surfaces", textureName, leftFoot);
        AkSoundEngine.SetSwitch("SWITCHES_Foleys_Surfaces", textureName, rightFoot);
        AkSoundEngine.SetSwitch("SWITCHES_Foleys_Surfaces", textureName, chest);
    }

    private void SetCharacter()
    {
        AkSoundEngine.SetSwitch("SWITCHES_General_Character", characterName, leftFoot);
        AkSoundEngine.SetSwitch("SWITCHES_General_Character", characterName, rightFoot);
        AkSoundEngine.SetSwitch("SWITCHES_General_Character", characterName, chest);
        AkSoundEngine.SetSwitch("SWITCHES_General_Character", characterName, gameObject);
    }



    private void Start()
    {
        if(!player) return;
        AudioManager.instance.AddListeners(leftFoot, 0, 1, 2, 3);
        AudioManager.instance.AddListeners(rightFoot, 0, 1, 2, 3);
        AudioManager.instance.AddListeners(gameObject, player.GetPlayerIndex());
        switch (player.GetPlayerIndex())
        {
            case (0):
                AudioManager.instance.PlayerInstance1 = this;
                SetTexture("Sand");
                SetCharacter();
                AkSoundEngine.SetSwitch("SWITCHES_General_Player", "P1", gameObject);
                wwiseListener.VolumesOffset[0] = 0;
                wwiseListener.VolumesOffset[1] = -23*AudioManager.stereoWidth;
                break;
            case (1):
                AudioManager.instance.PlayerInstance2 = this;
                SetTexture("Sand");
                SetCharacter();
                AkSoundEngine.SetSwitch("SWITCHES_General_Player", "P2", gameObject);
                wwiseListener.VolumesOffset[0] = -23*AudioManager.stereoWidth;
                wwiseListener.VolumesOffset[1] = 0;
                break;
            case (2):
                AudioManager.instance.PlayerInstance3 = this;
                SetTexture("Sand");
                SetCharacter();
                AkSoundEngine.SetSwitch("SWITCHES_General_Player", "P3", gameObject);
                wwiseListener.VolumesOffset[0] = 0;
                wwiseListener.VolumesOffset[1] = -23*AudioManager.stereoWidth;
                break;
            case (3):
                AudioManager.instance.PlayerInstance4 = this;
                SetTexture("Sand");
                SetCharacter();
                AkSoundEngine.SetSwitch("SWITCHES_General_Player", "P4", gameObject);
                wwiseListener.VolumesOffset[0] = -23 * AudioManager.stereoWidth;
                wwiseListener.VolumesOffset[1] = 0;
                break;
        }
    }

    private void Update()
    {
        if(!player) return;
        timer += Time.deltaTime;
        if (timer > refreshTime)
        {
            timer = 0;

            Vector3 originPoint = transform.position + new Vector3(0, 0.5f, 0);

            RaycastHit hit;
            Ray ray = new Ray(originPoint, Vector3.down);

            //Debug.DrawRay(originPoint, Vector3.down * 10, Color.blue, 2);

            if (Physics.SphereCast(originPoint, 0.3f, Vector3.down, out hit, 5, player.stareHandler.mask, QueryTriggerInteraction.Ignore))
            {
                AudioTexture audioTexture = hit.transform.GetComponent<AudioTexture>();

                if (audioTexture)
                {
                    newSurface = audioTexture.surface;
                }
                else
                {
                    /*
                    GetTerrainTexture getTerrainTexture = gameObject.GetComponent<GetTerrainTexture>();
                    int index = getTerrainTexture.GetTerrainAtPosition(hit.point);
                    print(index);
                    */
                    /*
                    if (isOnGrass)
                        newSurface = "Grass";
                    else
                        newSurface = "Sand";
                        */
                    if (StatesBuffer.Count > 0)
                        newSurface = StatesBuffer[0];
                    else
                        newSurface = "Sand";
                }
            }

            if (newSurface != previoustexture)
            {
                SetTexture(newSurface);
                //print("Walking on " + newSurface);
            }
            previoustexture = newSurface;
        }
    }

    public void PostLeftFootEvent(string eventName)
    {
        AkSoundEngine.PostEvent(eventName, leftFoot);
    }

    public void PostRightFootEvent(string eventName)
    {
        AkSoundEngine.PostEvent(eventName, rightFoot);
    }

    public void PostChestEvent(string eventName)
    {
        AkSoundEngine.PostEvent(eventName, chest);
    }

    public void PostEvent(string eventName)
    {
        AkSoundEngine.PostEvent(eventName, gameObject);
    }

    public void PostEvent(string eventName, GameObject in_gameObject)
    {
        AkSoundEngine.PostEvent(eventName, in_gameObject);
    }

    public void SetRTPCValue(string parameterName, float value)
    {
        AkSoundEngine.SetRTPCValue(parameterName, value, this.gameObject);
    }
}
