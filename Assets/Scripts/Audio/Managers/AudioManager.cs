﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public enum SoundMenu { Main, CharSelect, CharOption, SoundOption, GraphicOption };
public class AudioManager : MonoBehaviour
{
    public Audio.AudioSettings audioSettings;

    public static AudioManager instance;
    public PlayerAudioManager PlayerInstance1;
    public PlayerAudioManager PlayerInstance2;
    public PlayerAudioManager PlayerInstance3;
    public PlayerAudioManager PlayerInstance4;

    public static bool mainMenuFirstSound;
    public static bool characterSelectFirstSound;
    public static bool characterOptionFirstSound;
    public static bool soundOptionFirstSound;
    public static bool graphicOptionFirstSound;

    /*
    public static GameObject clocher;
    private bool firstBell = false;
    private bool secondBell = false;
    private bool thirdBell = false;
    private CooldownTimer firstTimerBell;
    private CooldownTimer secondTimerBell;
    private CooldownTimer thirdTimerBell;
    */

    public GameObject[] Listeners;
    public string[] Soundbanks;

    //public bool combatMusicOnStart;
    //private bool EndMusicTriggered = false;

    public static int StaringCount;
    public static float stereoWidth = 0.8f;
	//public static event EventHandler<string> OnIntroCue;

    private void Awake()
    {
        //DontDestroyOnLoad(this.gameObject);

        if (instance != null)
            Destroy(gameObject);
        else
        {
            instance = this;
        
            //SoundBanks Loading
            for (int i=0; i<Soundbanks.Length; i++)
            {
                AkSoundEngine.LoadBank(Soundbanks[i], out uint bankID);
            }

            stereoWidth = audioSettings.stereoWidth;
            audioSettings.InitRTPC();

            //Listener setting
            AddListeners(gameObject, 4);
        }
            
    }

    private void Start()
    {
        /*
        AkSoundEngine.SetState("STATE_Music_DuelState_J1", "False");
        AkSoundEngine.SetState("STATE_Music_DuelState_J2", "False");
        AkSoundEngine.SetState("STATE_Music_DuelState_J3", "False");
        AkSoundEngine.SetState("STATE_Music_DuelState_J4", "False");
        */

        /*
        AkSoundEngine.SetState("STATE_Music_Main", "MainMenu_End");
        AkSoundEngine.PostEvent("Post_Music_All", gameObject, (uint)AkCallbackType.AK_MusicSyncAll, MusicCallbackFunction, (uint)AkCallbackType.AK_MusicSyncAll);
        AkSoundEngine.SetState("STATE_Music_Main", "Fight_Intro");
        */

        //AkSoundEngine.PostEvent("AMB_Bed_Sea", gameObject);
        //AkSoundEngine.PostEvent("AMB_Bed_Waves", gameObject);
        //AkSoundEngine.PostEvent("AMB_Bed_Wind", gameObject);

        //BellTimer();

        /*
        Debug.Log("Score for win : " + (int)(((TestMode)TestMode.instance).killsToWin));
        Debug.Log("Score for 1st bell : " + (int)(((TestMode)TestMode.instance).killsToWin)/4);
        Debug.Log("Score for 2nd bell : " + (int)(((TestMode)TestMode.instance).killsToWin) / 4 * 2);
        Debug.Log("Score for 3rd bell : " + (int)(((TestMode)TestMode.instance).killsToWin) / 4 * 3);
        */
    }

    private void Update()
    {
        //firstTimerBell.Update(Time.deltaTime);
        //secondTimerBell.Update(Time.deltaTime);
        //thirdTimerBell.Update(Time.deltaTime);

        switch (StaringCount)
        {
            case (0):
                AkSoundEngine.SetState("STATE_Music_DS_Intensity", "None");
                break;
            case (1):
                AkSoundEngine.SetState("STATE_Music_DS_Intensity", "None");
                break;
            case (2):
                AkSoundEngine.SetState("STATE_Music_DS_Intensity", "Low");
                break;
            case (3):
                AkSoundEngine.SetState("STATE_Music_DS_Intensity", "Mid");
                break;
            case (4):
                AkSoundEngine.SetState("STATE_Music_DS_Intensity", "Mid");
                break;
        }
    }

    public void AddListeners(GameObject in_emitter, int listener1)
    {
        if (Listeners[listener1]) AkSoundEngine.AddListener(in_emitter, Listeners[listener1]);
    }
    public void AddListeners(GameObject in_emitter, int listener1, int listener2)
    {
        if (Listeners[listener1]) AkSoundEngine.AddListener(in_emitter, Listeners[listener1]);
        if (Listeners[listener2]) AkSoundEngine.AddListener(in_emitter, Listeners[listener2]);
    }
    public void AddListeners(GameObject in_emitter, int listener1, int listener2, int listener3)
    {
        if (Listeners[listener1]) AkSoundEngine.AddListener(in_emitter, Listeners[listener1]);
        if (Listeners[listener2]) AkSoundEngine.AddListener(in_emitter, Listeners[listener2]);
        if (Listeners[listener3]) AkSoundEngine.AddListener(in_emitter, Listeners[listener3]);
    }
    public void AddListeners(GameObject in_emitter, int listener1, int listener2, int listener3, int listener4)
    {
        if (Listeners[listener1]) AkSoundEngine.AddListener(in_emitter, Listeners[listener1]);
        if (Listeners[listener2]) AkSoundEngine.AddListener(in_emitter, Listeners[listener2]);
        if (Listeners[listener3]) AkSoundEngine.AddListener(in_emitter, Listeners[listener3]);
        if (Listeners[listener4]) AkSoundEngine.AddListener(in_emitter, Listeners[listener4]);
    }

    public GameObject GetListener(int listenerIndex)
    {
        return Listeners[listenerIndex];
    }

    public void PostEvent(string eventName)
    {
        AkSoundEngine.PostEvent(eventName, gameObject);
    }

    public void IncreaseStaringCount()
    {
        StaringCount++;
//        Debug.Log("StaringCount " + StaringCount);
    }

    public void DecreaseStaringCount()
    {
        StaringCount--;
//        Debug.Log("StaringCount " + StaringCount);
    }

    /*
    public void CheckBellRing(uint score, int playerIndex)
    {
        Debug.Log("Bell checked for player " + playerIndex + "with a score of " + score);

        switch (playerIndex)
        {
            case (0):
                AkSoundEngine.SetState("STATE_General_BellPlayer", "Player1");
                break;
            case (1):
                AkSoundEngine.SetState("STATE_General_BellPlayer", "Player2");
                break;
            case (2):
                AkSoundEngine.SetState("STATE_General_BellPlayer", "Player3");
                break;
            case (3):
                AkSoundEngine.SetState("STATE_General_BellPlayer", "Player4");
                break;
        }

        if (score == Mathf.CeilToInt(TestMode.GetInstance().GetScoreToWin() / 4.0f) && !firstBell)
        {
            firstBell = true;
            clocher.GetComponent<PostWwiseEvent>().PostEvent("FOLEYS_Props_Bell");
            //Debug.Log("The Bell ring");
        }

        if (score == Mathf.CeilToInt(TestMode.GetInstance().GetScoreToWin() / 4.0f * 2) && !secondBell)
        {
            secondBell = true;
            clocher.GetComponent<PostWwiseEvent>().PostEvent("FOLEYS_Props_Bell");
            //Debug.Log("The Bell ring");
        }

        if (score == Mathf.CeilToInt(TestMode.GetInstance().GetScoreToWin() / 4.0f * 3) && !thirdBell)
        {
            thirdBell = true;
            clocher.GetComponent<PostWwiseEvent>().PostEvent("FOLEYS_Props_Bell");
            //Debug.Log("The Bell ring");
        }
    }
    
    private void BellTimer()
    {
        if(!TestMode.GetInstance()) return;
        //print("AudioTimer launched");
        var setting = TestMode.GetInstance().Settings as TestModeSettings;
        float secondsInRound = setting.secondsInRound;

        firstTimerBell = new CooldownTimer(secondsInRound * 1/4);
        firstTimerBell.TimerCompleteEvent += () =>
        {
            print("Timer for first Bell reached");
            if (!firstBell)
            {
                clocher.GetComponent<PostWwiseEvent>().PostEvent("FOLEYS_Props_Bell");
                firstBell = true;
            }
        };
        firstTimerBell.Start();

        secondTimerBell = new CooldownTimer(secondsInRound * 2 / 4);
        secondTimerBell.TimerCompleteEvent += () =>
        {
            print("Timer for second Bell reached");
            if (!secondBell)
            {
                clocher.GetComponent<PostWwiseEvent>().PostEvent("FOLEYS_Props_Bell");
                secondBell = true;
            }
            MusicManager.instance.SetMusicState(MusicState.Fight);
        };
        secondTimerBell.Start();

        thirdTimerBell = new CooldownTimer(secondsInRound * 3 / 4);
        thirdTimerBell.TimerCompleteEvent += () =>
        {
            print("Timer for third Bell reached");
            if (!thirdBell)
            {
                clocher.GetComponent<PostWwiseEvent>().PostEvent("FOLEYS_Props_Bell");
                thirdBell = true;
            }
        };
        thirdTimerBell.Start();
    }

    public void CheckFightMusicState(uint score)
    {
        if (!EndMusicTriggered && score == Mathf.CeilToInt(TestMode.GetInstance().GetScoreToWin() / 2))
        {
            EndMusicTriggered = true;
            MusicManager.instance.SetMusicState(MusicState.Fight);
        }
    }
    */



    public PlayerAudioManager GetPlayerAudioManager(int playerIndex)
    {
        PlayerAudioManager playerAudioManagerToReturn = PlayerInstance1;
        switch (playerIndex)
        {
            case (0):
                playerAudioManagerToReturn = PlayerInstance1;
                break;
            case (1):
                playerAudioManagerToReturn = PlayerInstance2;
                break;
            case (2):
                playerAudioManagerToReturn = PlayerInstance3;
                break;
            case (3):
                playerAudioManagerToReturn = PlayerInstance4;
                break;
        }
        return playerAudioManagerToReturn;
    }

    public static bool GetStringFromPlayingID(string eventName, GameObject go, out uint out_playingID)
    {
        uint[] playingIds = new uint[50];

        uint testEventId = AkSoundEngine.GetIDFromString(eventName);

        uint count = (uint)playingIds.Length;
        AKRESULT result = AkSoundEngine.GetPlayingIDsFromGameObject(go, ref count, playingIds);

        for (int i = 0; i < count; i++)
        {
            uint playingId = playingIds[i];
            uint eventId = AkSoundEngine.GetEventIDFromPlayingID(playingId);

            if (eventId == testEventId)
            {
                out_playingID = playingId;
                return true;
            }
        }
        out_playingID = 0;
        return false;
    }

    public void StopEvent(string eventName, GameObject in_gameObject, int fadeTime, AkCurveInterpolation curve)
    {
        uint playingID;

        if (GetStringFromPlayingID(eventName, in_gameObject, out playingID))
        {
            AkSoundEngine.StopPlayingID(playingID, fadeTime, curve);
        }
        else
        {
            Debug.LogWarning("Trying to stop an event that was not find:" + eventName);
        }
    }
}
