using System;
using UnityEngine;

namespace Audio
{
    [RequireComponent(typeof(StareHandler))]
    public class StareAudio : MonoBehaviour
    {
        private PlayerController _controller;
        private StareHandler _stareHandler;
        private PlayerAudioManager _playerAudioManager;

        private bool firstRepulse;
        
        private void OnEnable()
        {
            _stareHandler.OnStareStart += OnStareStart;
            _stareHandler.OnStareStop += OnStareStop;
            _stareHandler.OnStarePushInTheAir += OnStarePushInTheAir;
            _stareHandler.OnStareTouch += OnStareTouch; ;
        }

        private void OnStareTouch(object sender, int e)
        {
            _playerAudioManager.PostEvent("EFFECTS_Char_HitMarker");
        }

        private void OnStareTouch(object sender, EventArgs e)
        {

        }

        private void OnStareStop(object sender, EventArgs e)
        {
            _playerAudioManager = AudioManager.instance.GetPlayerAudioManager(_controller.GetPlayerIndex());
            //Sound
            AudioManager.instance.DecreaseStaringCount();
            _playerAudioManager.PostEvent("Stop_EFFECTS_Char_Staring");
            //AudioManager.instance.StopEvent("EFFECTS_Char_Staring", _playerAudioManager.gameObject, 600, AkCurveInterpolation.AkCurveInterpolation_InvSCurve);
            _playerAudioManager.PostEvent("EFFECTS_Char_ExitStaring");
            switch (_controller.GetPlayerIndex())
            {
                case (0):
                    AkSoundEngine.SetState("STATE_Music_DuelState_J1", "False");
                    break;
                case (1):
                    AkSoundEngine.SetState("STATE_Music_DuelState_J2", "False");
                    break;
                case (2):
                    AkSoundEngine.SetState("STATE_Music_DuelState_J3", "False");
                    break;
                case (3):
                    AkSoundEngine.SetState("STATE_Music_DuelState_J4", "False");
                    break;
            }
            //Sound
        }

        private void OnDisable()
        {
            _stareHandler.OnStareStart -= OnStareStart;
            _stareHandler.OnStareStop -= OnStareStop;
        }

        private void Awake()
        {
            _stareHandler = GetComponent<StareHandler>();
            _controller = GetComponentInParent<PlayerController>();
        }

        private void OnStareStart(object sender, EventArgs e)
        {
            _playerAudioManager = AudioManager.instance.GetPlayerAudioManager(_controller.GetPlayerIndex());
            AudioManager.instance.IncreaseStaringCount();
           
            _playerAudioManager.PostEvent("EFFECTS_Char_Staring");
            _playerAudioManager.PostEvent("EFFECTS_Char_EnterStaring");

            switch (_controller.GetPlayerIndex())
            {
                case 0:
                case 2:
                    switch (_playerAudioManager.characterName)
                    {
                        case ("Stanislas"):
                            AkSoundEngine.PostTrigger("TRIGGER_DS_Stan_L", MusicManager.instance.gameObject);
                            break;
                        case ("Marta"):
                            AkSoundEngine.PostTrigger("TRIGGER_DS_Marta_L", MusicManager.instance.gameObject);
                            break;
                        case ("Medusa"):
                            AkSoundEngine.PostTrigger("TRIGGER_DS_Medusa_L", MusicManager.instance.gameObject);
                            break;
                        case ("Don"):
                            AkSoundEngine.PostTrigger("TRIGGER_DS_Don_L", MusicManager.instance.gameObject);
                            break;
                    }

                    break;
                case 1:
                case 3:
                    switch (_playerAudioManager.characterName)
                    {
                        case ("Stanislas"):
                            AkSoundEngine.PostTrigger("TRIGGER_DS_Stan_R", MusicManager.instance.gameObject);
                            break;
                        case ("Marta"):
                            AkSoundEngine.PostTrigger("TRIGGER_DS_Marta_R", MusicManager.instance.gameObject);
                            break;
                        case ("Medusa"):
                            AkSoundEngine.PostTrigger("TRIGGER_DS_Medusa_R", MusicManager.instance.gameObject);
                            break;
                        case ("Don"):
                            AkSoundEngine.PostTrigger("TRIGGER_DS_Don_R", MusicManager.instance.gameObject);
                            break;
                    }
                    break;
            }
        }

        private void OnStarePushInTheAir(object sender, EventArgs e)
        {
            if (!firstRepulse)
            {
                _playerAudioManager.PostEvent("EFFECTS_Char_Repulse");
            }
        }

    }
}