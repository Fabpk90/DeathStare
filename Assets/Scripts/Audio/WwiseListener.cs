﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WwiseListener : MonoBehaviour
{
    public float[] VolumesOffset = new float[2]; //Panning offset
    AkChannelConfig cfg;

    public GameObject camera;
    
    void Start()
    {
        cfg = new AkChannelConfig();
        //Mofifying Listener spatialisation
        cfg.SetStandard(AkSoundEngine.AK_SPEAKER_SETUP_STEREO);
        if (camera)
            AkSoundEngine.SetListenerSpatialization(camera, true, cfg, VolumesOffset);
        else
            AkSoundEngine.SetListenerSpatialization(gameObject, true, cfg, VolumesOffset);
    }
}
