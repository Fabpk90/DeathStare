﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetWwiseListener : MonoBehaviour
{
    public int[] listenersIndex;
    public bool onStartOfMatch;

    private void OnEnable()
    {
        GameMode.OnStartOfMatch += OnMatchStart;
    }

    private void OnDisable()
    {
        GameMode.OnStartOfMatch -= OnMatchStart;
    }

    private void OnMatchStart(object sender, EventArgs eventArgs)
    {
        if (onStartOfMatch)
        {
            for (int i = 0; i < listenersIndex.Length; i++)
            {
                AudioManager.instance.AddListeners(gameObject, listenersIndex[i]);
            }
        }
    }

    private void Start()
    {
        if (!onStartOfMatch)
        {
            for (int i = 0; i < listenersIndex.Length; i++)
            {
                AudioManager.instance.AddListeners(gameObject, listenersIndex[i]);
            }
        }
    }
}
