using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

namespace Audio
{
    
    [Serializable]
    public class AudioSettings 
    {
        public float masterVolume = 0.8f;
        public float voiceVolume = 0.8f;
        public float stareVolume = 0.8f;
        public float musicVolume = 0.8f;
        public float effectsVolume = 0.8f;
        public float foleysVolume = 0.8f;
        public float ambianceVolume = 0.8f;
        public float interfaceVolume = 0.8f;
        public float stereoWidth = 0.8f;

        
        public void CheckAndLoad()
        {
            if (File.Exists(Application.persistentDataPath + "/AudioSettings.data"))
            {
                FileStream file = File.OpenRead(Application.persistentDataPath + "/AudioSettings.data");
                BinaryFormatter bf = new BinaryFormatter();

                var ts = (AudioSettings) bf.Deserialize(file);
                ambianceVolume = ts.ambianceVolume;
                voiceVolume = ts.voiceVolume;
                stareVolume = ts.stareVolume;
                musicVolume = ts.musicVolume;
                effectsVolume = ts.effectsVolume;
                foleysVolume = ts.foleysVolume;
                masterVolume = ts.masterVolume;
                interfaceVolume = ts.interfaceVolume;
                stereoWidth = ts.stereoWidth;
            
                file.Close();
            }
            else
            {
                var file = File.Create(Application.persistentDataPath + "/AudioSettings.data");
            
                BinaryFormatter bf = new BinaryFormatter();
                bf.Serialize(file, this);
                file.Close();
            }
        }
        
        public void InitRTPC()
        {
            UpdateRTPC();
        }

        public void SetMasterVolume(float volume)
        {
            masterVolume = volume;
        }

        public void SetVoiceVolume(float volume)
        {
            voiceVolume = volume;
        }

        public void SetStareVolume(float volume)
        {
            stareVolume = volume;
        }

        public void SetMusicVolume(float volume)
        {
            musicVolume = volume;
        }
        
        public void SetEffectsVolume(float volume)
        {
            effectsVolume = volume;
        }

        public void SetFoleysVolume(float volume)
        {
            foleysVolume = volume;
        }

        public void SetAmbianceVolume(float volume)
        {
            ambianceVolume = volume;
        }

        public void SetInterfaceVolume(float volume)
        {
            interfaceVolume = volume;
        }

        public void SetStereoWidth(float value)
        {
            stereoWidth = value;
        }

        public void UpdateRTPC()
        {
            AkSoundEngine.SetRTPCValue("RTPC_Master_Volume", masterVolume);
            AkSoundEngine.SetRTPCValue("RTPC_Voice_Volume", voiceVolume);
            AkSoundEngine.SetRTPCValue("RTPC_DS_Volume", stareVolume);
            AkSoundEngine.SetRTPCValue("RTPC_Music_Volume", musicVolume);
            AkSoundEngine.SetRTPCValue("RTPC_Effects_Volume", effectsVolume);
            AkSoundEngine.SetRTPCValue("RTPC_Foleys_Volume", foleysVolume);
            AkSoundEngine.SetRTPCValue("RTPC_Ambiances_Volume", ambianceVolume);
            AkSoundEngine.SetRTPCValue("RTPC_UI_Volume", interfaceVolume);
            AkSoundEngine.SetRTPCValue("RTPC_StereoWidth", stereoWidth);
        }

        public void Save()
        {
            var file = File.Create(Application.persistentDataPath + "/AudioSettings.data");
            
            BinaryFormatter bf = new BinaryFormatter();
            bf.Serialize(file, this);
            file.Close();
        }
    }
}