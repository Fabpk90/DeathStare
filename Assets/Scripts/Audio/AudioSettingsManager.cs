using System;
using UnityEngine;
using UnityEngine.UI;

namespace Audio
{
    public class AudioSettingsManager : MonoBehaviour
    {
        public AudioSettings Settings;

        public Slider sliderMasterVolume;
        public Slider sliderMusicVolume;
        public Slider sliderVoicesVolume;
        public Slider sliderStareVolume;
        public Slider sliderEffectsVolume;
        public Slider sliderFoleysVolume;
        public Slider sliderAmbianceVolume;
        public Slider sliderInterfaceVolume;
        public Slider sliderStereoWidth;

        private void OnEnable()
        {
            Settings = new AudioSettings();
            Settings.CheckAndLoad();
            
            sliderMasterVolume.value = Settings.masterVolume;
            sliderVoicesVolume.value = Settings.voiceVolume;
            sliderStareVolume.value = Settings.stareVolume;
            sliderEffectsVolume.value = Settings.effectsVolume;
            sliderFoleysVolume.value = Settings.foleysVolume;
            sliderAmbianceVolume.value = Settings.ambianceVolume;
            sliderInterfaceVolume.value = Settings.interfaceVolume;
            sliderStereoWidth.value = Settings.stereoWidth;
            sliderMusicVolume.value = Settings.musicVolume;

            sliderMasterVolume.onValueChanged.AddListener(arg0 =>
            {
                Settings.SetMasterVolume(arg0);
                Settings.UpdateRTPC();   
            });
            sliderMusicVolume.onValueChanged.AddListener(arg0 =>
            {
                Settings.SetMusicVolume(arg0);
                Settings.UpdateRTPC();   
            });
            sliderVoicesVolume.onValueChanged.AddListener(arg0 =>
            {
                Settings.SetVoiceVolume(arg0);
                Settings.UpdateRTPC();
            });
            sliderStareVolume.onValueChanged.AddListener(arg0 =>
            {
                Settings.SetStareVolume(arg0);
                Settings.UpdateRTPC();
            });
            sliderEffectsVolume.onValueChanged.AddListener(arg0 =>
            {
                Settings.SetEffectsVolume(arg0);
                Settings.UpdateRTPC();
            });
            sliderFoleysVolume.onValueChanged.AddListener(arg0 =>
            {
                Settings.SetFoleysVolume(arg0);
                Settings.UpdateRTPC();
            });
            sliderAmbianceVolume.onValueChanged.AddListener(arg0 =>
            {
                Settings.SetAmbianceVolume(arg0);
                Settings.UpdateRTPC();
            });
            sliderInterfaceVolume.onValueChanged.AddListener(arg0 =>
            {
                Settings.SetInterfaceVolume(arg0);
                Settings.UpdateRTPC();
            });
            sliderStereoWidth.onValueChanged.AddListener(arg0 =>
            {
                Settings.SetStereoWidth(arg0);
                Settings.UpdateRTPC();
            });

            Settings.UpdateRTPC();
        }

        private void OnDisable()
        {
            Settings.Save();
            sliderAmbianceVolume.onValueChanged.RemoveAllListeners();
            sliderMasterVolume.onValueChanged.RemoveAllListeners();
            sliderEffectsVolume.onValueChanged.RemoveAllListeners();
            sliderVoicesVolume.onValueChanged.RemoveAllListeners();
            sliderStareVolume.onValueChanged.RemoveAllListeners();
            sliderFoleysVolume.onValueChanged.RemoveAllListeners();
            sliderInterfaceVolume.onValueChanged.RemoveAllListeners();
            sliderStereoWidth.onValueChanged.RemoveAllListeners();
        }
    }
}