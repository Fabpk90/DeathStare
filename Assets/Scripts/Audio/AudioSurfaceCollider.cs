﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioSurfaceCollider : MonoBehaviour
{
    public enum Surfaces { Grass, Wood, Cobble, Mix };
    public Surfaces surfaceToSet;

    private bool isSwitchable;
    private PlayerAudioManager playerAudioManager;
   
    /*
    private void OnTriggerStay(Collider other)
    {
        if (!playerAudioManager.isOnGrass)
            playerAudioManager.isOnGrass = true;
    }
    */

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<PlayerAudioManager>())
        {
            playerAudioManager = other.gameObject.GetComponent<PlayerAudioManager>();
            /*
            switch (surfaceToSet)
            {
                case Surfaces.Grass:
                    playerAudioManager.SetTexture("Grass");
                    break;
                case Surfaces.Wood:
                    playerAudioManager.SetTexture("Wood");
                    break;
                case Surfaces.Cobble:
                    playerAudioManager.SetTexture("Cobble");
                    break;
                case Surfaces.Mix:
                    playerAudioManager.SetTexture("Mix");
                    break;
            }
            */

            switch (surfaceToSet)
            {
                case Surfaces.Grass:
                    playerAudioManager.AddStateToBuffer("Grass");
                    break;
                case Surfaces.Wood:
                    playerAudioManager.AddStateToBuffer("Wood");
                    break;
                case Surfaces.Cobble:
                    playerAudioManager.AddStateToBuffer("Cobble");
                    break;
                case Surfaces.Mix:
                    playerAudioManager.AddStateToBuffer("Mix");
                    break;
            }
            playerAudioManager.SetSurfaceFromBuffer();
            //print("Enter " + surfaceToSet);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.GetComponent<PlayerAudioManager>())
        {
            playerAudioManager = other.gameObject.GetComponent<PlayerAudioManager>();
            switch (surfaceToSet)
            {
                case Surfaces.Grass:
                    playerAudioManager.RemoveStateToBuffer("Grass");
                    break;
                case Surfaces.Wood:
                    playerAudioManager.RemoveStateToBuffer("Wood");
                    break;
                case Surfaces.Cobble:
                    playerAudioManager.RemoveStateToBuffer("Cobble");
                    break;
                case Surfaces.Mix:
                    playerAudioManager.RemoveStateToBuffer("Mix");
                    break;
            }
            playerAudioManager.CheckExitSurface();
            //playerAudioManager.SetTexture("Sand");
            //playerAudioManager.isOnGrass = false;
        }
    }
}
