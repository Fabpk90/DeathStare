﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioEventConfiner : MonoBehaviour
{
    [Header("Event to clamp to AkAudioListener")]
    public AK.Wwise.Event Event = new AK.Wwise.Event();

    [Header("Settings")]
    public float UpdateInterval = 0.05f;

    private IEnumerator positionClamperRoutine;

    private Collider trigger;
    private Transform targetTransform;

    private GameObject eventEmitter;

    public int listenerIndex;

    private bool listenersInit;

    private CooldownTimer _timer;

    private void Awake()
    {
        eventEmitter = new GameObject("Clamped Emitter" + transform.name);
        eventEmitter.AddComponent<AkGameObj>();
        eventEmitter.transform.parent = transform;
        Rigidbody RB = eventEmitter.AddComponent<Rigidbody>();
        RB.isKinematic = true;
        SphereCollider SPC = eventEmitter.AddComponent<SphereCollider>();
        SPC.isTrigger = true;
        

        trigger = GetComponent<Collider>();
        trigger.isTrigger = true;

        _timer = new CooldownTimer(UpdateInterval, true);
        _timer.TimerCompleteEvent += () =>
        {
            Vector3 closestPoint = trigger.ClosestPoint(targetTransform.position);
            eventEmitter.transform.position = closestPoint;
        };
        
        GameMode.OnStartOfMatch += OnMatchStart;
    }

    private void OnMatchStart(object sender, EventArgs eventArgs)
    {
        /*
        if (!AudioManager.instance.GetPlayerAudioManager(listenerIndex))
        {
            targetTransform = AudioManager.instance.GetPlayerAudioManager(listenerIndex).gameObject.transform;
            AudioManager.instance.AddListeners(eventEmitter, listenerIndex);
            Event.Post(eventEmitter);
        }
        */

        if (AudioManager.instance.GetPlayerAudioManager(listenerIndex) != null)
        {
            targetTransform = AudioManager.instance.GetPlayerAudioManager(listenerIndex).gameObject.transform;
            AudioManager.instance.AddListeners(eventEmitter, listenerIndex);
            Event.Post(eventEmitter);

            _timer.Start();
        }
    }

    private void OnDisable()
    {
        Event.Stop(eventEmitter);
        GameMode.OnStartOfMatch -= OnMatchStart;
    }

    private void Update()
    {
        _timer.Update(Time.deltaTime);
    }


    /*
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.blue;
        if (eventEmitters != null)
        {
            Gizmos.DrawSphere(eventEmitters.transform.position, 0.2f);
        }
    }
    */
}
