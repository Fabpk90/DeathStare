﻿using System;
using UnityEngine;

public class StaminaAudio : MonoBehaviour
{
    private PlayerAudioManager playerAudioManager;
    private Actor.Player.Stare.StaminaHandler staminaHandler;
    private StareHandler stareHandler;
    private bool lowStaminaSFXPosted;

    private void OnEnable()
    {
        GameMode.OnStartOfMatch += OnMatchStart;
        staminaHandler.OnStaminaRegen += OnStaminaRegen;
        staminaHandler.OnStaminaNotEnough += OnStaminaNotEnough;
        staminaHandler.OnStaminaUsed += OnStaminaUsed;
        stareHandler.OnStareStop += OnStareStop;
    }

    private void Awake()
    {
        staminaHandler = gameObject.GetComponent<Actor.Player.Stare.StaminaHandler>();
        stareHandler = gameObject.GetComponent<StareHandler>();
    }

    private void OnMatchStart(object sender, EventArgs eventArgs)
    {
        playerAudioManager = AudioManager.instance.GetPlayerAudioManager(stareHandler.GetPlayerIndex());
    }

    private void OnStaminaRegen(object sender, float stamina)
    {
        
    }

    private void OnStareStop(object sender, EventArgs eventArgs)
    {
        AudioManager.instance.StopEvent("EFFECTS_Char_StaminaLow", playerAudioManager.gameObject, 200, AkCurveInterpolation.AkCurveInterpolation_Exp1);
        lowStaminaSFXPosted = false;
    }

    private void OnStaminaNotEnough(object sender, EventArgs eventArgs)
    {
        playerAudioManager.PostEvent("EFFECTS_Char_OutOfStamina");

        if (lowStaminaSFXPosted)
        {
            AudioManager.instance.StopEvent("EFFECTS_Char_StaminaLow", playerAudioManager.gameObject, 200, AkCurveInterpolation.AkCurveInterpolation_Exp1);
            lowStaminaSFXPosted = false;
        }
    }

    private void OnStaminaUsed(object sender, float stamina)
    {
        playerAudioManager.SetRTPCValue("RTPC_Character_Stamina", stamina);

        if (!lowStaminaSFXPosted && stamina <= 45)
        {
            print("StaminaLow");
            playerAudioManager.PostEvent("EFFECTS_Char_StaminaLow");
            lowStaminaSFXPosted = true;
        }
    }
}
