﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeMusicState : MonoBehaviour
{
    public MusicState state;

    void Start()
    {
        switch (state)
        {
            case MusicState.Menu:
                MusicManager.instance.SetMusicState(MusicState.Menu);
                break;
            case MusicState.Loading:
                MusicManager.instance.SetMusicState(MusicState.Loading);
                break;
            case MusicState.Intro:
                MusicManager.instance.SetMusicState(MusicState.Intro);
                break;
            case MusicState.Fight:
                MusicManager.instance.SetMusicState(MusicState.Fight);
                break;
            case MusicState.Outro:
                MusicManager.instance.SetMusicState(MusicState.Outro);
                break;
        }
    }
}
