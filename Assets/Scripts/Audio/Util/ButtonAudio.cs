﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ButtonAudio : MonoBehaviour
{
    public AK.Wwise.Event selectSound;
    public AK.Wwise.Event submitSound;
    public AK.Wwise.Event cancelSound;
    public AK.Wwise.Event sliderSound;

    private EventTrigger trigger;
    private EventTrigger.Entry selectEntry;
    private EventTrigger.Entry submitEntry;
    private EventTrigger.Entry cancelEntry;
    private Slider.SliderEvent sliderEvent;

    public SoundMenu menu;

    void OnEnable()
    {
        trigger = GetComponent<EventTrigger>();

        //Select
        selectEntry = new EventTrigger.Entry();
        selectEntry.eventID = EventTriggerType.Select;
        selectEntry.callback.AddListener((data) => { OnSelect(); });
        trigger.triggers.Add(selectEntry);

        //Submit
        submitEntry = new EventTrigger.Entry();
        submitEntry.eventID = EventTriggerType.Submit;
        submitEntry.callback.AddListener((data) => { OnSubmit(); });
        trigger.triggers.Add(submitEntry);

        //Cancel
        cancelEntry = new EventTrigger.Entry();
        cancelEntry.eventID = EventTriggerType.Cancel;
        cancelEntry.callback.AddListener((data) => { OnCancel(); });
        trigger.triggers.Add(cancelEntry);

        //Slider
        if (gameObject.GetComponent<Slider>())
        {
            sliderEvent = gameObject.GetComponent<Slider>().onValueChanged;
            sliderEvent.AddListener((data) => { OnValueChange(); });
        }
    }

    public void OnSelect()
    {
        switch (menu)
        {
            case SoundMenu.Main:
                if (AudioManager.mainMenuFirstSound && selectSound != null)
                    selectSound.Post(gameObject);
                break;

            case SoundMenu.CharSelect:
                if (AudioManager.characterSelectFirstSound && selectSound != null)
                    selectSound.Post(gameObject);
                break;

            case SoundMenu.CharOption:
                if (AudioManager.characterOptionFirstSound && selectSound != null)
                    selectSound.Post(gameObject);
                break;

            case SoundMenu.SoundOption:
                if (AudioManager.soundOptionFirstSound && selectSound != null)
                    selectSound.Post(gameObject);
                break;
                
            case SoundMenu.GraphicOption:
                if (AudioManager.graphicOptionFirstSound && selectSound != null)
                    selectSound.Post(gameObject);
                break;
        }

        switch (menu)
        {
            case SoundMenu.Main:
                if (!AudioManager.mainMenuFirstSound)
                    AudioManager.mainMenuFirstSound = true;
                break;

            case SoundMenu.CharSelect:
                if (!AudioManager.characterSelectFirstSound)
                    AudioManager.characterSelectFirstSound = true;
                break;

            case SoundMenu.CharOption:
                if (!AudioManager.characterOptionFirstSound)
                    AudioManager.characterOptionFirstSound = true;
                break;

            case SoundMenu.SoundOption:
                if (!AudioManager.soundOptionFirstSound)
                    AudioManager.soundOptionFirstSound = true;
                break;

            case SoundMenu.GraphicOption:
                if (!AudioManager.graphicOptionFirstSound)
                    AudioManager.graphicOptionFirstSound = true;
                break;
        }
    }

    public void OnSubmit()
    {
        if (submitSound != null)
            submitSound.Post(gameObject);
    }

    public void OnCancel()
    {
        if (submitSound != null)
            cancelSound.Post(gameObject);
    }

    public void OnValueChange()
    {
        switch (menu)
        {
            case SoundMenu.Main:
                if (AudioManager.mainMenuFirstSound && sliderSound != null)
                    sliderSound.Post(gameObject);
                break;

            case SoundMenu.CharSelect:
                if (AudioManager.characterSelectFirstSound && sliderSound != null)
                    sliderSound.Post(gameObject);
                break;

            case SoundMenu.CharOption:
                if (AudioManager.characterOptionFirstSound && sliderSound != null)
                    sliderSound.Post(gameObject);
                break;

            case SoundMenu.SoundOption:
                if (AudioManager.soundOptionFirstSound && sliderSound != null)
                    sliderSound.Post(gameObject);
                break;

            case SoundMenu.GraphicOption:
                if (AudioManager.graphicOptionFirstSound && sliderSound != null)
                    sliderSound.Post(gameObject);
                break;
        }

        switch (menu)
        {
            case SoundMenu.Main:
                if (!AudioManager.mainMenuFirstSound)
                    AudioManager.mainMenuFirstSound = true;
                break;

            case SoundMenu.CharSelect:
                if (!AudioManager.characterSelectFirstSound)
                    AudioManager.characterSelectFirstSound = true;
                break;

            case SoundMenu.CharOption:
                if (!AudioManager.characterOptionFirstSound)
                    AudioManager.characterOptionFirstSound = true;
                break;

            case SoundMenu.SoundOption:
                if (!AudioManager.soundOptionFirstSound)
                    AudioManager.soundOptionFirstSound = true;
                break;

            case SoundMenu.GraphicOption:
                if (!AudioManager.graphicOptionFirstSound)
                    AudioManager.graphicOptionFirstSound = true;
                break;
        }
    }
}
