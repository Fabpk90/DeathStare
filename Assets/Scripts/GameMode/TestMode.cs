﻿using System;
using System.Collections.Generic;
using Actor.Player;
using TMPro;
using UnityEngine;
using UnityEngine.InputSystem;

[Serializable]
public class FinalScoreHandler : IComparable<FinalScoreHandler>
{
    public uint score;
    public PlayerInput player;


    public FinalScoreHandler(uint score, PlayerInput player)
    {
        this.score = score;
        this.player = player;
    }
    
    public int CompareTo(FinalScoreHandler other)
    {
        if (score <= other.score)
            return 1;

        return -1;
    }
}


[RequireComponent(typeof(PlayerInputManager))]
public class TestMode : GameMode
{
    [Tooltip("Permet de tester avec une seule manette")]
    public bool debugPlayers;

    public Transform[] spawnPoints;

    private CooldownTimer _roundTimer;
    private CooldownTimer _roundTimerSoon;
    private CooldownTimer _slowTimeCooldown;
    public TextMeshProUGUI textTimerRound;
    

    private CooldownTimer[] _respawnCooldowns = new CooldownTimer[4];

    public GameObject scoreUI;
    public List<TextMeshProUGUI> scoreTexts;

    public EventHandler OnEndIsSoon;
    public bool endIsSoon;
    
    public static List<FinalScoreHandler> playerScores;

    public static event EventHandler<List<FinalScoreHandler>> OnScoreChanged;
    public static event EventHandler<int> OnRespawn;
	public static event EventHandler<List<FinalScoreHandler>> OnMatchEnd;

    private TestModeSettings _settings;
    public TestModeSettingsHelper SettingSerialized;
    public override void Init()
    {
        base.Init();
        
        SettingSerialized = TestModeSettingsHelper.GetInstance();
        
        _settings = Settings as TestModeSettings;
        playerScores = new List<FinalScoreHandler>();

        instance = this;
        
        if(Gamepad.all.Count == 0)
            Debug.LogError("Attention pas de manette connectée !");

        if (!debugPlayers || Gamepad.all.Count > 1)
        {
            //we add the 4 players, or less depending on connected gamepads
            for (int i = 0; i < Gamepad.all.Count && i < 4; i++)
            {
                _manager.playerPrefab = _settings.playerPrefab[SettingSerialized.playersPrefabIndex[i]].gameObject;
                _manager.playerPrefab.GetComponent<PlayerInput>().actions = _settings.playerInputs[i];

                _manager.playerPrefab.GetComponent<PlayerController>().cameraMovement.settings =
                    _settings.playerSensitivities[i];
                
                _manager.JoinPlayer(i, -1, "GamePads", Gamepad.all[i]);
            }
        }
        else
        {
            for (int i = 0; i < 4; i++)
            {
                _manager.playerPrefab = _settings.playerPrefab[SettingSerialized.playersPrefabIndex[i]].gameObject;
                _manager.playerPrefab.GetComponent<PlayerInput>().actions = _settings.playerInputs[i];
                
                _manager.playerPrefab.GetComponent<PlayerController>().cameraMovement.settings =
                    _settings.playerSensitivities[i];
                _manager.JoinPlayer(i, -1, "GamePads", Gamepad.all[0]);
            }
        }

        foreach (PlayerInput player in players)
        {
            player.SwitchCurrentActionMap("Death");
        }

        _roundTimer = new CooldownTimer(SettingSerialized.secondsInRound);
        _roundTimer.TimerCompleteEvent += OnEndTimeOfRound;
        
        _slowTimeCooldown = new CooldownTimer(SettingSerialized.secondsInSlowTime);
        _slowTimeCooldown.TimerCompleteEvent += () =>
        {
            playerScores.Sort();

            scoreUI.SetActive(true);

            Time.timeScale = 1.0f;
        };

        _roundTimerSoon = new CooldownTimer(SettingSerialized.secondsInRound * 2/3);
        _roundTimerSoon.TimerCompleteEvent += () =>
        {
            OnEndIsSoon?.Invoke(this, null);
            endIsSoon = true;
        };

        for (int i = 0; i < 4; i++)
        {
            _respawnCooldowns[i] = new CooldownTimer(SettingSerialized.respawnCooldown);
            
            var i1 = i;
            _respawnCooldowns[i].TimerCompleteEvent += () =>
            {
                Respawn(i1);
            };
        }

        OnKillEvent += OnKill;
        OnStartOfMatch += OnStartMatch;
    }

    protected override void StartOfMatch(object sender, EventArgs e)
    {
        base.StartOfMatch(sender, e);
        
        _roundTimer.Start();
        _roundTimerSoon.Start();
    }

    protected override void Disable()
    {
        base.Disable();
        
        OnKillEvent -= OnKill;
        OnStartOfMatch -= OnStartMatch;
    }

    private void OnKill(object sender, Tuple<int, int> e)
    {
        _respawnCooldowns[e.Item2].Start();

        for (int i = 0; i < playerScores.Count; i++)
        {
            if (playerScores[i].player.playerIndex == e.Item1)
            {
                playerScores[i].score++;
                playerScores.Sort();
                break;
            }
        }
            
        OnScoreChanged?.Invoke(this, playerScores);
    }

    private void OnStartMatch(object sender, EventArgs e)
    {
        foreach (var player in players)
        {
            player.SwitchCurrentActionMap("Arena");
        }
    }

    public static TestMode GetInstance()
    {
        return (TestMode) instance;
    }

    public uint GetScoreToWin()
    {
        return SettingSerialized.killsToWin;
    }

    private void Respawn(int playerIndex)
    {
        var player = players[playerIndex];
        var ct = player.GetComponent<CharacterController>();
        
        ct.enabled = false;
        player.transform.position = spawnPoints[player.playerIndex].position;
        player.transform.rotation = spawnPoints[player.playerIndex].rotation;
        ct.enabled = true;
        
        player.GetComponent<PlayerHealth>().ActivateInvicibility(SettingSerialized.invulnerableTime);
        player.GetComponent<PlayerController>().Respawn();
        
        OnRespawn?.Invoke(this, playerIndex);
    }

    protected void Update()
    {

        _roundTimer.Update(Time.deltaTime);
        _roundTimerSoon.Update(Time.deltaTime);
        _slowTimeCooldown.Update(Time.unscaledDeltaTime);
        
        for (int i = 0; i < 4; i++)
        {
            _respawnCooldowns[i].Update(Time.deltaTime);
        }
        
        //TODO: optimize this
        textTimerRound.text = ((int)_roundTimer.TimeRemaining / 60).ToString("00") + " : " + ((int)(_roundTimer.TimeRemaining % 60)).ToString("00");
    }

    private void OnEndTimeOfRound()
    {
        EndOfTheMatch();
    }

    public override void Win(List<int> winners)
    {
        base.Win(winners);
        
        _roundTimer.Pause();
        
        EndOfTheMatch();
    }

    private void EndOfTheMatch()
    {
        _slowTimeCooldown.Start();
       Time.timeScale = SettingSerialized.slowTimeValue;

       playerScores.Sort();
        
       scoreUI.SetActive(true);

        //Sound
        PlayerAudioManager winnerAudioManager = AudioManager.instance.GetPlayerAudioManager(playerScores[0].player.playerIndex);
        switch (winnerAudioManager.characterName)
        {
            case "Stanislas":
                AkSoundEngine.SetState("STATE_General_Winner", "Stan");
                break;

            case "Marta":
                AkSoundEngine.SetState("STATE_General_Winner", "Marta");
                break;

            case "Medusa":
                AkSoundEngine.SetState("STATE_General_Winner", "Medusa");
                break;

            case "Don":
                AkSoundEngine.SetState("STATE_General_Winner", "Don");
                break;
        }
        MusicManager.instance.SetMusicState(MusicState.Outro);
        //Sound

        OnMatchEnd?.Invoke(this, playerScores);

	}

    protected override void PlayerJoined(PlayerInput obj)
    {
        base.PlayerJoined(obj);

        var ct = obj.GetComponent<CharacterController>();
        
        ct.enabled = false;
        obj.transform.position = spawnPoints[obj.playerIndex].position;
        obj.transform.rotation = spawnPoints[obj.playerIndex].rotation;
        ct.enabled = true;
        
        //Sound
        AudioManager.instance.Listeners.SetValue(obj.camera.gameObject, obj.playerIndex);
        //Sound

        if (isMatchStarted)
            obj.SwitchCurrentActionMap("Arena");
        else
            obj.SwitchCurrentActionMap("Death");

        _manager.playerPrefab = _settings.playerPrefab[SettingSerialized.playersPrefabIndex[(obj.playerIndex + 1) % 4]].gameObject;
        playerScores.Add(new FinalScoreHandler(0, obj));
        
        InitPlayersCamera();
    }

    //Creating this because the unity's system doesn't work
    private void InitPlayersCamera()
    {
        for (int i = 0; i < players.Count; i++)
        {
            switch (i)
            {
                case 0:
                    players[0].GetComponentInChildren<Camera>().rect = new Rect(0f, 0.5f, 0.5f, 0.5f);
                    break;
                case 1:
                    players[1].GetComponentInChildren<Camera>().rect = new Rect(0.5f, 0.5f, 0.5f, 0.5f);
                    break;
                case 2:
                    players[2].GetComponentInChildren<Camera>().rect = new Rect(0f, 0f, 0.5f, 0.5f);
                    break;
                case 3:
                    players[3].GetComponentInChildren<Camera>().rect = new Rect(0.5f, 0f, 0.5f, 0.5f);
                    break;
            }
        }
    }
}
