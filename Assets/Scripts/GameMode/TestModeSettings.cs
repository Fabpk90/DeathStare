using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

[Serializable]
public class TestModeSettingsHelper : GameModeSettingsHelper
{
    private static TestModeSettingsHelper Instance;
    public float secondsInRound = 300;
    public float secondsInSlowTime = 3;
    public float slowTimeValue = 0.7f;
    public float invulnerableTime = 0.0f;
    public float respawnCooldown = 1.0f;
    public uint killsToWin = 10;

    public static TestModeSettingsHelper GetInstance()
    {
        if (Instance == null)
        {
            Instance = new TestModeSettingsHelper();
            Instance.CheckAndLoad();
        }

        return Instance;
    }

    protected override void CheckAndLoad()
    {
        base.CheckAndLoad();
        if (File.Exists(Application.persistentDataPath + "/TestModeSettings.data"))
        {
            FileStream file = File.OpenRead(Application.persistentDataPath + "/TestModeSettings.data");
            BinaryFormatter bf = new BinaryFormatter();

            TestModeSettingsHelper ts = (TestModeSettingsHelper) bf.Deserialize(file);
            secondsInRound = ts.secondsInRound;
            secondsInSlowTime = ts.secondsInSlowTime;
            slowTimeValue = ts.slowTimeValue;
            invulnerableTime = ts.invulnerableTime;
            respawnCooldown = ts.respawnCooldown;
            killsToWin = ts.killsToWin;
            
            file.Close();
        }
        else
        {
            var file = File.Create(Application.persistentDataPath + "/TestModeSettings.data");
            
            BinaryFormatter bf = new BinaryFormatter();
            bf.Serialize(file, this);
            file.Close();
        }
    }

    public override void Save()
    {
        base.Save();
        var file = File.Create(Application.persistentDataPath + "/TestModeSettings.data");
            
        BinaryFormatter bf = new BinaryFormatter();
        bf.Serialize(file, this);
        file.Close();
    }
}

[CreateAssetMenu(menuName = "GameMode/TestModeSettings")]
public class TestModeSettings : GameModeSettings
{
    public float secondsInRound = 300;
    public float secondsInSlowTime = 3;
    public float slowTimeValue = 0.7f;
    public float invulnerableTime = 0.0f;
    public float respawnCooldown = 1.0f;
    public uint killsToWin = 10;
}
