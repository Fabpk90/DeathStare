using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using Actor;
using UnityEngine;
using UnityEngine.InputSystem;

[Serializable]
public class GameModeSettingsHelper
{
    public int[] playersPrefabIndex = {0, 1, 2, 3};

    public virtual void Save()
    {
        var file = File.Create(Application.persistentDataPath + "/GameModeSettings.data");

        BinaryFormatter bf = new BinaryFormatter();
        bf.Serialize(file, this);
        file.Close();
    }
    
    protected virtual void CheckAndLoad()
    {
        if (File.Exists(Application.persistentDataPath + "/GameModeSettings.data"))
        {
            FileStream file = File.OpenRead(Application.persistentDataPath + "/GameModeSettings.data");
            BinaryFormatter bf = new BinaryFormatter();

            var gm = (GameModeSettingsHelper) bf.Deserialize(file);
            playersPrefabIndex = gm.playersPrefabIndex;
            
            file.Close();
        }
        else
        {
            var file = File.Create(Application.persistentDataPath + "/GameModeSettings.data");

            BinaryFormatter bf = new BinaryFormatter();
            bf.Serialize(file, this);
            file.Close();
        }
    }
}

public class GameModeSettings : ScriptableObject
{
    public InputActionAsset[] playerInputs;
    public ActorCameraMovementSettings[] playerSensitivities;
    public PlayerInput[] playerPrefab;
    
    public Color[] playerColors;
}
