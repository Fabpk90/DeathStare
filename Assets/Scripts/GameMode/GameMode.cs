﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.LowLevel;

[RequireComponent(typeof(PlayerInputManager))]
public class GameMode : MonoBehaviour
{
    public static GameMode instance;

    public List<PlayerInput> players;
    protected PlayerInputManager _manager;
    private List<InputDevice> _devicesDisconnected;

    public GameModeSettings Settings;
    public bool isMatchStarted;
    
    public static event EventHandler<Tuple<int, int>> OnKillEvent;
    public event EventHandler OnEndOfMatch;
    public static event EventHandler OnStartOfMatch;
    public static event EventHandler<PlayerInput> OnPlayerJoin;
	public static bool matchIsRunning;

    private void Start()
    {
        if (instance != null) return;
        
        instance = this;

        _manager = GetComponent<PlayerInputManager>();

        _manager.onPlayerJoined += OnPlayerJoined;

        InputSystem.onDeviceChange += OnDeviceChange;

        _devicesDisconnected = new List<InputDevice>(4);
        isMatchStarted = false;

        MusicManager.OnMatchStart += StartOfMatch;
        
        Init();
    }

    protected virtual void StartOfMatch(object sender, EventArgs e)
    {
        OnStartOfMatch?.Invoke(this, EventArgs.Empty);
		matchIsRunning = true;
		isMatchStarted = true;
	}

    public void PlayerSpawn(PlayerInput obj)
    {
        OnPlayerJoin?.Invoke(this, obj);
    }

    private void OnDeviceChange(InputDevice arg1, InputDeviceChange arg2)
    {
        switch (arg2)
        {
            case InputDeviceChange.Added:
                if (players.Count < 4 && arg1 is Gamepad && !_devicesDisconnected.Contains(arg1))
                {
                    print("Connecting " + arg1);
                    _manager.JoinPlayer(players.Count, -1, "GamePads", arg1);
                }
                break;
            case InputDeviceChange.Removed:
                break;
            case InputDeviceChange.Disconnected:
                _devicesDisconnected.Add(arg1);
                print("Disconnecting " + arg1);
                
                break;
            case InputDeviceChange.Reconnected:
                print("Reconnected");
                break;
            case InputDeviceChange.Enabled:
                break;
            case InputDeviceChange.Disabled:
                break;
            case InputDeviceChange.UsageChanged:
                break;
            case InputDeviceChange.ConfigurationChanged:
                break;
            case InputDeviceChange.Destroyed:
                break;
            default:
                throw new ArgumentOutOfRangeException(nameof(arg2), arg2, null);
        }
    }


    protected virtual void Disable()
    {
        _manager.onPlayerJoined -= OnPlayerJoined;
        InputSystem.onDeviceChange -= OnDeviceChange;
    }

    private void OnDisable()
    {
        Disable();
    }

    public void PlayerKilled(int playerIndexKiller, int playerIndexKilled)
    {
        OnKillEvent?.Invoke(this, new Tuple<int, int>(playerIndexKiller, playerIndexKilled));
    }
    

    private void OnPlayerJoined(PlayerInput obj)
    {
        PlayerJoined(obj);
    }

    protected virtual void PlayerJoined(PlayerInput obj)
    {
        Debug.Log("Player " + obj.playerIndex + " joined !");

        //to see clearly in the inspector which player it is
        obj.gameObject.name = "Player " + obj.playerIndex;
    
        players.Add(obj);
    }

    public virtual void Init()
    {
        
    }

    public virtual void Win(List<int> winners)
    {
		matchIsRunning = false;
		OnEndOfMatch?.Invoke(this, null);
    }
}
