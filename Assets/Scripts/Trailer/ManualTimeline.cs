﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Timeline;
using UnityEngine.Playables;

public class ManualTimeline : MonoBehaviour
{
	private PlayableDirector playable;
	private float frameCount = 0;
	public bool run = true;
	public float mult = 0.2f;
	public float startFrame = -10;

	private void Awake()
	{
		Time.timeScale = 0;
		playable = GetComponent<PlayableDirector>();
		frameCount = startFrame;
	}

	void LateUpdate()
    {
		if (frameCount >= 60)
		{
			run = false;
		}
		if (!run) return;	
		playable.time = frameCount;
		playable.Evaluate();
		frameCount += .016f;
    }
}
