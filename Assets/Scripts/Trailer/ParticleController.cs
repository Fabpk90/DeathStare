﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleController : MonoBehaviour
{
	private ParticleSystem ps;
	public float simTarget = 0.2f;
	private float prevSimTarget = -1;

	private void Awake()
	{
		ps = GetComponent<ParticleSystem>();
	}

	private void Update()
	{
		if (prevSimTarget != simTarget)
		{
			ps.Simulate(simTarget, true, true);
		}
		prevSimTarget = simTarget;
	}
}
