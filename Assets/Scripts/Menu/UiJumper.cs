﻿using System.Collections;
using System.Collections.Generic;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem.UI;
using UnityEngine.UI;

public class UiJumper : MonoBehaviour
{
    public PlayerUISpawner uiManager;
    public GameObject onSubmitSelection;


    public void JumpToGameObject(int index)
    {
        uiManager.PlayerInputs[index].GetComponent<MultiplayerEventSystem>().SetSelectedGameObject(onSubmitSelection);
        if (onSubmitSelection.GetComponent<Button>())
        {
            onSubmitSelection.GetComponent<Button>().OnSelect(new BaseEventData(EventSystem.current));
        }
    }
}
