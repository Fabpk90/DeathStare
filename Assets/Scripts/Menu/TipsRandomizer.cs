using System;
using TMPro;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Menu
{
    public class TipsRandomizer : MonoBehaviour
    {
        public TipsStrings Tips;

        public TextMeshProUGUI Text;
        private void Awake()
        {
            Text.text = Tips.tips[Random.Range(0, Tips.tips.Length)];
        }
    }
}