using System;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

namespace Menu
{
    public class LoadNextLevelOnButtonPressed : MonoBehaviour
    {
        public float timeToWait;
        private CooldownTimer _timer;
        private bool _isSceneLoaded;
        private AsyncOperation _scene;
        public GameObject toShowWhenLoaded;
        private void Start()
        {
            _scene = null;

            _timer = new CooldownTimer(timeToWait);
            _timer.TimerCompleteEvent += () => toShowWhenLoaded.SetActive(true);
            _timer.Start();
        }

        private void Update()
        {
            if (_scene == null)
            {
                _scene =  SceneManager.LoadSceneAsync("FinalOpti");
                _scene.allowSceneActivation = false;
            }

            if (_scene.progress <= 0.9f)
            {
                _isSceneLoaded = true;
            }
            
            _timer.Update(Time.deltaTime);
            if (Gamepad.current.buttonSouth.isPressed && _isSceneLoaded && _timer.IsCompleted)
            {
                _scene.allowSceneActivation = true;
            }
        }
    }
}