﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.UI;
using UnityEngine.UI;

public class CancelButtonNavigation : MonoBehaviour
{
    public InputSystemUIInputModule uiModule;
    public Button CancelButton;

    public Button SecondCancelButton;

    private bool isFirstButtonSelected = true;
    // Start is called before the first frame update
    void OnEnable()
    {
        foreach (Transform tr in uiModule.GetComponentInParent<Canvas>().transform)
        {
            if (tr.name == "Back")
            {
                CancelButton = tr.GetComponent<Button>();
                break;
            }
        }
        
        uiModule.cancel.action.performed += OnCancel;
    }

    private void OnDisable()
    {
        uiModule.cancel.action.performed -= OnCancel;
    }

    private void OnCancel(InputAction.CallbackContext obj)
    {
        if(isFirstButtonSelected)
            CancelButton.OnSubmit(new BaseEventData(EventSystem.current));
        else
            SecondCancelButton.OnSubmit(new BaseEventData(EventSystem.current));
    }

    public void ChangeButton()
    {
        isFirstButtonSelected = !isFirstButtonSelected;
    }
}
