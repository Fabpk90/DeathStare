using System;
using System.Collections.Generic;
using Actor;
using TMPro;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;

namespace Menu
{
    public class InputRemapper : MonoBehaviour
    {
        public string playerIndex;
        public string[] ButtonBindingName;
        public Button[] Buttons;
        public InputActionAsset ActionMap;
        public ActorCameraMovementSettings CameraMovementSettings;

        public Slider sliderSensibility;
        public TextMeshProUGUI textSlider;

        public Button inverseYButton;
        
        private Dictionary<Guid, string> _overrides = new Dictionary<Guid, string>();
        private CameraMovementSerializeSettings _settings;
        
        private void Awake()
        {
            _settings = new CameraMovementSerializeSettings();
            _settings.CheckAndLoad(playerIndex);
            
            sliderSensibility.value = _settings.sensitivity;
            sliderSensibility.maxValue = 2.0f;
            textSlider.text = "Sensi Val: " + sliderSensibility.value.ToString("F1");
            
            sliderSensibility.onValueChanged.AddListener(val =>
            {
                _settings.sensitivity = val;
                textSlider.text = "Sensi Val: " + val.ToString("F1");
            });
            
            inverseYButton.onClick.AddListener(() =>
            {
                _settings.inverseVertical =
                    !_settings.inverseVertical;

                inverseYButton.GetComponentInChildren<TextMeshProUGUI>().text =
                    _settings.inverseVertical ? "X" : " ";
            });
            
            inverseYButton.GetComponentInChildren<TextMeshProUGUI>().text =
                _settings.inverseVertical ? "X" : " ";
            
            // Loading.
            foreach (var map in ActionMap.actionMaps)
            {
                var bindings = map.bindings;
                for (var i = 0; i < bindings.Count; ++i)
                {
                    if (_overrides.TryGetValue(bindings[i].id, out var overridePath))
                        map.ApplyBindingOverride(i, new InputBinding { overridePath = overridePath });
                }
            }
        }

        private void OnDisable()
        {
            sliderSensibility.onValueChanged.RemoveAllListeners();
            
           _settings.Save();
        }

        public void StartRebinding(int index)
        {
            Buttons[index].GetComponentInChildren<TextMeshProUGUI>().text = "...";
            print("Starting rebinding");
            ActionMap.FindAction(ButtonBindingName[index]).PerformInteractiveRebinding().Start()
                .OnComplete(operation =>
            {
                Buttons[index].GetComponentInChildren<TextMeshProUGUI>().text = operation.selectedControl.displayName;
                operation.Dispose();
                
                // Saving.
                foreach (var map in ActionMap.actionMaps)
                foreach (var binding in map.bindings)
                {
                    if (!string.IsNullOrEmpty(binding.overridePath))
                        _overrides[binding.id] = binding.overridePath;
                }
            });
        }
    }
}