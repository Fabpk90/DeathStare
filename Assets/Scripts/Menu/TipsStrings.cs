using UnityEngine;

namespace Menu
{
    [CreateAssetMenu(menuName = "Menu/Tips")]
    public class TipsStrings : ScriptableObject
    {
        [Multiline]
        public string[] tips;
    }
}