﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.UI;
using UnityEngine.UI;

public class PlayerUISpawner : MonoBehaviour
{
    public PlayerInput playerPrefabUI;
    public GameObject root;
    public GameObject[] firstSelectedParents;

    public List<PlayerInput> PlayerInputs;
    private Dictionary<InputDevice, int> _devicesLost;
    public int playersConnected;

    private PlayerInputManager _manager;

    public GameObject[] playersMenu;
    public Button[] playersOptionsBackButton;
    
    public UiWaker Waker;
    private void OnEnable()
    {
        InputSystem.onDeviceChange += OnDeviceChange;
        _manager = GetComponent<PlayerInputManager>();
        _manager.onPlayerJoined += OnPlayerJoined;
    }

    private void OnDeviceChange(InputDevice arg1, InputDeviceChange arg2)
    {
        switch (arg2)
        {
            case InputDeviceChange.Added:
                if(!_devicesLost.ContainsKey(arg1))
                    _manager.JoinPlayer(_manager.playerCount, -1 ,playerPrefabUI.currentControlScheme, arg1);
                break;
            case InputDeviceChange.Removed:
                break;
            case InputDeviceChange.Disconnected:
                for (int i = 0; i < PlayerInputs.Count; i++)
                {
                    if (PlayerInputs[i].devices.Count == 0)
                    {
                        if (!_devicesLost.ContainsKey(arg1))
                            _devicesLost.Add(arg1, i);
                        else
                            _devicesLost[arg1] = i;
                        
                        playersMenu[i].SetActive(false);
                        playersConnected--;
                    }
                }
                break;
            case InputDeviceChange.Reconnected:
                if (_devicesLost.ContainsKey(arg1))
                {
                    playersMenu[_devicesLost[arg1]].SetActive(true);
                    playersConnected++;
                }
                break;
            case InputDeviceChange.Enabled:
                break;
            case InputDeviceChange.Disabled:
                break;
            case InputDeviceChange.UsageChanged:
                break;
            case InputDeviceChange.ConfigurationChanged:
                break;
            case InputDeviceChange.Destroyed:
                break;
            default:
                throw new ArgumentOutOfRangeException(nameof(arg2), arg2, null);
        }
    }

    public void DestroyGamepad(int i)
    {
        playersMenu[i].SetActive(false);
		            
        List<InputDevice> devicesToRemove = new List<InputDevice>();
        foreach (KeyValuePair<InputDevice,int> pair in _devicesLost)
        {
            if (PlayerInputs[pair.Value].devices.Count == 0)
            {
                devicesToRemove.Add(pair.Key);
            }
        }

        foreach (InputDevice device in devicesToRemove)
        {
            _devicesLost.Remove(device);
        }
        PlayerInputs.RemoveAt(i);
    }

    private void Start()
    {
        _manager.playerPrefab = playerPrefabUI.gameObject;
        playersConnected = 0;

        _devicesLost = new Dictionary<InputDevice, int>();

        PlayerInputs = new List<PlayerInput>(4);
        
        for (int i = 0; i < Gamepad.all.Count; i++)
        {
            _manager.JoinPlayer(i, -1, playerPrefabUI.currentControlScheme, Gamepad.all[i]);
        }
        
    }

    public void ChangeBackButtonOfPlayer(int index)
    {
        PlayerInputs[index].GetComponent<CancelButtonNavigation>().ChangeButton();
    }
    

    private void OnPlayerJoined(PlayerInput obj)
    {
        //TODO: add the ui dynamically ?
        obj.GetComponent<MultiplayerEventSystem>().firstSelectedGameObject = firstSelectedParents[obj.playerIndex].gameObject;
        
        PlayerInputs.Add(obj);
        obj.transform.parent = transform.parent;
        Waker.Systems.Add(obj.GetComponent<MultiplayerEventSystem>());
        playersMenu[obj.playerIndex].SetActive(true);

        obj.GetComponent<CancelButtonNavigation>().SecondCancelButton = playersOptionsBackButton[obj.playerIndex];
        
        playersConnected++;
    }

    private void OnDisable()
    {
        InputSystem.onDeviceChange -= OnDeviceChange;
        _manager.onPlayerJoined -= OnPlayerJoined;
    }

	public void OpenPlayerNav()
	{
		for(int i = 0; i< PlayerInputs.Count; i++)
		{
			playersMenu[i].SetActive(true);
		}
	}
}
