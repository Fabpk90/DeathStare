using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Menu
{
    public class MenuSelector : MonoBehaviour
    {
        public TestModeSettings Settings;
        private TestModeSettingsHelper _settings;

        public GameObject[] playersModels;
        public Button[] buttons;

        public GameObject[] spawnPoints;
        private GameObject[] _spawnedModel;
        
        private List<int> _playersReady;
        public Button StartButton;

        public List<int> playersPrefabIndex;
        public PlayerInputManager Manager;
        private PlayerUISpawner _spawner;
        private List<PlayerInput> inputs;
		[Space]
		public TextMeshProUGUI scoreLimitText;
		public TextMeshProUGUI timeLimitText;
		private int scoreLimitId = 2;
		private int timeLimitId = 2;
		public int[] scoreLimitValues = new int[] { 1,5, 10, 20, 50 };
		public float[] timeLimitValues = new float[] {60,210,300,600,1800,9999};
		[Space]
		public TextMeshProUGUI[] characterName;
		public string[] prefabName = new string[] { "Don", "Marta", "Medusa", "Stanislas" };
		[Space]
		public GameObject[] selectArrows;

		private void Awake()
		{
			_settings = TestModeSettingsHelper.GetInstance();
			inputs = new List<PlayerInput>();
			_spawner = Manager.GetComponent<PlayerUISpawner>();
            
			StartButton.gameObject.SetActive(false);
			_playersReady = new List<int>(4);
			_spawnedModel = new GameObject[4];
			playersPrefabIndex = new List<int>(4);

           
            
			for (int i = 0; i < Settings.playerPrefab.Length; i++)
			{
				playersPrefabIndex.Add(_settings.playersPrefabIndex[i]);
			}

			for (int i = 0; i < buttons.Length; i++)
			{
				buttons[i].GetComponentInChildren<TextMeshProUGUI>().text = "Not Ready";
				var i1 = i;
				buttons[i].onClick.AddListener(() =>
				{
					PlayerReady(i1, i1);
				});
			}

			InitScoreTimeLimits();
		}

		private void OnEnable()
		{
			Manager.onPlayerJoined += OnPlayerJoin;
		}

		private void OnDisable()
        {
			Manager.onPlayerJoined -= OnPlayerJoin;
            SaveSettingsToSO();

            for (int i = 0; i < inputs.Count; i++)
            {
	            if (i >= Gamepad.all.Count)
	            {
		            Destroy(_spawnedModel[i]);

		            _spawner.DestroyGamepad(i);
		            Destroy(inputs[i].gameObject);
		            inputs.RemoveAt(i);
	            }
            }
        }

        private void OnPlayerJoin(PlayerInput obj)
        {
	        inputs.Add(obj);
	        _spawnedModel[obj.playerIndex] = Instantiate(playersModels[playersPrefabIndex[obj.playerIndex]],
                spawnPoints[obj.playerIndex].transform.position,
                spawnPoints[obj.playerIndex].transform.rotation);
			SetName(obj.playerIndex, playersPrefabIndex[obj.playerIndex]);
		}

        public void CheckForEnablingStartButton()
        {
            if (_playersReady.Count == PlayerInputManager.instance.playerCount)
            {
                StartButton.gameObject.SetActive(true);
            }
        }

        public void SaveSettingsToSO()
        {
            for(int i = 0; i < Settings.playerPrefab.Length; i++)
            {
                _settings.playersPrefabIndex[i] = playersPrefabIndex[i];
            }

            _settings.Save();
        }

        public void PlayerChangeModel(int indexPlayer, bool isPlus)
        {
            Destroy(_spawnedModel[indexPlayer]);
            if (isPlus)
            {
                playersPrefabIndex[indexPlayer] = (playersPrefabIndex[indexPlayer] + 1) % playersPrefabIndex.Count;
            }
            else
            {
                if (playersPrefabIndex[indexPlayer] - 1 < 0)
                    playersPrefabIndex[indexPlayer] = playersPrefabIndex.Count - 1;
                else
                    playersPrefabIndex[indexPlayer]--;
            }

			SetName(indexPlayer, playersPrefabIndex[indexPlayer]);
            _spawnedModel[indexPlayer] = Instantiate(playersModels[playersPrefabIndex[indexPlayer]],
                spawnPoints[indexPlayer].transform.position, spawnPoints[indexPlayer].transform.rotation);
        }

        private void PlayerReady(int index, int indexBtn)
        {
            var btn = buttons[indexBtn].GetComponent<Button>();
            if (!_playersReady.Contains(index))
            {
                btn.GetComponentInChildren<TextMeshProUGUI>().text = "Ready";
                var c = btn.colors;
                c.selectedColor = Color.green;
                btn.colors = c;
                _playersReady.Add(index);
                if (_playersReady.Count == _spawner.playersConnected)
                {
                    SaveSettingsToSO();
                    SceneManager.LoadScene(1);
                }
				ActiveSelect(index, false);
            }
            else
            {
                btn.GetComponentInChildren<TextMeshProUGUI>().text = "Not Ready";
                var c = btn.colors;
                c.selectedColor = Color.red;
                btn.colors = c;
                _playersReady.Remove(index);
                StartButton.gameObject.SetActive(false);
				ActiveSelect(index, true);
			}
        }

		public void ScoreLimitClick()
		{
			scoreLimitId = (scoreLimitId + 1) % scoreLimitValues.Length;
			_settings.killsToWin = (uint)scoreLimitValues[scoreLimitId];
			if (scoreLimitValues[scoreLimitId] > 1)
			{
				scoreLimitText.text = scoreLimitValues[scoreLimitId] + " kills";
			}
			else
			{
				scoreLimitText.text = scoreLimitValues[scoreLimitId] + " kill";
			}
		}

		public void TimeLimitClick()
		{
			timeLimitId = (timeLimitId + 1) % timeLimitValues.Length;
			_settings.secondsInRound = timeLimitValues[timeLimitId];
			timeLimitText.text = ((int)(timeLimitValues[timeLimitId]/60f)).ToString("00") + " : " + (timeLimitValues[timeLimitId] % 60).ToString("00");
		}

		public void InitScoreTimeLimits()
		{
			_settings.killsToWin = (uint)scoreLimitValues[scoreLimitId];
			if(scoreLimitValues[scoreLimitId] > 1)
			{
				scoreLimitText.text = scoreLimitValues[scoreLimitId] + " kills";
			}
			else
			{
				scoreLimitText.text = scoreLimitValues[scoreLimitId] + " kill";
			}

			_settings.secondsInRound = timeLimitValues[timeLimitId];
			timeLimitText.text = ((int)(timeLimitValues[timeLimitId] / 60f)).ToString("00") + " : " + (timeLimitValues[timeLimitId] % 60).ToString("00");
		}

		public void SetName(int playerIndex, int prefabIndex)
		{
			characterName[playerIndex].text = prefabName[prefabIndex];
		}

		public void ActiveSelect(int index,bool b)
		{
			selectArrows[index*2].SetActive(b);//0 - 2 - 4 - 6
			selectArrows[index*2 +1].SetActive(b);//1 - 3 - 5 -7
		}
	}
}