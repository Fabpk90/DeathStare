﻿using System.Collections;
using System.Collections.Generic;
using Menu;
using UnityEngine;
using UnityEngine.UI;

public class ButtonPlayerSelector : MonoBehaviour
{
    private Button _btn;

    public int index;
    public MenuSelector MenuSelector;
    public bool isPlus;
    
    // Start is called before the first frame update
    void Start()
    {
        _btn = GetComponent<Button>();
        
        _btn.onClick.AddListener(() =>
        {
            MenuSelector.PlayerChangeModel(index, isPlus);
        });
    }
}
