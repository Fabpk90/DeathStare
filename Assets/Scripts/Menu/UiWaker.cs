﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class UiWaker : MonoBehaviour
{
    public List<EventSystem> Systems = new List<EventSystem>();

    private void OnEnable()
    {
        foreach (EventSystem system in Systems)
        {
            if (system.currentSelectedGameObject)
            {
                system.SetSelectedGameObject(system.firstSelectedGameObject);
                system.currentSelectedGameObject.GetComponent<Selectable>().OnSelect(new BaseEventData(EventSystem.current));
            }
        }
    }
}
