﻿using System.Collections;
using System.Collections.Generic;
using Cinemachine;
using UnityEngine;

public class CameraCinemachineController : MonoBehaviour
{
    public CinemachineVirtualCamera Camera;

    public float movementDuration;
    public AnimationCurve movementCurve;
    private CooldownTimer _timer;

    private bool _isGoingForward;
    public int _startingIndex;

    public GameObject[] Canvases;

    private CinemachineTrackedDolly _dolly;
    private bool _once;
    
    // Start is called before the first frame update
    void Start()
    {
        _startingIndex = 0;
        _isGoingForward = true;
        _timer = new CooldownTimer(movementDuration);
        _dolly = Camera.GetCinemachineComponent<CinemachineTrackedDolly>();
        Canvases[(int)_startingIndex].SetActive(true);
    }

    public void GoBack()
    {
        _isGoingForward = false;
        StartMoving();
    }

    public void GoForward()
    {
        _isGoingForward = true;
        StartMoving();
    }

    private void StartMoving()
    {
        _timer.Start();
        Canvases[_startingIndex].SetActive(false);
        _once = true;
    }

    // Update is called once per frame
    void Update()
    {
        _timer.Update(Time.deltaTime);
        if (_timer.IsActive)
        {
            if (_timer.PercentElapsed >= 0.8f && _once)
            {
                _startingIndex = Mathf.RoundToInt(_dolly.m_PathPosition);
                Canvases[_startingIndex].SetActive(true);
                
                _once = false;
            }
            _dolly.m_PathPosition = _startingIndex + movementCurve.Evaluate(_timer.PercentElapsed) * (!_isGoingForward ? -1 : 1);
        }
    }
}
