﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class ChangeModel : MonoBehaviour
{
    public GameObject[] listPrefab;
    private PlayerInputManager _manager;

    private int indexPlayer = 0;
    private PlayerInput _player;

    // Start is called before the first frame update
    void Start()
    {
        _manager = PlayerInputManager.instance;
        _manager.playerPrefab = listPrefab[0];
        
        _manager.onPlayerJoined += OnPlayerJoined;

        _manager.JoinPlayer(0, 0, "GamePads", Gamepad.current);
    }

    private void OnPlayerJoined(PlayerInput obj)
    {
        _player = obj;
        _player.gameObject.transform.position = listPrefab[indexPlayer].transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            ChangePlayer();
        }
    }

    private void ChangePlayer()
    {
        Destroy(_player.gameObject);
        indexPlayer = (indexPlayer + 1) % listPrefab.Length; 
        _manager.playerPrefab = listPrefab[indexPlayer];
        _manager.JoinPlayer(0, 0, "GamePads", Gamepad.current);
    }
}
