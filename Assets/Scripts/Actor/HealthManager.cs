using System;
using System.Collections.Generic;
using Actor.Hittable;
using UnityEngine;

namespace Actor
{
    public abstract class HealthManager : MonoBehaviour, IHittable
    {
        public float maxHealth;
        public float health;

        public List<HittablePoint> points;
        public event EventHandler<float> OnHealthRegen;

        protected virtual void OnAwake()
        {
            health = maxHealth;
        }

        private void Awake()
        {
            OnAwake();
        }

        public Vector3 GetPosition()
        {
            return transform.position;
        }

        public virtual bool RegenHealth(float amount)
        {
            if(Math.Abs(health - maxHealth) < 0.001f) return false;
            
            if (amount + health <= maxHealth)
            {
                health += amount;
            }
            else
            {
                health = maxHealth;
            }
            
            OnHealthRegen?.Invoke(this, health);
            return true;
        }

        public virtual EHitType TakeDamage(int playerIndex, float amount)
        {
            if (health - amount <= 0)
            {
                health -= amount;
                Die();
                return EHitType.DEATH;
            }

            health -= amount;
        
            return EHitType.TOUCHED;
        }

        protected virtual void Die()
        {
            foreach (HittablePoint point in points)
            {
                StareHandler.HittablePoints.Remove(point);
            }

        }

       
    }
}