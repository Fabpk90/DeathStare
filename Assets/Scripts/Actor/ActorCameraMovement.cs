using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

namespace Actor
{
    [Serializable]
    public class CameraMovementSerializeSettings
    {
        public bool inverseVertical = false;
        public float sensitivity = 1f;

        public string playerIndex;
        
        public void Save()
        {
            var file = File.Create(Application.persistentDataPath + "/" + playerIndex + "CameraSettings.data");

            BinaryFormatter bf = new BinaryFormatter();
            bf.Serialize(file, this);
            file.Close();
        }
    
        public void CheckAndLoad(string playerIndex)
        {
            this.playerIndex = playerIndex;
            
            if (File.Exists(Application.persistentDataPath + "/" + playerIndex + "CameraSettings.data"))
            {
                FileStream file = File.OpenRead(Application.persistentDataPath + "/" + playerIndex + "CameraSettings.data");
                BinaryFormatter bf = new BinaryFormatter();

                var cm = (CameraMovementSerializeSettings) bf.Deserialize(file);
                inverseVertical = cm.inverseVertical;
                sensitivity = cm.sensitivity;
            
                file.Close();
            }
            else
            {
                var file = File.Create(Application.persistentDataPath + "/" + playerIndex + "CameraSettings.data");

                BinaryFormatter bf = new BinaryFormatter();
                bf.Serialize(file, this);
                file.Close();
            }
        }
    }
    
    [Serializable]
    public class CameraMovementSettings
    {
        public float maxsensitivityWalkRun = 320;
        public Vector2 sensitivityAerial;
        public Vector2 sensitivityStare;
        
        public Vector2 angleYClamp;
        public Vector2 angleXClamp;
    }
    public class ActorCameraMovement : MonoBehaviour
    {
        public ActorCameraMovementSettings settings;
        private CameraMovementSerializeSettings _settingSerialized;
        public GameObject camParent;

        private PlayerController _controller;

        private Matrix4x4 _rotation;
        private Vector2 _delta;
        private float _xRotation = 0.0f;
        private float _yRotation = 0.0f;
        private Vector2 _sensitivity;

        private void Start()
        {
            _controller = GetComponentInParent<PlayerController>();
            
            _settingSerialized = new CameraMovementSerializeSettings();
            _settingSerialized.CheckAndLoad(_controller.GetPlayerIndex().ToString());
        }

        public void MoveCamera(Vector2 delta)
        {
            _delta = delta;
        }

        private Vector2 GetSensitivity()
        {
            if (_controller.controller.m_CharacterController.isGrounded)
            {
                if (_controller.controller.isStaring)
                    return settings.cameraSettings.sensitivityStare;

                return settings.cameraSettings.maxsensitivityWalkRun * _settingSerialized.sensitivity *
                       Vector2.one;
            }

            return settings.cameraSettings.sensitivityAerial * _settingSerialized.sensitivity;
        }

        private void Update()
        {
            _sensitivity = GetSensitivity() * Time.deltaTime;
            _xRotation += -_delta.y * (_settingSerialized.inverseVertical ? -1 : 1) * _sensitivity.y;
            _xRotation = Mathf.Clamp(_xRotation, settings.cameraSettings.angleYClamp.x, settings.cameraSettings.angleYClamp.y);

            _yRotation += _delta.x * _sensitivity.x;
            
            if(settings.cameraSettings.angleXClamp != Vector2.zero)
                _yRotation = Mathf.Clamp(_yRotation, settings.cameraSettings.angleXClamp.x, settings.cameraSettings.angleXClamp.y);

            transform.localRotation = Quaternion.Euler(_xRotation, 0f, 0f);
            camParent.transform.localRotation = Quaternion.Euler(Vector3.up * _yRotation);


			//transform.position = _controller.controller.animator.GetBoneTransform(HumanBodyBones.Head).position;
        }
    }
}