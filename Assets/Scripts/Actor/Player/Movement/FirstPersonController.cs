using System;
using System.Collections.Generic;
using Actor.Player.Movement;
using UnityEngine;
using UnityStandardAssets.Utility;

public enum ESurfaceType
{
	WOOD,
	CONCRETE,
	SAND
}

[RequireComponent(typeof(CharacterController))]
public class FirstPersonController : MonoBehaviour
{
    private static readonly int Jumping = Animator.StringToHash("Jumping");
    private static readonly int Crouching = Animator.StringToHash("Crouching");
    private static readonly int Running = Animator.StringToHash("Running");
    private static readonly int Walking = Animator.StringToHash("Walking");
    private static readonly int Staring = Animator.StringToHash("Staring");
    
    public bool isCrouching;

    public bool isRunning;

    public bool isStaring;

    public bool isOnFlatFloor;

    private float _startingHeightCollider;

    public CharacterController m_CharacterController;
    private CollisionFlags m_CollisionFlags;

    [SerializeField] private FOVKick m_FovKick = new FOVKick();
    
    [SerializeField] private CurveControlledBob m_HeadBob = new CurveControlledBob();
    private Vector2 m_Input;
    private bool m_IsWalking;
    private bool m_Jump;
    [SerializeField] private LerpControlledBob m_JumpBob = new LerpControlledBob();
    private bool m_Jumping;
    private Vector3 m_MoveDir = Vector3.zero;
    private float m_NextStep;
    private Vector3 m_OriginalCameraPosition;
    private bool m_PreviouslyGrounded;
    
    private float m_StepCycle;

    public MovementSettings settings;
    //private MovementSettingsHelper _settingsData;

    public Animator animator;

    private CooldownTimer jumpCooldown;
    private CooldownTimer stareCooldown;
    private static readonly int Velocity = Animator.StringToHash("Velocity");

    public GameObject feet;
    private static readonly int Backwards = Animator.StringToHash("Backwards");

    private Ray[] _rays = new Ray[5];
    public int minRayToGround = 3;
    public float maxDistanceToCheck = 0.01f;
    private static readonly int Falling = Animator.StringToHash("Falling");

	public Vector3 hitNormal;
	//public float slideSpeed = 6f;

	public LayerMask sphereCastLayerMask;
    
    [SerializeField,DisplayWithoutEdit]
    private Vector3 _velocity = Vector3.zero;
	private Vector3 _acceleration;
    private Vector3 _jumpNormal = Vector3.zero;
	private bool castDetection = false;


	//new jump
	private bool _jumpInput = false;
	private bool _isJumping = false;
	private float _jumpDuration = 0;
	private float _lastGroundedTime = 0;
	private bool _hasJump = false;
	//Stuck normal
	private Vector3 _controllerHitNormal;

	public bool isGrounded => m_CharacterController.isGrounded;
	public float lastGroundedTime => _lastGroundedTime;

	// public float angle;

	public event EventHandler OnLanding;
	public event EventHandler<ESurfaceType> OnWalkingOnSurface;

	[NonSerialized]
	public StareHandler Stare;

    // Use this for initialization
    private void Awake()
    {
        m_CharacterController = GetComponent<CharacterController>();

        _startingHeightCollider = m_CharacterController.height;
        
        //InitFromSO();
        
        jumpCooldown = new CooldownTimer(settings.parameters.jumpCooldown);
        jumpCooldown.Start();
        jumpCooldown.Update(settings.parameters.jumpCooldown);
        
        stareCooldown = new CooldownTimer(settings.parameters.stareCooldown);
        stareCooldown.Start();
        stareCooldown.Update(settings.parameters.stareCooldown);

        m_StepCycle = 0f;
        m_NextStep = m_StepCycle / 2f;

        ResetStates();
    }

    private void Start()
    {
	    Stare.OnStareStop += (sender, args) =>  SetStare(false); 
    }

    public void WalkingOnSurface(ESurfaceType type)
    {
	    OnWalkingOnSurface?.Invoke(this, type);
    }

    public void ResetStates()
    {
        m_Jumping = false;
        SetCrouch(false);
        isRunning = false;
        isStaring = false;
        
        animator.SetBool(Running, false);
        animator.SetBool(Staring, false);
        
        _velocity = Vector3.zero;
		_acceleration = Vector3.zero;
    }

    /// <summary>
    /// Loads all the params from a scriptable object
    /// </summary>
   /* private void InitFromSO()
    {
        _settingsData = settings.parameters;
        
        jumpCooldown = new CooldownTimer(_settingsData.jumpCooldown);
        jumpCooldown.Start();
        jumpCooldown.Update(_settingsData.jumpCooldown);
        
        stareCooldown = new CooldownTimer(_settingsData.stareCooldown);
        stareCooldown.Start();
        stareCooldown.Update(_settingsData.stareCooldown);
    }*/

    private void Update()
    {
        //if (!m_PreviouslyGrounded && m_CharacterController.isGrounded)
       // {
            //StartCoroutine(m_JumpBob.DoBobCycle());
           
           // m_MoveDir.y = 0f;
            //m_Jumping = false;
       // }

       /* if (!m_CharacterController.isGrounded && !m_Jumping && m_PreviouslyGrounded)
        {
            m_MoveDir.y = 0f;
        }*/

        
        
        jumpCooldown.Update(Time.deltaTime);
        stareCooldown.Update(Time.deltaTime);
    }

    private void PlayLandingSound()
    {
        AkSoundEngine.PostEvent("FOLEYS_Char_Jump_Landing", feet);
        m_NextStep = m_StepCycle + .5f;
		OnLanding?.Invoke(this,null);
    }



    private void FixedUpdate()
    {
        UpdateMovementState();
        UpdateAnimationSpeed();
        float speed = GetSpeedFromState() * m_Input.magnitude;
        
        Vector3 desiredMove = transform.forward * m_Input.y + transform.right * m_Input.x;

		Vector3 castOrigin = transform.position - Vector3.up * m_CharacterController.height / 2 + Vector3.up * m_CharacterController.radius;
		// get a normal for the surface that is being touched to move along it
		hitNormal = Vector3.up;
		RaycastHit hitInfo;
		castDetection = Physics.SphereCast(castOrigin, m_CharacterController.radius, Vector3.down, out hitInfo,m_CharacterController.radius * 1.1f, sphereCastLayerMask,QueryTriggerInteraction.Ignore);
		if (castDetection)
        {
            hitNormal = hitInfo.normal;
        }

		//if (Vector3.Angle(Vector3.up, hitNormal) < m_CharacterController.slopeLimit)
	//	if(m_CharacterController.isGrounded)
		//{
           desiredMove = Vector3.ProjectOnPlane(desiredMove, hitInfo.normal).normalized;

            m_MoveDir.x = desiredMove.x * speed;
            m_MoveDir.z = desiredMove.z * speed;
       // }
        
        animator.SetBool(Falling, !m_CharacterController.isGrounded);

        if (m_CharacterController.isGrounded && !_isJumping )
        {
			if(!_isJumping) m_MoveDir.y = -settings.parameters.stickToGroundForce;
			_lastGroundedTime = Time.time;
			_hasJump = false;
        }
        else
        {
            m_MoveDir += Physics.gravity * (settings.parameters.gravityMultiplier * Time.fixedDeltaTime);
        }

		//Debug.Log(m_CharacterController.isGrounded + "  " + castDetection);

		if (_jumpInput)
		{
		
			if (CanJump() && castDetection && !_isJumping)
			{
				//first frame jump
				_jumpDuration = 0;
				m_MoveDir.y =  settings.parameters.jumpSpeed;
				_isJumping = true;
				animator.SetBool(Falling, true);
				animator.SetTrigger(Jumping);
				PlayJumpSound();
				jumpCooldown.Start();
				_hasJump = true;
			}

			if (_isJumping)
			{
				//next jump frames
				_jumpDuration += Time.fixedDeltaTime;
				m_MoveDir.y += settings.parameters.jumpCurve.Evaluate(_jumpDuration / settings.parameters.jumpMaxDuration) * settings.parameters.jumpSpeed;
			}
		}else
		{
			_isJumping = false;
		}

		/*if(m_CharacterController.isGrounded && !castDetection)
		{
			//stuck on wall
			//do something to don't stick on this fucking wall
			Vector2 hN = new Vector2(_controllerHitNormal.x, _controllerHitNormal.z);
			Vector2 mD = new Vector2(m_MoveDir.x, m_MoveDir.z);
			float dot = Vector2.Dot(hN.normalized, mD.normalized);
			dot = Mathf.Clamp01(dot + 1);
			float y = m_MoveDir.y;
			m_MoveDir = m_MoveDir * dot + _controllerHitNormal* 10;
			m_MoveDir.y = y;
		}*/

		VelocityUpdate();
		m_CollisionFlags = m_CharacterController.Move((m_MoveDir + _velocity) * Time.fixedDeltaTime);
		if(!m_PreviouslyGrounded && m_CharacterController.isGrounded && _lastGroundedTime + 0.5f < Time.time)  PlayLandingSound();
		m_PreviouslyGrounded = m_CharacterController.isGrounded;
	}

	private void VelocityUpdate()
	{	
		if (castDetection) {
			_velocity.y = 0; //remove y vel when almost on floor
			_acceleration += -_velocity.normalized * (_velocity.sqrMagnitude * settings.parameters.groundedDrag * Time.fixedDeltaTime);//drag force
		}
		else
		{
			_acceleration += -_velocity.normalized * (_velocity.sqrMagnitude * settings.parameters.aerialDrag * Time.fixedDeltaTime);//drag force
		}
		_velocity += _acceleration; // add acceleration
		_acceleration = Vector3.zero; // reset acceleration
		if (castDetection && _velocity.sqrMagnitude < settings.parameters.groundVelocityThreshold*settings.parameters.groundVelocityThreshold) _velocity = Vector3.zero;//cut velocity under a threshold

	}

	private bool CanJump()
	{
		return (m_CharacterController.isGrounded 
		        || !_hasJump 
		        &&_lastGroundedTime + settings.parameters.fallDelay  > Time.time)
		        && jumpCooldown.IsCompleted;
	}

    private void UpdateAnimationSpeed()
    {
        float magnitude = m_Input.magnitude;

        if (m_CharacterController.isGrounded && magnitude > 0)
        {
            animator.SetFloat(Backwards, m_Input.y > 0 ? 1 : -1);
            animator.speed = Mathf.Clamp01(magnitude + 0.2f);
        }
        else
            animator.speed = 1.0f;
    }

    private void UpdateMovementState()
    {
        if (isStaring)
        {
            isRunning = false;
        }
        else if(m_Input.magnitude > 0.4f)
        {
            isRunning = true;
        }
        else
        {
            isRunning = false;
        }
        
        animator.SetBool(Running, isRunning);
    }

    public void SetInputMovement(Vector2 input)
    {
        m_Input = input;

        m_IsWalking = input != Vector2.zero;
        animator.SetBool(Walking, m_IsWalking);
        animator.SetFloat(Velocity, m_Input.magnitude);
    }

    private float GetSpeedFromState()
    {
        if (isStaring)
        {
            if (m_CharacterController.isGrounded)
                return m_Input.y > 0 ?settings.parameters.stareWalkingSpeedForward : settings.parameters.stareWalkingSpeedBackwards;

            return m_Input.y > 0 ? settings.parameters.stareAerialSpeedForward : settings.parameters.stareAerialSpeedBackwards;
        }
        if (!m_CharacterController.isGrounded)
            return m_Input.y > 0 ? settings.parameters.aerialSpeedForward : settings.parameters.aerialSpeedBackwards;
        if (isCrouching)
            return m_Input.y > 0 ? settings.parameters.crouchingSpeedForward : settings.parameters.crouchingSpeedBackwards;
        if (isRunning)
            return m_Input.y > 0 ? settings.parameters.runSpeedForward : settings.parameters.runSpeedBackwards;

        return m_Input.y > 0 ? settings.parameters.walkSpeedForward : settings.parameters.walkSpeedBackwards;
    }

    private void PlayJumpSound()
    {
        AkSoundEngine.PostEvent("FOLEYS_Char_Jump_TakeOff", feet);
        if (AudioManager.instance.GetPlayerAudioManager(Stare.GetPlayerIndex()))
        {
            AudioManager.instance.GetPlayerAudioManager(Stare.GetPlayerIndex()).PostEvent("VO_Char_Barks_Jump");
        }
    }

	public void SetJump(bool b)
	{
		_jumpInput = b;

		if (!b) return;
		//to limit the rounding error
		if (!(Math.Abs(jumpCooldown.TotalTime - settings.parameters.jumpCooldown) > 0.001f)) return;
			
		jumpCooldown = new CooldownTimer(settings.parameters.jumpCooldown);
		jumpCooldown.Start();
	}

   /* public void Jump()
    {
        if (m_CharacterController.isGrounded)
        {
            if(isCrouching)
                SetCrouch(false);
            
            m_Jump = true;

            PlayJumpSound();
        }
    }*/

    public bool CanStare()
    {
	    if (!stareCooldown.IsCompleted) return false;
	    
	    stareCooldown = new CooldownTimer(settings.parameters.stareCooldown);
        stareCooldown.Start();
        return true;

    }

   /* public bool CanJump()
    {
        if (jumpCooldown.IsCompleted)
        {
            jumpCooldown.Start();
            return true;
        }

        return false;
    }*/

    public void ToggleCrouch()
    {
        if(!m_CharacterController.isGrounded) return;
        isCrouching = !isCrouching;

        animator.SetBool(Crouching, isCrouching);

        if (isCrouching)
        {
            m_CharacterController.height = settings.parameters.crouchingHeightCollider;
            var transform1 = animator.transform;
            
            var v = transform1.localPosition;
            v.y /= 2;
            transform1.localPosition = v;
        }
        else
        {
            m_CharacterController.height = _startingHeightCollider;
            var transform1 = animator.transform;
            
            var v = transform1.localPosition;
            v.y *= 2;
            transform1.localPosition = v;
        }
    }

    public void SetCrouch(bool isCrouching)
    {
        if (isCrouching)
        {
            m_CharacterController.height = settings.parameters.crouchingHeightCollider;
            var transform1 = animator.transform;
            
            var v = transform1.localPosition;
            v.y /= 2;
            transform1.localPosition = v;
        }
        else if(this.isCrouching)
        {
            m_CharacterController.height = _startingHeightCollider;
            var transform1 = animator.transform;
            
            var v = transform1.localPosition;
            v.y *= 2;
            transform1.localPosition = v;
        }
        
        this.isCrouching = isCrouching;
        animator.SetBool(Crouching, isCrouching);
    }

    private void OnControllerColliderHit(ControllerColliderHit hit)
    {
		
		Rigidbody body = hit.collider.attachedRigidbody;
		//dont move the rigidbody if the character is on top of it
		_controllerHitNormal = hit.normal;

        if (m_CollisionFlags == CollisionFlags.Below)
        {
			//set the normal( when something is below)
			//hitNormal = hit.normal;
			return;
        }


        if (body == null || body.isKinematic)
        {
            return;
        }

        body.AddForceAtPosition(m_CharacterController.velocity * 0.1f, hit.point, ForceMode.Impulse);
    }

    public void SetStare(bool isStaring)
    {
        this.isStaring = isStaring;
        animator.SetBool(Staring, isStaring);

        if (isStaring)
        {
            SetCrouch(false);
        }
    }

	public Vector3 GetFeetPosition()
	{
		return transform.position - Vector3.up * m_CharacterController.height/2;
	}

	public void AddForce(Vector3 force)
	{
		_acceleration += force;
	}
}