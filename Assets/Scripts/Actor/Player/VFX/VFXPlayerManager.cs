﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets;

public class VFXPlayerManager : MonoBehaviour
{
	public ParticleSystem leftStep;
	public ParticleSystem rightStep;
	public ParticleSystem deathStareInit;
	public ParticleSystem deathStareTick;
	public ParticleSystem impactLeft;
	public ParticleSystem impactRight;
	public ParticleSystem landing;
	[Space]
	public StareHandler stare;
	public FirstPersonController fpc;

	private void OnEnable()
	{
		if(!stare) return;
		stare.OnStareStart += OnStareStart;
		fpc.OnLanding += OnLanding;
	}

	private void OnDisable()
	{
		if(!stare) return;
		stare.OnStareStart -= OnStareStart;
		fpc.OnLanding -= OnLanding;
	}

	/*private void Update()
	{
		leftStep.transform.rotation = Quaternion.identity;
		rightStep.transform.rotation = Quaternion.identity;
		landing.transform.rotation = Quaternion.identity;


	}*/

	private void OnStareStart(object sender, System.EventArgs e)
	{
		deathStareInit.Play();
		UpdateImpactParentRot();
		impactLeft.Play();
		impactRight.Play();
	}

	private void OnLanding(object sender, System.EventArgs e)
	{
		landing.transform.rotation = Quaternion.identity;
		landing.Play();
	}

	public void Landing()
	{
		landing.transform.rotation = Quaternion.identity;
		landing.Play();
	}

	public void PostLeftFootEvent(string eventName)
	{
		leftStep.transform.rotation = Quaternion.identity;
		leftStep.Play(true);

	}

	public void PostRightFootEvent(string eventName)
	{
		rightStep.transform.rotation = Quaternion.identity;
		rightStep.Play(true);

	}

	private void UpdateImpactParentRot()
	{
		Vector3 v = stare.Cam.transform.forward;
		v.y = 0;
		v = v.normalized;
		impactLeft.transform.parent.rotation = Quaternion.LookRotation(v, Vector3.up);
	}
}
