﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.Principal;
using Actor;
using Actor.Hittable;
using Actor.Player;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityStandardAssets.Characters.FirstPerson;

//TODO: make this more smart, abstract the calling away
//keep only the controller part here


[RequireComponent(typeof(PlayerInput))]
public class PlayerController : MonoBehaviour
{
    private PlayerInput _input;

    public FirstPersonController controller;
    
    public ActorCameraMovement cameraMovement;
    public StareHandler stareHandler;
    public PlayerHealth health;

    public GameObject[] bodyParts;
    
    public event EventHandler OnRespawn;

    private PlayerAudioManager _playerAudioManager;

    private LayerMask _layer;
    private InputActionMap _actions;
    private InputActionMap _deathActions;

    // Start is called before the first frame update
    void Awake()
    {
        _input = GetComponent<PlayerInput>();

        _actions = _input.actions.actionMaps[0];
        _deathActions = _input.actions.actionMaps[1];

        _deathActions["Look"].performed += OnLook;

        _actions["Movement"].performed += OnMovement;

        _actions["Look"].performed += OnLook;

        _actions["Stare"].started += OnStartStare;
        _actions["Stare"].canceled += OnStopStare;
        
        _actions["Jump"].started += OnJump;
        _actions["Jump"].canceled += OnStopJump;
        
        _actions["Crouch"].started += OnCrouch;
        
        _layer = 20 + GetPlayerIndex(); //J1Invisible
        gameObject.layer = _layer;

        foreach (Transform child in gameObject.GetComponentsInChildren<Transform>(true))
        {
            switch (child.gameObject.layer)
            {
                case 5: //UI
                    continue;
                case 20: //JXBody
                case 24: //VFX
                    child.gameObject.layer += GetPlayerIndex();
                    break;
            }
        }

        int vfxMask = (1 << 24) | (1 << 25) | (1 << 26) | (1 << 27);
        
        stareHandler.mask =  ~( (1 << _layer) | 4 );
        stareHandler.Cam.cullingMask = ~(1 << _layer) ^ vfxMask ^ 1 << (24 + GetPlayerIndex());

        controller.Stare = stareHandler;
    }

    private void Start()
    {
        GameMode.instance.PlayerSpawn(_input);
    }

    private void OnDisable()
    {

        _deathActions["Look"].performed -= OnLook;
        _actions["Movement"].performed -= OnMovement;

        _actions["Look"].performed -= OnLook;

        _actions["Stare"].started -= OnStartStare;
        _actions["Stare"].canceled -= OnStopStare;
        
        _actions["Jump"].started -= OnJump;
        _actions["Jump"].canceled -= OnStopJump;
        
        _actions["Crouch"].started -= OnCrouch;
    }

    public void ChangeBodyVisibility(bool isVisible)
    {
        foreach (var tr in bodyParts)
        {
            tr.layer = isVisible ? 0 : 20 + GetPlayerIndex();
        }
    }

    public PlayerInput GetInput()
    {
        return _input;
    }

    public int GetPlayerIndex()
    {
        return _input.playerIndex;
    }

	public Color GetPlayerColor()
    {
        TestMode t = TestMode.GetInstance();
        return t.Settings.playerColors[GetPlayerIndex()];
    }

    public void Respawn()
    {
        OnRespawn?.Invoke(this, null);
        
        ChangeBodyVisibility(false);
        controller.SetInputMovement(Vector2.zero);
        controller.ResetStates();
        stareHandler.StopStare();
        _input.SwitchCurrentActionMap("Arena");
    }

	public void AddForce(Vector3 force)
	{
		controller.AddForce(force);
	}

    private void OnCrouch(InputAction.CallbackContext obj)
    {
        if(!stareHandler.isStaring)
            controller.ToggleCrouch();
    }

    private void OnJump(InputAction.CallbackContext obj)
    {
		if(!stareHandler.isStaring)
            controller.SetJump(true);
    }

	private void OnStopJump(InputAction.CallbackContext obj)
	{
		controller.SetJump(false);
	}

	private void OnLook(InputAction.CallbackContext obj)
    {
        cameraMovement.MoveCamera(obj.ReadValue<Vector2>());
    }

    private void OnMovement(InputAction.CallbackContext obj)
    {
        var v = obj.ReadValue<Vector2>();
        controller.SetInputMovement(v);
    }

    private void OnStopStare(InputAction.CallbackContext obj)
    {
        stareHandler.StopStare();
        controller.SetStare(false);
    }

    private void OnStartStare(InputAction.CallbackContext obj)
    {
		if (controller.CanStare())
        {
            stareHandler.StartStare();
            controller.SetStare(true);
        }
    }
}
