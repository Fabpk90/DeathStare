﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using DG.Tweening;
[ExecuteAlways]
public class UIHealthBar : MonoBehaviour
{
	//parameter
	public bool inverseSide = false;
	public float updateSpeed = 1;
	public float delayBeforeUpdate = 1000;
	[Space]
	public Image health;
	public Image positiveHealth;
	public Image negativeHealth;

	[SerializeField, DisplayWithoutEdit]
	private bool _isUpdating;
	private float _lastChange;
	private float _lastValue;
	[SerializeField,DisplayWithoutEdit]
	private float _targetValue;
	private bool _hasChange = false;

	private void Awake()
	{
		
	}

	private void LateUpdate()
	{
		UpdateFillOrigin();
		if (!_hasChange) return;

		if (!_isUpdating)
		{
			if(Time.time> _lastChange + delayBeforeUpdate)
			{
				_isUpdating = true;
			}

		}else
		{
			if(_targetValue < _lastValue)
			{
				//lose life
				health.fillAmount = _targetValue;
				negativeHealth.fillAmount -= Time.deltaTime * updateSpeed;
				positiveHealth.fillAmount = _targetValue;
				if(negativeHealth.fillAmount <= _targetValue)
				{
					_isUpdating = false;
					_hasChange = false;
					negativeHealth.fillAmount = _targetValue;
					positiveHealth.fillAmount = _targetValue;
					health.fillAmount = _targetValue;
				}
			}
			else
			{
				//gain life
				health.fillAmount += Time.deltaTime * updateSpeed;
				negativeHealth.fillAmount = health.fillAmount;
				positiveHealth.fillAmount = _targetValue;
				if (health.fillAmount >= _targetValue)
				{
					_isUpdating = false;
					_hasChange = false;
					positiveHealth.fillAmount = _targetValue;
					negativeHealth.fillAmount = _targetValue;
					health.fillAmount = _targetValue;

				}
			}
		}
	}

	private void UpdateFillOrigin()
	{
		health.fillOrigin = inverseSide ? 0 : 1;
		negativeHealth.fillOrigin = inverseSide ? 0 : 1;
		positiveHealth.fillOrigin = inverseSide ? 0 : 1;
	}

	[ContextMenu("test")]
	public void Test()
	{
		SetValue(Random.value);
	}

	public void Flash()
	{

	}

	public void Shake()
	{

	}

	public void SetValue(float value, bool instant = false)
	{
		_lastValue = _targetValue;
		_targetValue = value;
		_lastChange = Time.time;
		_hasChange = true;
		if (_targetValue < _lastValue)
		{
			health.fillAmount = _targetValue;
			negativeHealth.fillAmount = _lastValue;
			positiveHealth.fillAmount = _targetValue;
		}
		else
		{
			health.fillAmount = _lastValue;
			negativeHealth.fillAmount = _lastValue;
			positiveHealth.fillAmount = _targetValue;
		}
		if (instant)
		{
			_hasChange = false;
			health.fillAmount = _targetValue;
			negativeHealth.fillAmount = _targetValue;
			positiveHealth.fillAmount = _targetValue;
		}
	}
}
