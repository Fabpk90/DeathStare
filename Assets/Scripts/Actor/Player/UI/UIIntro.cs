﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;
public class UIIntro : MonoBehaviour
{
	public Image background;
	public TextMeshProUGUI textMesh;

	private void OnEnable()
	{
        MusicManager.OnIntroCue += OnIntroCue;
	}

	
	private void OnDisable()
	{
		MusicManager.OnIntroCue -= OnIntroCue;
	}

	private void TriggerFill()
	{
		background.DOKill(false);
		background.fillAmount = 1;
		background.DOFillAmount(0, 1);
		textMesh.transform.DOKill(true);
		textMesh.transform.localScale = Vector3.one;
		textMesh.transform.DOScale(0.8f, 1).SetEase(Ease.InCubic);
	}

	private void OnIntroCue(object sender, string e)
	{
		switch (e) //Mettez ce que vous voulez faire à chaque étape du décompte dans chaque case
		{
			case "3":
				TriggerFill();
				textMesh.text = "3";
				//print("Intro: 3");
				//Call what you want here
				break;

			case "2":
				TriggerFill();
				textMesh.text = "2";
				//print("Intro: 2");
				//Call what you want here
				break;

			case "1":
				TriggerFill();
				textMesh.text = "1";
				//print("Intro: 1");
				//Call what you want here
				break;

			case "Fight":
				background.enabled = false;
				textMesh.text = "";
				//print("Intro: Fight");
				//Call what you want here
				break;
		}
	}

}
