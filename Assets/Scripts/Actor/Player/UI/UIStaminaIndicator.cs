﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Actor.Player.Stare;
using DG.Tweening;
public class UIStaminaIndicator : MonoBehaviour
{
	public Image staminaUI;
	public StaminaHandler staminaHandler;
	public AnimationCurve opacityOverStamina = new AnimationCurve(new Keyframe(0,1), new Keyframe(1,0));

	private void OnEnable()
	{
		staminaHandler.OnStaminaUsed += OnStaminaChange;
		staminaHandler.OnStaminaRegen += OnStaminaChange;
		staminaHandler.OnStaminaNotEnough += OnStaminaNotEnough;
	}

	

	private void OnDisable()
	{
		staminaHandler.OnStaminaUsed -= OnStaminaChange;
		staminaHandler.OnStaminaRegen -= OnStaminaChange;
		staminaHandler.OnStaminaNotEnough -= OnStaminaNotEnough;
	}

	private void OnStaminaChange(object sender, float e)
	{
		Color c = staminaUI.color;
		c.a = opacityOverStamina.Evaluate(staminaHandler.GetNormalizedStamina());
		staminaUI.color = c;
		//float v = 1- staminaHandler.GetNormalizedStamina();

		//staminaUI.DOColor(c, 0.15f).SetEase(Ease.InElastic);
	}

	private void OnStaminaNotEnough(object sender, System.EventArgs e)
	{
		Color c = staminaUI.color;
		c.a = opacityOverStamina.Evaluate(staminaHandler.GetNormalizedStamina());
		staminaUI.color = c;
		Debug.Log("not enough");
		//staminaUI.DOColor(c, 0.15f).SetEase(Ease.InElastic);
	}
}
