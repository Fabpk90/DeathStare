﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


//handles Player UI
//bridge between player status and animator

[RequireComponent(typeof(Animator))]
public class PlayerUIManager : MonoBehaviour
{
	
	public StareHandler stare;
	public PlayerController controller;
	[Space]
	public CanvasScaler canvasScaler;
	public RectTransform deathStareUpBar;
	public RectTransform deathStareDownBar;
	private Animator animator;



	private bool isStaring;

	private void Awake()
	{
		animator = GetComponent<Animator>();
	}


	private void OnEnable()
	{
		stare.OnStareStart += OnStareStart;
		stare.OnStareStop += OnStareStop;
		stare.OnStareTick += OnStareTick;
	}

	

	private void OnDisable()
	{
		stare.OnStareStart -= OnStareStart;
		stare.OnStareStop -= OnStareStop;
		stare.OnStareTick -= OnStareTick;
	}

	private void OnStareTick(object sender, EventArgs e)
	{
		animator.ResetTrigger("Tick");
		animator.SetTrigger("Tick");
	}



	//Get the height of the stare (in viewport coord)
	public Vector2 GetViewHeight()
	{
		Vector2 v = Vector2.zero;
		v.x = deathStareDownBar.rect.height / canvasScaler.referenceResolution.y;
		v.y = 1 - deathStareUpBar.rect.height / canvasScaler.referenceResolution.y;
		return v;
	}
	

	private void OnStareStart(object sender, System.EventArgs e)
	{
		animator.SetFloat("DSLoadSpeed", 1/stare.stareParams.settings.loadingTimeToStare);
		animator.SetBool("isStaring", true);
	}

	private void OnStareStop(object sender, System.EventArgs e)
	{
		animator.SetBool("isStaring", false);
	}
}
