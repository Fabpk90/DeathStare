﻿using UnityEngine;
using System.Collections;
using Actor.Player;
using System;
using UnityEngine.UI;
using UnityEngine.InputSystem;
using DG.Tweening;

public class UIDamageHighlight : MonoBehaviour
{
	public PlayerHealth health;
	public PlayerInput playerInput;
	[Space]
	public Image[] dmgHighlights;
	public RectTransform dmgRotation;
	public Image dmgCircle;
	public Image screenDamage;
	private RectTransform parent;

	public float maxDuration = 2;
	public float maxThickness = 100;
	public float maxAngleRange = 90;
	public AnimationCurve thicknessOverTime = new AnimationCurve(new Keyframe(0, 1), new Keyframe(1, 0));
	public AnimationCurve angleRangeOverTime = new AnimationCurve(new Keyframe(0, 1), new Keyframe(1, 0));

	private float _time = 0;
	private float _thickness = 100;
	private Transform target;
	private Vector3 targetPos;

	private void Awake()
	{
		parent = dmgHighlights[0].rectTransform.parent.GetComponent<RectTransform>();
		_time = maxDuration;
	}

	private void Start()
	{
		UpdateBorder(0, 0, 0);
		
	}
	private void OnEnable()
	{
		health.OnTakingDamage += OnTakingDamage;
	}

	private void OnDisable()
	{
		ResetRumble();
		health.OnTakingDamage -= OnTakingDamage;
	}

	private void LateUpdate()
	{
		if (target)
		{
			targetPos = target.transform.position;
			
		}else
		{
			if (_time > maxDuration)
			{
				UpdateBorder(0, 0, 0);
				ResetRumble();
				return;
			}
		}
		
		float normTime = _time / maxDuration;
		float angle = ComputeAngle(targetPos);

		dmgRotation.localRotation = Quaternion.Euler(0, 0, angle);
		dmgCircle.color = new Color(1, 0, 0,1- normTime * normTime);

		screenDamage.color = new Color(1, 0, 0, 1 - normTime * normTime * 2);

		UpdateRumble(angle, Mathf.Clamp01(0.5f-normTime* normTime));
	
		UpdateBorder(angle, 
			angleRangeOverTime.Evaluate(normTime) * maxAngleRange, 
			thicknessOverTime.Evaluate(normTime) * maxThickness);
		
		if(_time > maxDuration)
		{
			target = null;
			UpdateBorder(0, 0, 0);
			ResetRumble();
		}
		_time += Time.deltaTime;
	}
	private void ResetRumble()
	{
		if (playerInput.playerIndex >= Gamepad.all.Count) return;
		Gamepad.all[playerInput.playerIndex].SetMotorSpeeds(0, 0);
	}
	private void UpdateRumble(float angle, float amount)
	{
		if (playerInput.playerIndex >= Gamepad.all.Count) return;
		float c = Mathf.Cos(angle * Mathf.Deg2Rad);
		Vector2 motor = new Vector2(Mathf.Clamp01(c * -1), Mathf.Clamp01(c)).normalized * amount;
		Gamepad.all[playerInput.playerIndex].SetMotorSpeeds(motor.x, motor.y);
	}

	private void OnTakingDamage(object sender, Tuple<float, int> t)
	{
		_time = 0;
		target = GameMode.instance.players[t.Item2].transform;
	}

	private float ComputeAngle(Vector3 dmgSrc)
	{
		Vector3 v = dmgSrc;
		v.y = transform.root.position.y;

		Vector3 dir = v - transform.root.position;

		return Vector3.SignedAngle(dir, transform.root.right, Vector3.up);

	}

	private void SetNormalizedMin(RectTransform t, Vector2 v)
	{
		t.offsetMin = new Vector2(Mathf.Clamp01(v.x) * parent.rect.width, Mathf.Clamp01(v.y) * parent.rect.height);
	}
	private void SetNormalizedMax(RectTransform t, Vector2 v)
	{
		t.offsetMax = new Vector2(Mathf.Clamp(v.x - 1, -1, 0) * parent.rect.width, Mathf.Clamp(v.y - 1, -1, 0) * parent.rect.height);
	}

	private void UpdateBorder(float directionAngle, float angleSize, float thickness)
	{

		//Danger  c'est moche !!! 
		float angleMin = (directionAngle + angleSize / 2) * Mathf.Deg2Rad;
		float angleMax = (directionAngle - angleSize / 2) * Mathf.Deg2Rad;
		float cosMin = (Mathf.Cos(angleMin) * 1.41f + 1) / 2; //1.41f == diagonale d'un carré de 1 (le canvas est considérer comme un carré de par les coordonnées normalisé)
		float cosMax = (Mathf.Cos(angleMax) * 1.41f + 1) / 2;
		float sinMin = (Mathf.Sin(angleMin) * 1.41f + 1) / 2;
		float sinMax = (Mathf.Sin(angleMax) * 1.41f + 1) / 2;

		float w = thickness / parent.rect.height;
		float h = thickness / parent.rect.width;
		/////////////
		//topMinbar
		if (sinMin > 0.5f)
		{
			SetNormalizedMin(dmgHighlights[0].rectTransform, new Vector2(cosMin, 1 - w));
		}
		else
		{
			if (cosMin > 0.5f)
			{
				SetNormalizedMin(dmgHighlights[0].rectTransform, new Vector2(1, 1 - w));
			}
			else
			{
				SetNormalizedMin(dmgHighlights[0].rectTransform, new Vector2(0, 1 - w));
			}
		}
		//topMaxBar
		if (sinMax > 0.5f)
		{
			SetNormalizedMax(dmgHighlights[0].rectTransform, new Vector2(cosMax, 1));
		}
		else
		{
			if (cosMax > 0.5f)
			{
				SetNormalizedMax(dmgHighlights[0].rectTransform, new Vector2(1, 1));
			}
			else
			{
				SetNormalizedMax(dmgHighlights[0].rectTransform, new Vector2(0, 1));
			}
		}
		/////////////
		//botMinBar
		if (sinMax < 0.5f)
		{
			SetNormalizedMin(dmgHighlights[2].rectTransform, new Vector2(cosMax, 0));
		}
		else
		{
			if (cosMax > 0.5f)
			{
				SetNormalizedMin(dmgHighlights[2].rectTransform, new Vector2(1, 0));
			}
			else
			{
				SetNormalizedMin(dmgHighlights[2].rectTransform, new Vector2(0, 0));
			}
		}
		//botMaxBar
		if (sinMin < 0.5f)
		{
			SetNormalizedMax(dmgHighlights[2].rectTransform, new Vector2(cosMin, w));
		}
		else
		{
			if (cosMin > 0.5f)
			{
				SetNormalizedMax(dmgHighlights[2].rectTransform, new Vector2(1, w));
			}
			else
			{
				SetNormalizedMax(dmgHighlights[2].rectTransform, new Vector2(0, w));
			}
		}
		/////////////
		//RightMinBar
		if (cosMax > 0.5f)
		{
			SetNormalizedMin(dmgHighlights[1].rectTransform, new Vector2(1 - h, sinMax));
		}
		else
		{
			if (sinMax > 0.5f)
			{
				SetNormalizedMin(dmgHighlights[1].rectTransform, new Vector2(1 - h, 1));
			}
			else
			{
				SetNormalizedMin(dmgHighlights[1].rectTransform, new Vector2(1 - h, 0));
			}
		}
		//RightMaxBar
		if (cosMin > 0.5f)
		{
			SetNormalizedMax(dmgHighlights[1].rectTransform, new Vector2(1, sinMin));
		}
		else
		{
			if (sinMin > 0.5f)
			{
				SetNormalizedMax(dmgHighlights[1].rectTransform, new Vector2(1, 1));
			}
			else
			{
				SetNormalizedMax(dmgHighlights[1].rectTransform, new Vector2(1, 0));
			}
		}
		/////////////
		//LeftMinBar
		if (cosMin < 0.5f)
		{
			SetNormalizedMin(dmgHighlights[3].rectTransform, new Vector2(0, sinMin));
		}
		else
		{
			if (sinMin > 0.5f)
			{
				SetNormalizedMin(dmgHighlights[3].rectTransform, new Vector2(0, 1));
			}
			else
			{
				SetNormalizedMin(dmgHighlights[3].rectTransform, new Vector2(0, 0));
			}
		}
		//LeftMaxBar
		if (cosMax < 0.5f)
		{
			SetNormalizedMax(dmgHighlights[3].rectTransform, new Vector2(h, sinMax));
		}
		else
		{
			if (sinMax > 0.5f)
			{
				SetNormalizedMax(dmgHighlights[3].rectTransform, new Vector2(h, 1));
			}
			else
			{
				SetNormalizedMax(dmgHighlights[3].rectTransform, new Vector2(h, 0));
			}
		}
	}
}
