﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.InputSystem;

public class UICursorsManager : MonoBehaviour
{
	public StareHandler stare;
	public Canvas spawnCanvas;
	public UICursor prefab;
	private List<UICursor> cursors = new List<UICursor>();
	public Camera cam;




	private void OnEnable()
	{
		stare.OnStareStop += OnStareStop;
		GameMode.OnStartOfMatch += OnStartOfMatch;
		stare.OnStareTouch += OnStareTouch;
		stare.OnStareBlocked += OnStareBlocked;
		stare.OnStareKill += OnStareKill;
		TestMode.OnScoreChanged += OnScoredChanged;
	}


	private void OnDisable()
	{
		stare.OnStareStop -= OnStareStop;
		GameMode.OnStartOfMatch -= OnStartOfMatch;
		stare.OnStareTouch -= OnStareTouch;
		stare.OnStareBlocked -= OnStareBlocked;
		stare.OnStareKill -= OnStareKill;
		TestMode.OnScoreChanged -= OnScoredChanged;

	}

	private void OnScoredChanged(object sender, List<FinalScoreHandler> e)
	{
		for(int i = 0; i<e.Count;i++)
		{
			//Debug.Log("N°" + ((int)(i + 1)) + "/"+e.Count+ " : " + e[i].player.gameObject.name + " with " + e[i].score);
			if (i == 0 && e[0].score > e[1].score)
			{
				cursors[e[0].player.playerIndex].ActiveCrown(true);
			}
			else
			{
				cursors[e[i].player.playerIndex].ActiveCrown(false);
			}
		}
	}

	private void OnStartOfMatch(object sender, System.EventArgs e)
	{
		for(int i = 0; i< GameMode.instance.players.Count; i++)
		{
			cursors.Add(CreateCursor(i));
		}
	}



	private void OnStareStop(object sender, System.EventArgs e)
	{
		foreach(UICursor c in cursors)
		{
			c.SetDisplay(false);
		}
	}

	private void OnStareTouch(object sender, int e)
	{
		cursors[e].HitConfirm();
	}


	private void OnStareBlocked(object sender, List<int> e)
	{
		foreach(int i in e)
		{
			cursors[i].HitBlock();
		}
	}

	private void OnStareKill(object sender, int e)
	{
		cursors[e].KillConfirm();
	}




	private void Update()
	{
		if (stare.isStaring)
		{
			for(int c = 0; c< cursors.Count; c++)
			{
				bool d = false;
				for(int i = 0; i< stare.playersHitDuringThisFrame.Count; i++)
				{
					if(c == stare.playersHitDuringThisFrame[i].GetPlayerIndex())
					{
						d = true;
					}
				}
				cursors[c].SetDisplay(d);
			}
		}
	}

	private UICursor CreateCursor(int id)
	{
		var c = Instantiate(prefab, spawnCanvas.transform);
		c.target = id;
		c.cam = cam;
		c.SetDisplay(false);
		return c;
	}
}
