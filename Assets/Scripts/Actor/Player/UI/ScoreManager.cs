﻿using System;

using System.Collections.Generic;

using UnityEngine;


public class ScoreManager : MonoBehaviour
{
    private PlayerController _controller;
    private PlayerAudioManager playerAudioManager;
    
    private uint _score;
	public delegate void BasicEvent(uint i);
	public event BasicEvent OnScored;

    private void OnEnable()
    {
        GameMode.OnKillEvent += OnKill;
    }

    private void OnDisable()
    {
        GameMode.OnKillEvent -= OnKill;
    }

    private void Start()
    {
        _controller = GetComponentInParent<PlayerController>();
        
        //Sound
        AudioManager.instance.AddListeners(gameObject, 4);
        playerAudioManager = AudioManager.instance.GetPlayerAudioManager(_controller.GetPlayerIndex());
        //Sound
    }

	private void OnKill(object sender, Tuple<int, int> e)
    {
        if (e.Item1 == _controller.GetPlayerIndex())
        {
            _score++;
			OnScored?.Invoke(_score);
            //Sound
            FightAudioManager.instance.CheckBellRing(_score, e.Item1);
            FightAudioManager.instance.CheckFightMusicState(_score);

			if (!playerAudioManager) playerAudioManager =  AudioManager.instance.GetPlayerAudioManager(_controller.GetPlayerIndex());

			switch (e.Item1){
                case (0):
                    //AkSoundEngine.PostEvent("STINGERS_Kill_Stan_L", gameObject);
                    AkSoundEngine.PostTrigger("TRIGGER_Kill_P1", MusicManager.instance.gameObject);
                    playerAudioManager.PostEvent("VO_Char_Punchline_Kill");
                    break;
                case (1):
                    //AkSoundEngine.PostEvent("STINGERS_Kill_Marta_R", gameObject);
                    AkSoundEngine.PostTrigger("TRIGGER_Kill_P2", MusicManager.instance.gameObject);
                    playerAudioManager.PostEvent("VO_Char_Punchline_Kill");
                    break;
                case (2):
                    //AkSoundEngine.PostEvent("STINGERS_Kill_Medusa_L", gameObject);
                    AkSoundEngine.PostTrigger("TRIGGER_Kill_P3", MusicManager.instance.gameObject);
                    playerAudioManager.PostEvent("VO_Char_Punchline_Kill");
                    break;
                case (3):
                    //AkSoundEngine.PostEvent("STINGERS_Kill_Don_R", gameObject);
                    AkSoundEngine.PostTrigger("TRIGGER_Kill_P4", MusicManager.instance.gameObject);
                    playerAudioManager.PostEvent("VO_Char_Punchline_Kill");
                    break;
            }
            //Sound

            //TODO: fix this ugly hax to be more abstract
            if (TestMode.GetInstance().GetScoreToWin() <= _score)
                TestMode.GetInstance().Win(new List<int>() {_controller.GetPlayerIndex()});
        }
    }

    public uint GetScore()
    {
        return _score;
    }
}
