﻿using UnityEngine;
using System.Collections.Generic;
using TMPro;
using UnityEngine.UI;
[RequireComponent(typeof(Animator))]
public class UICursor : MonoBehaviour
{
	public int target;
	public Vector3 offset;
	public Camera cam;
	public TMP_Text indexText;
	public Image triangle;
	public Image crown;

	private RectTransform canvasRect;
	private RectTransform rectTransform;
	private Animator _animator;
	private bool _display;
	private Vector2 uiOffset;
	private static readonly int Hit = Animator.StringToHash("Hit");
	private static readonly int Kill = Animator.StringToHash("Kill");
	private static readonly int Block = Animator.StringToHash("Block");
	private static readonly int Display = Animator.StringToHash("Display");

	private void Awake()
	{
		_animator = GetComponent<Animator>();
		canvasRect = transform.parent.GetComponent<RectTransform>();
		rectTransform = GetComponent<RectTransform>();
		uiOffset = new Vector2((float)canvasRect.sizeDelta.x / 2f, (float)canvasRect.sizeDelta.y / 2f);
	}

	private void LateUpdate()
	{
		if (!_display) return;

		//no need to repeat that !
		indexText.text = "P" + (target+1);
		indexText.color = TestMode.GetInstance().Settings.playerColors[target];
		triangle.color = TestMode.GetInstance().Settings.playerColors[target];


		WorldToCanvas(GameMode.instance.players[target].transform.position + offset);
	}

	public void SetDisplay(bool b)
	{
		if(b != _display)
		{
			_animator.SetBool(Display, b);
		}
		_display = b;
	}

	public void HitConfirm()
	{
		_animator.ResetTrigger(Hit);
		_animator.SetTrigger(Hit);
	}

	public void HitBlock()
	{
		//_animator.ResetTrigger("Hit");
		//_animator.SetTrigger("Hit");
		_animator.ResetTrigger("Block");
		_animator.SetTrigger("Block");
	}

	public void KillConfirm()
	{
		_animator.ResetTrigger(Hit);
		_animator.SetTrigger(Hit);
		_animator.ResetTrigger(Kill);
		_animator.SetTrigger(Kill);
	}

	public void WorldToCanvas(Vector3 position)
	{
		Vector2 viewportPoint = cam.WorldToViewportPoint(position);
		rectTransform.anchorMin = viewportPoint;
		rectTransform.anchorMax = viewportPoint;
	}

	public void ActiveCrown(bool b)
	{
		crown.enabled = b;
	}
}
