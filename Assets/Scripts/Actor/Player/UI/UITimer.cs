﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;
using TMPro;

public class UITimer : MonoBehaviour
{
	public Image background;
	public TextMeshProUGUI textMesh;

	private Vector3 targetLocalPos;

	private void OnEnable()
	{
		MusicManager.OneSecBeforeMatchStart += OnMatchStart;
	}

	private void OnDisable()
	{
		MusicManager.OneSecBeforeMatchStart -= OnMatchStart;
	}

	private void Start()
	{
		targetLocalPos = background.rectTransform.localPosition;
		background.rectTransform.localPosition += Vector3.up * 200;
	}

	private void OnMatchStart(object sender, System.EventArgs e)
	{
		background.rectTransform.DOLocalMove(targetLocalPos, 1).SetEase(Ease.InCubic);
	}
}
