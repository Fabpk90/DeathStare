using System;
using UnityEngine;
using UnityEngine.UI;
using Sweet.UI;
using UnityEngine.InputSystem;
using DG.Tweening;

namespace Actor.Player.UI
{
    public class HealthBarManager : MonoBehaviour
    {
		//public UISlider uiSlider;
		public UIHealthBar uiHealthBar;
		public Image healUI;		
        
        public PlayerHealth health;
        public PlayerController controller;

		private Vector3 localPos;

		private Vector3 targetPos;


        private void OnEnable()
        {
            health.OnTakingDamage += OnTakingDamage;
            health.OnHealthRegen += HealthOnOnHealthRegen;
            controller.OnRespawn += OnRespawn;
			GameMode.OnPlayerJoin += OnPlayerInit;
			MusicManager.OneSecBeforeMatchStart += OnMatchStart;
        }

	

		private void OnDisable()
        {
            health.OnTakingDamage -= OnTakingDamage;
            health.OnHealthRegen -= HealthOnOnHealthRegen;
            controller.OnRespawn -= OnRespawn;
			GameMode.OnPlayerJoin -= OnPlayerInit;
			MusicManager.OneSecBeforeMatchStart -= OnMatchStart;
        }

		private void Awake()
		{
			localPos = uiHealthBar.transform.localPosition;
		}


		private void OnPlayerInit(object sender, PlayerInput e)
		{

			SetPosition();
			uiHealthBar.health.color = controller.GetPlayerColor();
			if (GameMode.instance.isMatchStarted)
			{
				uiHealthBar.transform.DOLocalMove(targetPos, 1).SetEase(Ease.InCubic);
			}
		}

		private void OnMatchStart(object sender, EventArgs e)
		{
			uiHealthBar.transform.DOLocalMove(targetPos, 1).SetEase(Ease.InCubic);
		}

		private void Start()
        {
			//uiSlider.value = health.health / health.maxHealth;
			uiHealthBar.SetValue(health.health / health.maxHealth, true);
		}

        private void OnRespawn(object sender, EventArgs e)
        {
			//uiSlider.value = 1;
			uiHealthBar.SetValue(1, true);

		}

		private void OnTakingDamage(object sender, Tuple<float,int> t)
        {
			//uiSlider.value = health.health / health.maxHealth;
			uiHealthBar.SetValue(health.health / health.maxHealth);
			uiHealthBar.transform.DOKill(true);
			uiHealthBar.transform.DOShakePosition(0.5f,10);
			uiHealthBar.health.DOKill(false);
			uiHealthBar.health.color = Color.white;
			uiHealthBar.health.DOColor(controller.GetPlayerColor(), 0.3f).SetDelay(0.3f).SetEase(Ease.OutCubic);
		}

		private void HealthOnOnHealthRegen(object sender, float e)
		{
			uiHealthBar.SetValue(health.health / health.maxHealth);
			healUI.color = new Color(healUI.color.r, healUI.color.g, healUI.color.b, 1);
			healUI.DOColor(new Color(healUI.color.r, healUI.color.g, healUI.color.b, 0), 2).SetDelay(1).SetEase(Ease.InCubic);
		}

		private void SetPosition()
		{
			targetPos = localPos;
			switch (controller.GetPlayerIndex())
			{
				case 0:
					uiHealthBar.transform.localPosition = new Vector3(0, 800, 0);
					uiHealthBar.transform.localRotation = Quaternion.Euler(0, 180, 0);
					break;
				case 1:
					uiHealthBar.transform.localPosition = new Vector3(0, 800, 0);
					uiHealthBar.transform.localRotation = Quaternion.Euler(0, 0, 0);
					uiHealthBar.inverseSide = true;
					break;
				case 2:
					uiHealthBar.transform.localPosition = new Vector3(0, -800, 0);
					targetPos = new Vector3(localPos.x, -localPos.y, localPos.z);
					uiHealthBar.transform.localRotation = Quaternion.Euler(180, 180, 0);

					break;
				case 3:
					uiHealthBar.transform.localPosition = new Vector3(0, -800, 0);
					targetPos = new Vector3(localPos.x, -localPos.y, localPos.z);
					uiHealthBar.transform.localRotation = Quaternion.Euler(180, 0, 0);
					uiHealthBar.inverseSide = true;
					break;
			}

			
		}
	}
}