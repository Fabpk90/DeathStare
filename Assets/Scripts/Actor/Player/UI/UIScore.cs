﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;
public class UIScore : MonoBehaviour
{
	public TMP_Text text;
	public Image[] background;
	public Image victoryCrown;
	public int screenIndex = 0;
	private ScoreManager scoreManager;

	private Vector3 startLocPos;
	private Vector2 startSizeDelta;
	private RectTransform rectTransform;

	public Vector2 pos1 = new Vector2(50, 50);
	public Vector2 pos2 = new Vector2(30, 30);
	public Vector2 pos3 = new Vector2(10, 10);
	public Vector2 pos4 = new Vector2(0, 0);
	public float finalScalingMultiplier = 2;

	private float scalingMult = 1;

	private bool matchStart = false;

	private void Awake()
	{
		rectTransform = GetComponent<RectTransform>();

		foreach (Image i in background) i.enabled = false;

		text.enabled = false;
		
	}


	private void OnEnable()
	{
		GameMode.OnPlayerJoin += OnPlayerJoin;
		TestMode.OnScoreChanged += OnScoredChange;
		MusicManager.OnMatchStart += OnMatchStart;
		MusicManager.OnIntroCue += OnCue;
		TestMode.OnMatchEnd += OnMatchEnd;
	}

	

	private void OnDisable()
	{
		GameMode.OnPlayerJoin -= OnPlayerJoin;
		TestMode.OnScoreChanged -= OnScoredChange;
		if(scoreManager)
			scoreManager.OnScored -= OnScored;
		MusicManager.OnMatchStart -= OnMatchStart;
		MusicManager.OnIntroCue -= OnCue;
		TestMode.OnMatchEnd -= OnMatchEnd;
	}

	

	private void Start()
	{
		startLocPos = rectTransform.localPosition;
		startSizeDelta = rectTransform.sizeDelta;
		
	}

	private void OnMatchStart(object sender, System.EventArgs e)
	{
		matchStart = true;
		if (scoreManager)
		{
			foreach (Image i in background) i.enabled = true;
			text.enabled = true;
		}
		//background.enabled = true;
		//text.enabled = true;
	}


	private void OnPlayerJoin(object sender, UnityEngine.InputSystem.PlayerInput e)
	{
		if(screenIndex == e.playerIndex)
		{
			if (matchStart)
			{
				foreach (Image i in background) i.enabled = true;
				text.enabled = true;
			}
			
			scoreManager = e.GetComponentInChildren<ScoreManager>();
			scoreManager.OnScored += OnScored;
			background[0].color = e.GetComponentInChildren<PlayerController>().GetPlayerColor();
		}
	}

	private void OnScored(uint i)
	{
		text.text = "" + i;
		Color c = GameMode.instance.players[screenIndex].GetComponentInChildren<PlayerController>().GetPlayerColor();
		background[0].color = Color.white;
		background[0].DOColor(c, 1f).SetDelay(0.2f);

        //Sound
        AudioManager.instance.GetPlayerAudioManager(screenIndex).PostEvent("UI_HUD_Score_Up");
        //Sound
    }

	private void OnCue(object sender, string e)
	{
		if (e == "Outro")
		{
			System.Collections.Generic.List<FinalScoreHandler> scores = TestMode.playerScores;
			if (!scoreManager) return;
			scalingMult = finalScalingMultiplier;
			switch (GetRating(scores))
			{
				case 1:
					Scaling(pos1);
					break;
				case 2:
					Scaling(pos2);
					break;
				case 3:
					Scaling(pos3);
					break;
				default:
					Scaling(pos4);
					break;
			}

			if (scores[0].player.playerIndex == screenIndex)
			{
				victoryCrown.DOColor(new Color(victoryCrown.color.r, victoryCrown.color.g, victoryCrown.color.b, 1), 3).SetEase(Ease.OutCubic);
			}
		}
	}

	private void OnMatchEnd(object sender, System.Collections.Generic.List<FinalScoreHandler> e)
	{
		//moved to onCue
	}

	private void OnScoredChange(object sender, System.Collections.Generic.List<FinalScoreHandler> e)
	{
		if(!scoreManager) return;
		switch (GetRating(e))
		{
			case 1:
				Scaling(pos1);
				break;
			case 2:
				Scaling(pos2);
				break;
			case 3:
				Scaling(pos3);
				break;
			default: Scaling(pos4); break;
		}
	}

	private int GetRating(System.Collections.Generic.List<FinalScoreHandler> e)
	{
		int rating = 0;
		for(int i = 0; i< e.Count; i++)
		{
			if(scoreManager.GetScore() > e[i].score)
			{
				rating++;
			}
		}
		return e.Count - rating;
	}

	


	private void Scaling(Vector2 offset)
	{
		rectTransform.DOKill();

		Vector3 newLocPos = rectTransform.localPosition;
		Vector2 newSizeDelta = rectTransform.sizeDelta;
		offset *= scalingMult;
		switch (screenIndex)
		{
			case 0:
				newLocPos = startLocPos + new Vector3(-offset.x/2, offset.y/2, 0);
				newSizeDelta = startSizeDelta + new Vector2(offset.x, offset.y);
				break;
			case 1:
				newLocPos = startLocPos + new Vector3(offset.x / 2, offset.y / 2, 0);
				newSizeDelta = startSizeDelta + new Vector2(offset.x, offset.y);
				break;
			case 2:
				newLocPos = startLocPos + new Vector3(-offset.x / 2, -offset.y / 2, 0);
				newSizeDelta = startSizeDelta + new Vector2(offset.x, offset.y);
				break;
			case 3:
				newLocPos = startLocPos + new Vector3(offset.x / 2, -offset.y / 2, 0);
				newSizeDelta = startSizeDelta + new Vector2(offset.x, offset.y);
				break;
			default:break;
		}

		rectTransform.DOLocalMove(newLocPos, 1).SetEase(Ease.OutCubic);
		rectTransform.DOSizeDelta(newSizeDelta, 1).SetEase(Ease.OutCubic);
	}
}
