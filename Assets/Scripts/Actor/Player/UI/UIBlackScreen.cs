﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using UnityEngine.UI;

public class UIBlackScreen : MonoBehaviour
{
	public int screenIndex;

	private Image img;
	private PlayerController controller;

	private void Awake()
	{
		img = GetComponent<Image>();
		img.color = new Color(0, 0, 0, 1);
	}

	private void OnEnable()
	{
		GameMode.OnPlayerJoin += OnPlayerJoin;
		GameMode.OnKillEvent += OnKill;
	}

	private void OnDisable()
	{
		GameMode.OnPlayerJoin -= OnPlayerJoin;
		GameMode.OnKillEvent -= OnKill;
		if (controller) controller.OnRespawn -= OnRespawn;
	}

	private void OnKill(object sender, System.Tuple<int, int> e)
	{
		if (e.Item2 == screenIndex)
		{
			//img.DOColor(new Color(0, 0, 0, 1), 0.5f).SetEase(Ease.InExpo);
			img.color = new Color(0, 0, 0, 1);
		}
	}



	private void OnPlayerJoin(object sender, UnityEngine.InputSystem.PlayerInput e)
	{
		if(screenIndex == e.playerIndex)
		{
			img.color = new Color(0, 0, 0, 0);
			controller = e.GetComponent<PlayerController>();
			controller.OnRespawn += OnRespawn;
		}

	}

	private void OnRespawn(object sender, System.EventArgs e)
	{
		img.DOColor(new Color(0, 0, 0, 0), 0.5f).SetEase(Ease.InExpo);
	}
}
