﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using DG.Tweening;
using TMPro;
public class UIKilledAnnounce : MonoBehaviour
{
	public TextMeshProUGUI text;
	public Image background;
	public int playerIndex;

	private void OnEnable()
	{
		GameMode.OnKillEvent += OnKill;
	}

	private void OnDisable()
	{
		GameMode.OnKillEvent -= OnKill;
	}

	private void OnKill(object sender, System.Tuple<int, int> e)
	{
		if(e.Item2 == playerIndex)
		{
			if (e.Item1 <0)
			{
				Color c = GameMode.instance.Settings.playerColors[e.Item2];
				string str = "<color=#" + ColorUtility.ToHtmlStringRGB(c) + ">";
				text.text = str + "You</color> killed "+ str +"yourself";
			}
			else
			{
				Color c = GameMode.instance.Settings.playerColors[e.Item1];
				text.text = "Killed by <color=#" + ColorUtility.ToHtmlStringRGB(c) + ">P" + (e.Item1 + 1) + "</color>";
			}
			background.color = new Color(background.color.r, background.color.g, background.color.b, 1);
			text.color = new Color(text.color.r, text.color.g, text.color.b, 1);
			background.DOColor(new Color(background.color.r, background.color.g, background.color.b, 0), 2).SetDelay(5);
			text.DOColor(new Color(text.color.r, text.color.g, text.color.b, 0), 2).SetDelay(5);
		}
	}
}
