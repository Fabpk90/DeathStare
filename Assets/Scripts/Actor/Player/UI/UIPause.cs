﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using DG.Tweening;
using Actor.Player;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

public class UIPause : MonoBehaviour
{
	public PlayerInput ipt;
	[Space]
	public Selectable firstItem;
	public GameObject containerMenu;

	public static int isPaused = -1;


	private void OnEnable()
	{
		ipt.actions.actionMaps[0]["Pause"].performed += OnPause;
		ipt.actions.actionMaps[2]["Pause"].performed += OnPause;
	}


	private void OnDisable()
	{
		ipt.actions.actionMaps[0]["Pause"].performed -= OnPause;
		ipt.actions.actionMaps[2]["Pause"].performed -= OnPause;
	}


	

	private void Awake()
	{
		containerMenu.SetActive(false);
	}

	private void OnPause(InputAction.CallbackContext obj)
	{
		if (isPaused == -1)
		{
			if (!GameMode.matchIsRunning) return;
			ipt.SwitchCurrentActionMap("UI");
			Time.timeScale = 0;
			containerMenu.SetActive(true);
			isPaused = ipt.playerIndex;
			OnPause();
			firstItem.Select();
		}
		else
		{
			if(isPaused == ipt.playerIndex)
			{
				UnPause();
			}
		}
	}

	private void OnPause()
	{
		containerMenu.transform.DOKill();
		containerMenu.transform.localScale = Vector3.zero;
		containerMenu.transform.DOScale(1, 0.5f).SetUpdate(true).SetEase(Ease.OutBounce);
	}

	public void ToMainMenu()
	{
		SceneManager.LoadScene(0);
		AkSoundEngine.PostEvent("Stop_All_Soft", gameObject);
	}
	public void ToRestart()
	{
		SceneManager.LoadScene(1);
		AkSoundEngine.PostEvent("Stop_All_Soft", gameObject);
	}

	public void UnPause()
	{
		isPaused = -1;
		containerMenu.transform.DOKill();
		containerMenu.transform.DOScale(0, 0.5f).SetUpdate(true).SetEase(Ease.InBounce).OnComplete(CompleteUnpaused);
		Time.timeScale = 1;
		ipt.SwitchCurrentActionMap("Arena");
	}

	private void CompleteUnpaused()
	{
		containerMenu.SetActive(false);
	}
}
