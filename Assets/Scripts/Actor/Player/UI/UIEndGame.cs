﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using DG.Tweening;
using UnityEngine.SceneManagement;

public class UIEndGame : MonoBehaviour
{
	public Image upBar;
	public Image downBar;
	public Selectable firstButton;

	private Vector3 upPos, downPos;

	private void Awake()
	{
		upPos = upBar.rectTransform.localPosition;
		downPos = downBar.rectTransform.localPosition;
		upBar.rectTransform.localPosition = upPos + Vector3.up * 550;
		downBar.rectTransform.localPosition = downPos - Vector3.up * 550;
		upBar.gameObject.SetActive(false);
		downBar.gameObject.SetActive(false);
	}

	private void OnEnable()
	{
		TestMode.OnMatchEnd += OnMatchEnd;
	}

	private void OnDisable()
	{
		TestMode.OnMatchEnd -= OnMatchEnd;
	}

	private void OnMatchEnd(object sender, System.Collections.Generic.List<FinalScoreHandler> e)
	{
		upBar.gameObject.SetActive(true);
		downBar.gameObject.SetActive(true);
		CloseScreen();
	}

	private void CloseScreen()
	{
		upBar.rectTransform.DOLocalMove(upPos, 1.5f).SetEase(Ease.InCubic).SetDelay(6);
		downBar.rectTransform.DOLocalMove(downPos, 1.5f).SetEase(Ease.InCubic).SetDelay(6).OnComplete(AnimationEnd);
	}

	private void AnimationEnd()
	{
		firstButton.Select();
	}

	public void Restart()
	{
		//todo
		SceneManager.LoadScene(1);
		AkSoundEngine.PostEvent("Stop_All_Soft", gameObject);
	}

	public void ToMainMenu()
	{
		//todo
		SceneManager.LoadScene(0);
		AkSoundEngine.PostEvent("Stop_All_Soft", gameObject);
	}
}
