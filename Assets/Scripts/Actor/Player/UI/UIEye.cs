﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Actor.Player.Stare;
using DG.Tweening;

public class UIEye : MonoBehaviour
{
	private Animator anim;
	private RectTransform rectTransform;

	public StaminaHandler staminaHandler;
	public StareHandler stareHandler;
	public PlayerController controller;

	//public Transform[] TicPulse;
	public Transform[] lowLevel;

	private Vector3 localPos;
	private Vector3 targetLocPos;

	private void Awake()
	{
		anim = GetComponent<Animator>();
		rectTransform = GetComponent<RectTransform>();
		localPos = transform.localPosition;
	}

	private void OnEnable()
	{
		staminaHandler.OnStaminaRegen += OnStaminaChange;
		staminaHandler.OnStaminaUsed += OnStaminaChange;
		staminaHandler.OnStaminaNotEnough += OnStaminaNotEnough;
		stareHandler.OnStareStart += OnStareStart;
		stareHandler.OnStareStop += OnStareStop;
		stareHandler.OnStareTick += OnStareTick;
		GameMode.OnPlayerJoin += OnPlayerInit;
		MusicManager.OneSecBeforeMatchStart += OnMatchStart;
	}

	

	private void OnDisable()
	{
		staminaHandler.OnStaminaRegen -= OnStaminaChange;
		staminaHandler.OnStaminaUsed -= OnStaminaChange;
		staminaHandler.OnStaminaNotEnough -= OnStaminaNotEnough;
		stareHandler.OnStareStart -= OnStareStart;
		stareHandler.OnStareStop -= OnStareStop;
		stareHandler.OnStareTick -= OnStareTick;
		GameMode.OnPlayerJoin -= OnPlayerInit;
		MusicManager.OneSecBeforeMatchStart -= OnMatchStart;
	}

	private void OnPlayerInit(object sender, UnityEngine.InputSystem.PlayerInput e)
	{
		switch (controller.GetPlayerIndex())
		{
			case 0:
				targetLocPos = localPos;
				transform.localPosition = targetLocPos + Vector3.up * 200;
				break;
			case 1:
				targetLocPos = new Vector3(-localPos.x, localPos.y, localPos.z);
				transform.localPosition = targetLocPos + Vector3.up * 200;

				break;
			case 2:
				targetLocPos = new Vector3(localPos.x, -localPos.y, localPos.z);
				transform.localPosition = targetLocPos - Vector3.up * 200;

				break;
			case 3:
				targetLocPos = new Vector3(-localPos.x, -localPos.y, localPos.z);
				transform.localPosition = targetLocPos - Vector3.up * 200;


				break;
		}
		if (GameMode.instance.isMatchStarted)
		{
			transform.DOLocalMove(targetLocPos, 1).SetEase(Ease.InCubic);
		}
	}

	private void OnMatchStart(object sender, System.EventArgs e)
	{
		transform.DOLocalMove(targetLocPos, 1).SetEase(Ease.InCubic);
	}

	private void OnStareTick(object sender, System.EventArgs e)
	{
		/*foreach(Transform t in TicPulse)
		{
			t.DOShakeScale(0.3f, 1, 10, 0, true);
		}*/
		anim.ResetTrigger("Tick");
		anim.SetTrigger("Tick");
	}

	private void Update()
	{

		anim.SetFloat("Stamina", StaminaToAnim(staminaHandler.GetStamina() / staminaHandler.Stamina.settings.maxStamina));

	}


	private void OnStareStop(object sender, System.EventArgs e)
	{
		anim.SetBool("Stare", false);
		foreach (Transform t in lowLevel)
		{
			t.gameObject.SetActive(false);
		}
	}

	private void OnStareStart(object sender, System.EventArgs e)
	{
		anim.SetBool("Stare", true);
	}

	private void OnStaminaChange(object sender, float e)
	{
		/*if(staminaHandler.GetNormalizedStamina() < 0.3f && stareHandler.isStaring)
		{
			foreach (Transform t in lowLevel)
			{
				t.gameObject.SetActive(true);
				t.DOKill(true);
				t.DOShakePosition(0.2f,0.1f,10, 50, false, true);
			}
		}
		else
		{
			foreach (Transform t in lowLevel)
			{
				t.gameObject.SetActive(false);
			}
		}*/

		if(staminaHandler.GetNormalizedStamina() >= 1)
		{
			rectTransform.DOKill(true);
			rectTransform.DOPunchScale(Vector3.one * 1.2f, 0.5f, 3, 0.3f);
		}
		//anim.SetFloat("Stamina", StaminaToAnim(staminaHandler._stamina/staminaHandler.Stamina.settings.maxStamina));
	}

	private void OnStaminaNotEnough(object sender, System.EventArgs e)
	{
		rectTransform.DOKill(true);
		rectTransform.DOPunchScale(Vector3.one * 1.2f, 0.5f, 3, 0.3f);
	}

	private float StaminaToAnim(float normStamina)
	{
		return Mathf.Clamp01(normStamina) * -1 + 1;
	}
}
