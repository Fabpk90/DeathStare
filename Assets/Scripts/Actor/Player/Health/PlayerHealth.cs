using System;
using System.Collections.Generic;
using Actor.Hittable;
using UnityEngine;

namespace Actor.Player
{
    public class PlayerHealth : HealthManager
    {
        public HealthSettings Settings;
        private PlayerController _controller;
        private CooldownTimer _timer;
        //Sound
        private PlayerAudioManager playerAudioManager;
        //Sound

        public bool canTakeDamage = true;
		
        public event EventHandler<Tuple<float,int>> OnTakingDamage;
        public event EventHandler<Tuple<float, int>> OnTakingTick;

		//sends the index of the player not receiving damage anymore
		public event EventHandler<int> OnStopDamage;

        private List<int> _playersAttackingMe;

        private int _lastAttacker;

        private CooldownTimer _timerDissolve;
        private float _timerDissolveTime;
        public GameObject[] DissolveMaterials;
        public AnimationCurve dissolveCurve;
        private static readonly int StepTime = Shader.PropertyToID("StepTime");

        public void ActivateInvicibility(float time)
        {
            if(!canTakeDamage && health <= 0) return;
            canTakeDamage = false;
            
            _timer.Start(time);
        }

        protected override void OnAwake()
        {
            maxHealth = Settings.maxHealth;
            base.OnAwake();
        }

        private void Start()
        {
            _lastAttacker = -1;
            _timerDissolveTime = TestMode.GetInstance().SettingSerialized.respawnCooldown;
            
            _controller = GetComponent<PlayerController>();
            _timer = new CooldownTimer(0);
            _timerDissolve = new CooldownTimer(_timerDissolveTime);
            _playersAttackingMe = new List<int>(3);

            _timer.TimerCompleteEvent += () =>
            {
                canTakeDamage = true;
            };
            
            _controller.OnRespawn += OnRespawn;
            _controller.stareHandler.OnStareStop += (sender, args) =>
            {
                ActivateInvicibility(0.4f);
            };
            OnTakingDamage += PlayDamageSound;
        }

        private void OnRespawn(object sender, EventArgs e)
        {
            health = maxHealth;
            _lastAttacker = -1;
            
            _timer.Start();
            
            foreach (GameObject material in DissolveMaterials)
            {
                var materials = material.GetComponent<Renderer>().materials;
                foreach (var mat in  materials)
                {
                    mat.SetFloat(StepTime, 0);
                }
            }
        }

        private void Update()
        {
            //Sound
            if (playerAudioManager == null)
            {
                playerAudioManager = AudioManager.instance.GetPlayerAudioManager(_controller.GetPlayerIndex());
            }
            playerAudioManager.SetRTPCValue("RTPC_Character_Health", health);
            //Sound

            _timer.Update(Time.deltaTime);
            _timerDissolve.Update(Time.deltaTime);
            
            if (_timerDissolve.IsActive)
            {
                foreach (GameObject material in DissolveMaterials)
                {
                    var materials = material.GetComponent<Renderer>().materials;
                    foreach (var mat in  materials)
                    {
                        mat.SetFloat(StepTime, dissolveCurve.Evaluate(_timerDissolve.PercentElapsed));
                    }
                }
            }
        }

        protected override void Die()
        {
            health = -1f;
            canTakeDamage = false;
            
            _controller.controller.SetInputMovement(Vector2.zero);
            _controller.GetInput().SwitchCurrentActionMap("Death");
            _controller.ChangeBodyVisibility(true);
            
            //Animation maybe ?
            //Sound
            if (playerAudioManager == null)
            {
                playerAudioManager = AudioManager.instance.GetPlayerAudioManager(_controller.GetPlayerIndex());
            }
            playerAudioManager.PostEvent("VO_Char_Barks_Death");
            playerAudioManager.PostEvent("EFFECTS_Char_Respawn");
            //Sound

            _timerDissolve.Start();
        }

        public override EHitType TakeDamage(int playerIndex, float amount)
        {
            if (!canTakeDamage) return EHitType.BLOCKED;
            
            OnTakingTick?.Invoke(this, new Tuple<float, int>(amount, playerIndex));
            
            if (!_playersAttackingMe.Contains(playerIndex))
                _playersAttackingMe.Add(playerIndex);
            
            _lastAttacker = playerIndex;

			if (base.TakeDamage(playerIndex, amount) == EHitType.TOUCHED)
			{
				OnTakingDamage?.Invoke(this, new Tuple<float, int>(amount, playerIndex));
				return EHitType.TOUCHED;
			}

			OnTakingDamage?.Invoke(this, new Tuple<float, int>(amount, playerIndex));

            GameMode.instance.PlayerKilled(playerIndex, _controller.GetPlayerIndex());
            
            return EHitType.DEATH;
        }

        public void UnAttack(int playerNotAttacking)
        {
            _playersAttackingMe.Remove(playerNotAttacking);
            
            if(_playersAttackingMe.Count == 0)
                OnStopDamage?.Invoke(this, _controller.GetPlayerIndex());
        }

        public void Suicide()
        {
            if(health <= 0) return;
            
            Die();
            
            GameMode.instance.PlayerKilled(_lastAttacker, _controller.GetPlayerIndex());
        }

        //Sound
        private void PlayDamageSound(object sender, Tuple<float, int> t)
        {
            if (playerAudioManager == null)
            {
                playerAudioManager = AudioManager.instance.GetPlayerAudioManager(_controller.GetPlayerIndex());
            }
            playerAudioManager.SetRTPCValue("RTPC_Character_Health", health);
            //            Debug.Log(_controller.GetPlayerIndex() + " " + health);
            playerAudioManager.PostEvent("EFFECTS_Char_Damage");
            if (health > 1)
            {
                playerAudioManager.PostEvent("VO_Char_Barks_Damage");
            }
        }
        //Sound
    }
}
