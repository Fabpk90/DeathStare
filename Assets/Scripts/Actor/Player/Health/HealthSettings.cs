using UnityEngine;

namespace Actor.Player
{
    [CreateAssetMenu(fileName = "Actor/Health")]
    public class HealthSettings : ScriptableObject
    {
        public float maxHealth;
    }
}