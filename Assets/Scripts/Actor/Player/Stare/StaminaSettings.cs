using System;
using UnityEngine;

namespace Actor.Player.Stare
{
    [Serializable]
    public class Stamina
    {
        public float maxStamina;
        public float burstRegen;
        public float delayBeforeRegen;
        public float regenFromTickTouch;
        
        [Header("Tick")]
        public float stareCostPerTick;
        public float stareRegenPerSecond;
    }
    
    [CreateAssetMenu(menuName = "Actor/Stare/StaminaSettings")]
    public class StaminaSettings : ScriptableObject
    {
        public Stamina settings;
    }
}