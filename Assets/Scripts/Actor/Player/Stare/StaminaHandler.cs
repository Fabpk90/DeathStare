using System;
using UnityEngine;

namespace Actor.Player.Stare
{
    public class StaminaHandler : MonoBehaviour
    {
        public StaminaSettings Stamina;

        private float _stamina;
        private StareHandler _stareHandler;

        public event EventHandler<float> OnStaminaRegen;
        public event EventHandler<float> OnStaminaUsed;

        public event EventHandler OnStaminaNotEnough;

        private CooldownTimer _cooldownRegen;
        private bool _isRegen;
        private bool _isBurntOut;

        public float GetStamina()
        {
            return _stamina;
        }
        
        private void OnEnable()
        {
            _stareHandler.OnStareTick += OnStareTicking;
            _stareHandler.OnStareStop += OnStareStopping;
            _stareHandler.OnStareTouchOnce += OnStareTouches;
        }

        private void OnStareTouches(object sender, EventArgs e)
        {
            _stamina += Stamina.settings.regenFromTickTouch + Stamina.settings.stareCostPerTick;

            if (_stamina > Stamina.settings.maxStamina)
                _stamina = Stamina.settings.maxStamina;
        }

        private void OnDisable()
        {
            _stareHandler.OnStareTick -= OnStareTicking;
            _stareHandler.OnStareStop -= OnStareStopping;
            _stareHandler.OnStareTouchOnce -= OnStareTouches;
        }

		public float GetNormalizedStamina()
		{
			return _stamina / Stamina.settings.maxStamina;
		}

        private void OnStareTicking(object sender, EventArgs e)
        {
            _isRegen = false;
            _cooldownRegen.Pause();
            
            _stamina -= Stamina.settings.stareCostPerTick;

            if (_stamina <= 0)
            {
                OnStaminaNotEnough?.Invoke(this, EventArgs.Empty);
                _stareHandler.StopStare();
                _isBurntOut = true;
            }
            else
            {
                OnStaminaUsed?.Invoke(this, _stamina);
            }
        }
        
        private void OnStareStopping(object sender, EventArgs e)
        {
            _cooldownRegen.Start();

            if (_stamina > 0)
            {
                _stamina += Stamina.settings.burstRegen;

                if (_stamina > Stamina.settings.maxStamina)
                    _stamina = Stamina.settings.maxStamina;
                OnStaminaRegen?.Invoke(this, _stamina);
            }
        }

        private void Awake()
        {
            _stareHandler = GetComponent<StareHandler>();
        }

        private void Start()
        {
            _cooldownRegen = new CooldownTimer(Stamina.settings.delayBeforeRegen);
            _cooldownRegen.TimerCompleteEvent += () =>
            {
                _isRegen = true;
                if (_isBurntOut)
                {
                    _stamina = Stamina.settings.maxStamina;
                    _isBurntOut = false;
                    OnStaminaRegen?.Invoke(this, _stamina);
                }
            };

            _stamina = Stamina.settings.maxStamina;
            OnStaminaRegen?.Invoke(this, _stamina);
        }

        private void Update()
        {
            _cooldownRegen.Update(Time.deltaTime);

            if(_isRegen)
            {
                _stamina += Stamina.settings.stareRegenPerSecond * Time.deltaTime;
                
                if (_stamina > Stamina.settings.maxStamina)
                {
                    _isRegen = false;
                    _stamina = Stamina.settings.maxStamina;
                }
                OnStaminaRegen?.Invoke(this, _stamina);
            }
        }

        public void RaiseNotEnoughStamina()
        {
            OnStaminaNotEnough?.Invoke(this, EventArgs.Empty);
        }
    }
}