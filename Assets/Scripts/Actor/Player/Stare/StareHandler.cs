using System;
using System.Collections.Generic;
using Actor;
using Actor.Hittable;
using Actor.Player;
using Actor.Player.Stare;
using UnityEngine;
using UnityEngine.Serialization;

public class StareHandler : MonoBehaviour
{
    public static readonly List<HittablePoint> HittablePoints = new List<HittablePoint>(500);

    public bool isStaring;
    [FormerlySerializedAs("camera")] public Camera Cam;
    private HashSet<HealthManager> _hitDuringThisFrame;
    public List<PlayerController> playersHitDuringThisFrame;
    private List<PlayerController> _playersHitDuringLastFrame;
    private List<PlayerController> _playerKilledDuringFrame;
    private List<HittablePoint> _hittableToPush;

    private PlayerController _controller;
    private StaminaHandler _staminaHandler;
    public PlayerUIManager UIManager;

    public bool debugRay;
    
    public LayerMask mask;

    public StareSettings stareParams;
  //  private StareSettingsHelper _stareSettings;

    private CooldownTimer _cooldownDamageStare;
    private CooldownTimer _cooldownLoadingStare;
    public event EventHandler OnStareStart;
    //fired on hitting someone
    public event EventHandler<int> OnStareTouch;
    public event EventHandler OnStareTouchOnce;
    public event EventHandler OnStareStop;
    public event EventHandler OnStareTick;

    public event EventHandler<int> OnStareKill;
    public event EventHandler<List<int>> OnStareBlocked;

    public event EventHandler OnStarePushInTheAir;

    public event EventHandler<List<int>> OnDuelStart; 
    public event EventHandler<List<int>> OnDuelContinue; 
    public event EventHandler<int> OnDuelStop;
    private List<int> _actorsInDuel;
    private bool _alreadyInDuel;

	public float stareRecoilGroundedDelay = 0.15f;
   
    private void Awake()
    {
        //_stareSettings = stareParams.settings;

        Cam.fieldOfView = stareParams.settings.FOVNormal;
        
        _controller = GetComponentInParent<PlayerController>();
        _staminaHandler = GetComponent<StaminaHandler>();
      
        _hitDuringThisFrame = new HashSet<HealthManager>();
        
        playersHitDuringThisFrame = new List<PlayerController>(3);
        _playerKilledDuringFrame = new List<PlayerController>(3);
        _playersHitDuringLastFrame = new List<PlayerController>(3);
        _hittableToPush = new List<HittablePoint>(20);
      
        _actorsInDuel = new List<int>(3);
      
        _cooldownDamageStare = new CooldownTimer(1.0f / stareParams.settings.damageTickRate);
        _cooldownDamageStare.TimerCompleteEvent += () =>
        {
            StareTick(stareParams.settings.damagePerTick);
        };
        
        _cooldownLoadingStare = new CooldownTimer(stareParams.settings.loadingTimeToStare);
        _cooldownLoadingStare.TimerCompleteEvent += () => StareTick(stareParams.settings.damageFirstTick);
    }

    /// <summary>
    /// Check if the actor is staring at something
    /// </summary>
    /// <returns> true if he/she is, false otherwise</returns>
    private bool CheckForThingsInSight()
    {
        _playersHitDuringLastFrame.Clear();
        _hitDuringThisFrame.Clear();
        _hittableToPush.Clear();
        
        _playersHitDuringLastFrame.AddRange(playersHitDuringThisFrame);
        playersHitDuringThisFrame.Clear();

        bool found = false;
        // var viewHeight = VignetteManager.GetViewHeight();
        var viewHeight = UIManager.GetViewHeight();

        for (var index = 0; index < HittablePoints.Count; index++)
        {
            HittablePoint point = HittablePoints[index];
            if (!point) continue;

            Vector3 viewportPoint = Cam.WorldToViewportPoint(point.GetPosition());

            if (!(viewportPoint.z > 0) || !(viewportPoint.x > 0) || !(viewportPoint.x < 1) ||
                !(viewHeight.x < viewportPoint.y) || !(viewHeight.y > viewportPoint.y) ||
                _hitDuringThisFrame.Contains(point.healthManager)) continue;

            var worldRay = Cam.ViewportPointToRay(viewportPoint);

            if (!Physics.Raycast(worldRay, out var hitInfo, float.PositiveInfinity, mask,
                QueryTriggerInteraction.Ignore)) continue;

            if (debugRay)
            {
                Debug.DrawRay(worldRay.origin, worldRay.direction * 100.0f, Color.red);
                print(worldRay);
                print(viewportPoint);
            }

            IHittable hit = hitInfo.transform.GetComponent<IHittable>();

            if (hit == null || hitInfo.transform.root.gameObject != point.transform.root.gameObject) continue;


            _hitDuringThisFrame.Add(point.healthManager);
            found = true;

            PlayerController p = point.healthManager.GetComponent<PlayerController>();

            if (p)
            {
                //we hit a player !
                playersHitDuringThisFrame.Add(p);
            }
            else
            {
                _hittableToPush.Add(point);
            }
        }

        CheckForNotAttackingPlayers();

        return found;
    }

    private void CheckForNotAttackingPlayers()
    {
        List<PlayerController> playersNotAttacked = new List<PlayerController>(3);

        for (var index = 0; index < _playersHitDuringLastFrame.Count; index++)
        {
            var player = _playersHitDuringLastFrame[index];
            if (!playersHitDuringThisFrame.Contains(player))
            {
                playersNotAttacked.Add(player);
            }
        }

        for (var index = 0; index < playersNotAttacked.Count; index++)
        {
            PlayerController playerController = playersNotAttacked[index];
            playerController.health.UnAttack(_controller.GetPlayerIndex());
        }
    }

    private void Update()
    {
        if (!isStaring)
        {
            Cam.fieldOfView = Mathf.Lerp(Cam.fieldOfView, stareParams.settings.FOVNormal,
                stareParams.settings.FOVChangingSpeed * Time.deltaTime);
        }
        else
        {
            CheckForThingsInSight();
            
            _cooldownLoadingStare.Update(Time.deltaTime);
            if (_cooldownLoadingStare.IsCompleted)
            {
                _cooldownDamageStare.Update(Time.deltaTime);
            }
            
            Cam.fieldOfView = Mathf.Lerp(Cam.fieldOfView, stareParams.settings.FOVStare,
                stareParams.settings.FOVChangingSpeed * Time.deltaTime);
        }
    }

    private void StareTick(float amount)
    {
        _cooldownDamageStare.Start(1.0f / stareParams.settings.damageTickRate);

        _actorsInDuel.Clear();

        PushAllHittables();
        
        bool touched = false;
        
        for (var index = 0; index < playersHitDuringThisFrame.Count; index++)
        {
            PlayerController playerController = playersHitDuringThisFrame[index];
            if (!playerController) continue;

            //is he/she staring at us ?
            if (!playerController.stareHandler.playersHitDuringThisFrame.Contains(_controller))
            {
                //Physics
                Vector3 repulsionForce = playerController.transform.position - transform.position;
                playerController.AddForce((repulsionForce.normalized * stareParams.settings.stareForce) +
                                          repulsionForce.normalized * (stareParams.settings.forceOverDistance.Evaluate(
                                                                           stareParams.settings.maxDistance /
                                                                           repulsionForce.magnitude) *
                                                                       stareParams.settings.maxDistanceForce));
                //Sound
                switch (playerController.GetPlayerIndex())
                {
                    case (0):
                        AkSoundEngine.SetState("STATE_Music_DuelState_J1", "False");
                        break;
                    case (1):
                        AkSoundEngine.SetState("STATE_Music_DuelState_J2", "False");
                        break;
                    case (2):
                        AkSoundEngine.SetState("STATE_Music_DuelState_J3", "False");
                        break;
                    case (3):
                        AkSoundEngine.SetState("STATE_Music_DuelState_J4", "False");
                        break;
                }
                //Sound

                switch (playerController.health
                    .TakeDamage(_controller.GetPlayerIndex(), amount))
                {
                    case EHitType.TOUCHED:
                        touched = true;
                        OnStareTouch?.Invoke(this, playerController.GetPlayerIndex());
                        break;
                    case EHitType.BLOCKED:
                        break;
                    case EHitType.DEATH:
                        //the mf is ded
                        touched = true;
                        _playerKilledDuringFrame.Add(playerController);
                        OnStareKill?.Invoke(this, playerController.GetPlayerIndex());
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
            else // it's staring back at us !
            {
                _actorsInDuel.Add(playerController.GetPlayerIndex());
                // print("Duel with " + playerController.GetComponent<HealthManager>().transform.gameObject);
                //Sound
                switch (playerController.GetPlayerIndex())
                {
                    case (0):
                        AkSoundEngine.SetState("STATE_Music_DuelState_J1", "True");
                        break;
                    case (1):
                        AkSoundEngine.SetState("STATE_Music_DuelState_J2", "True");
                        break;
                    case (2):
                        AkSoundEngine.SetState("STATE_Music_DuelState_J3", "True");
                        break;
                    case (3):
                        AkSoundEngine.SetState("STATE_Music_DuelState_J4", "True");
                        break;
                }

                //Sound
            }
        }
        
        if(touched)
            OnStareTouchOnce?.Invoke(this, EventArgs.Empty);

        for (var index = 0; index < _playerKilledDuringFrame.Count; index++)
        {
            PlayerController controller = _playerKilledDuringFrame[index];
            playersHitDuringThisFrame.Remove(controller);
        }

        _playerKilledDuringFrame.Clear();

        if (_actorsInDuel.Count > 0)
        {
            OnStareBlocked?.Invoke(this, _actorsInDuel);
            if (!_alreadyInDuel)
            {
                _alreadyInDuel = true;
                OnDuelStart?.Invoke(this, _actorsInDuel);
            }
            else
            {
                OnDuelContinue?.Invoke(this, _actorsInDuel);
            }
        }
        else
        {
            if(_alreadyInDuel)
                OnDuelStop?.Invoke(this, _controller.GetPlayerIndex());
         
            _alreadyInDuel = false;
        }
        OnStareTick?.Invoke(this, null);
    }

    private void PushAllHittables()
    {
        for (var index = 0; index < _hittableToPush.Count; index++)
        {
            //we don't push if the thing is ded
            if (_hittableToPush[index].TakeDamage(_controller.GetPlayerIndex(), stareParams.settings.damagePerTick) ==
                EHitType.DEATH) continue;
            
            HittablePoint hittable = _hittableToPush[index];
            Vector3 repulsionForce = hittable.GetPosition() - transform.position;

            Vector3 force = repulsionForce.normalized * stareParams.settings.stareForce +
                            repulsionForce.normalized
                            * (stareParams.settings.forceOverDistance.Evaluate(
                                   stareParams.settings.maxDistance / repulsionForce.magnitude)
                               * stareParams.settings.maxDistanceForce);
            hittable.AddForce(force * 5.0f);
        }
    }

    public void StartStare()
    {
        if (_staminaHandler.GetStamina() <= 0)
        {
            _staminaHandler.RaiseNotEnoughStamina();
            return;
        }
        isStaring = true;

        OnStareStart?.Invoke(this, null);
        CheckForThingsInSight();
        
        _cooldownLoadingStare.Start(stareParams.settings.loadingTimeToStare);
        
        //backwards force when ticking in air
        if (!_controller.controller.isGrounded && Time.time > _controller.controller.lastGroundedTime + stareRecoilGroundedDelay)
        {
            OnStarePushInTheAir?.Invoke(this, EventArgs.Empty);
            _controller.AddForce(-Cam.transform.forward * stareParams.settings.aerialRecoilForce);
        }
    }

    public void StopStare()
    {
        if (!isStaring) return;
      
        isStaring = false;

        if (_alreadyInDuel)
        {
            OnDuelStop?.Invoke(this, _controller.GetPlayerIndex());
            _alreadyInDuel = false;
        }
      
        OnStareStop?.Invoke(this, null);
      
        playersHitDuringThisFrame.Clear();
        _cooldownDamageStare.Start(1.0f / stareParams.settings.damageTickRate); // resets the damage cooldown
      
        CheckForNotAttackingPlayers();
    }

    public int GetPlayerIndex()
    {
        return _controller.GetPlayerIndex();
    }
}