using System;
using UnityEngine;

namespace Actor.Player.Stare
{
    [Serializable]
    public class StareSettingsHelper
    {
        public float stareForce;
        public float aerialRecoilForce;
        public AnimationCurve forceOverDistance;
        public float maxDistance;
        public float maxDistanceForce;

        [Tooltip("Le nombres de ticks par secondes")]
        public float damageTickRate;
        public int damagePerTick;
        public int damageFirstTick; 

        public float loadingTimeToStare;

        [Header("FOV")]
        public float FOVChangingSpeed;
        [Range(45, 90)]
        public float FOVNormal;

        [Range(45, 90)]
        public float FOVStare;
    }
    [CreateAssetMenu(fileName = "Actor/Stare/StareSettings")]
    public class StareSettings : ScriptableObject
    {
        public StareSettingsHelper settings;
    }
}