using Actor.Hittable;
using UnityEngine;
using System;

namespace Actor.Props
{
    public class PropsHealthManager : HealthManager
    {
        public bool takesDamage;

        /*
        private void Awake()
        {
            //Sound
            GameMode.OnStartOfMatch += OnMatchStart;
            AkSoundEngine.SetSwitch("SWITCHES_Foleys_Things", "Box", gameObject);
            //Sound
        }

        private void OnDisable()
        {
            GameMode.OnStartOfMatch -= OnMatchStart;
        }

        private void OnMatchStart(object sender, EventArgs eventArgs)
        {
            AudioManager.instance.AddListeners(gameObject, 0, 1, 2, 3);
        }
        */

        public override EHitType TakeDamage(int playerIndex, float amount)
        {
            if(takesDamage)
                return base.TakeDamage(playerIndex, amount);
            return EHitType.BLOCKED;
        }

        protected override void Die()
        {
            base.Die();
            Destroy(gameObject);
        }
    }
}