using System;
using Actor.Hittable;
using UnityEngine;

namespace Actor.Props
{
    public class Props : PropsHealthManager
    {
        private CooldownTimer _timer;
        public float timeToRespawn;

        private CooldownTimer _dissolveTimer;
        public float timeToDissolve;
        public AnimationCurve dissolveCurve;

        private CooldownTimer _respawnDissolveTimer;
        public float respawnDissolveTime;
        public AnimationCurve dissolveRespawnCurve;
        
        public GameObject respawnPoint;
        public bool isStartPosRespawnPoint;

        private Renderer _renderer;
        private static readonly int StepTime = Shader.PropertyToID("StepTime");

		public float partTriggerThreshold = 1f;
		public ParticleSystem partSystem;

        protected override void OnAwake()
        {
            base.OnAwake();
            if (isStartPosRespawnPoint)
            {
                respawnPoint = new GameObject();
                respawnPoint.transform.position = transform.position;
            }
            
            _respawnDissolveTimer = new CooldownTimer(respawnDissolveTime);

            _dissolveTimer = new CooldownTimer(timeToDissolve);
            _dissolveTimer.TimerCompleteEvent += () =>
            {
                Respawn(respawnPoint.transform.position);
                _respawnDissolveTimer.Start();
            };

            _timer = new CooldownTimer(timeToRespawn);
            _timer.TimerCompleteEvent += () =>
            {
                _dissolveTimer.Start();
            };

            _renderer = GetComponent<Renderer>();

            //Sound
            GameMode.OnStartOfMatch += OnMatchStart;
            AkSoundEngine.SetSwitch("SWITCHES_Foleys_Things", "Box", gameObject);
            //Sound
        }

        private void OnDisable()
        {
            GameMode.OnStartOfMatch -= OnMatchStart;
        }

        private void OnMatchStart(object sender, EventArgs eventArgs)
        {
            AudioManager.instance.AddListeners(gameObject, 0, 1, 2, 3);
        }

        public override EHitType TakeDamage(int playerIndex, float amount)
        {
            if (!_dissolveTimer.IsActive)
            {
                _timer.Start();
                return base.TakeDamage(playerIndex, amount);
            }

            return EHitType.DEATH;
        }

        public void Respawn(Vector3 position)
        {
			transform.position = position;
			transform.rotation = Quaternion.identity;
			Rigidbody rb = GetComponent<Rigidbody>();
			if (!rb) return;
			rb.velocity = Vector3.zero;
			rb.angularVelocity = Vector3.zero;	
        }

        private void Update()
        {
            _timer.Update(Time.deltaTime);
            _dissolveTimer.Update(Time.deltaTime);
            _respawnDissolveTimer.Update(Time.deltaTime);
            if (_dissolveTimer.IsActive)
            {
                foreach (Material material in _renderer.materials)
                {
                    material.SetFloat(StepTime, dissolveCurve.Evaluate(_dissolveTimer.PercentElapsed));
                }
            }
            else if (_respawnDissolveTimer.IsActive)
            {
                foreach (Material material in _renderer.materials)
                {
                    material.SetFloat(StepTime,
                        1 - dissolveRespawnCurve.Evaluate(_respawnDissolveTimer.PercentElapsed));
                }
            }
        }

		private void PlayVfxAtPos(Vector3 worldPos, Vector3 normal)
		{
			if (!partSystem) return;
			partSystem.transform.position = worldPos;

			Quaternion q = Quaternion.LookRotation(Vector3.Cross(normal, transform.forward), normal);
			partSystem.transform.rotation = q;
			partSystem.Play();
		}
        
		private void OnCollisionEnter(Collision collision)
		{
			float impactforce = collision.relativeVelocity.x + collision.relativeVelocity.y + collision.relativeVelocity.z;
            
			if(collision.impulse.sqrMagnitude > partTriggerThreshold * partTriggerThreshold)
			{
                //Sound
                gameObject.GetComponent<DynamicObstructionReference>().CheckObstruction();
                AkSoundEngine.PostEvent("FOLEYS_Props_Thing_Hit", gameObject);
                //Sound
                PlayVfxAtPos(collision.contacts[0].point,collision.contacts[0].normal);
			}
		}

		private void OnCollisionStay(Collision collision)
		{
			float impactforce = collision.relativeVelocity.x + collision.relativeVelocity.y + collision.relativeVelocity.z;
            
			if (collision.impulse.sqrMagnitude > partTriggerThreshold * partTriggerThreshold)
			{
                //Sound
                gameObject.GetComponent<DynamicObstructionReference>().CheckObstruction();
                AkSoundEngine.PostEvent("FOLEYS_Props_Thing_Hit", gameObject);
                //Sound
                PlayVfxAtPos(collision.contacts[0].point,collision.contacts[0].normal);
			}
		}
	}
}