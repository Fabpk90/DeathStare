using System;
using UnityEngine;

namespace Actor.Props
{
    [RequireComponent(typeof(SphereCollider))]
    public class HealthPack : MonoBehaviour
    {
        public float collectRange;
        public float amount;
        public float cooldown;

        public GameObject toDeactivate;

		// private CooldownTimer _cooldownRespawnTimer;
		//  private Collider[] _colliders;

		/*  private void Awake()
		  {
			  //_colliders = new Collider[50];

			 // _cooldownRespawnTimer = new CooldownTimer(cooldown);
			 /* _cooldownRespawnTimer.TimerCompleteEvent += () =>
			  {
				  toDeactivate.SetActive(true);

				  var size = Physics.OverlapSphereNonAlloc(transform.position, collectRange, _colliders);

				  for (int i = 0; i < size; i++)
				  {
					  PlayerController p = _colliders[i].transform.GetComponent<PlayerController>();

					  if (!p) continue;

					  if (p.health.RegenHealth(amount))
					  {
					  //    toDeactivate.SetActive(false);
					  //    _cooldownRespawnTimer.Start();
					  }

					  break;
				  }
			  };*/
		// }

		/*  private void Update()
		  {
			  _cooldownRespawnTimer.Update(Time.deltaTime);
		  }*/

		private void OnEnable()
		{
			HealthPackManager.healthPackList.Add(this);
		}

		private void OnDisable()
		{
			HealthPackManager.healthPackList.Remove(this);
		}

		public void Activate(bool b)
		{
			toDeactivate.SetActive(b);
		}


        private void OnTriggerEnter(Collider other)
        {
            PlayerController p = other.transform.GetComponent<PlayerController>();

            if (p && toDeactivate.activeSelf)
            {
                if (p.health.RegenHealth(amount))
                {
                    //Sound
                    AudioManager.instance.GetPlayerAudioManager(p.GetPlayerIndex()).PostEvent("EFFECTS_Char_MedicKit");
					//Sound
					HealthPackManager.instance.Use(this);
                   // toDeactivate.SetActive(false);
                    //_cooldownRespawnTimer.Start();
                }
            }
        }
    }
}