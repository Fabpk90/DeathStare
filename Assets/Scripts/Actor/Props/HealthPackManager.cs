﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Actor.Props;

public class HealthPackManager : MonoBehaviour
{

	public static HealthPackManager instance;

	public int maxHealthPack = 2;
	public float cooldDownToRespawn = 100;

	public static List<HealthPack> healthPackList = new List<HealthPack>();

	private List<HealthPack> activeList = new List<HealthPack>();

	private void Awake()
	{
		if (instance != null)
		{
			Destroy(instance);
			return;
		}

		instance = this;

	}

	private void Start()
	{
		foreach(HealthPack h in healthPackList)
		{
			h.Activate(false);
		}
		for (int i = 0; i < maxHealthPack; i++)
		{
			SetActive(healthPackList[Random.Range(0, healthPackList.Count)]);
		}
	}


	public void Use(HealthPack p)
	{
		p.Activate(false);
		activeList.Remove(p);
		healthPackList.Add(p);
		StartCoroutine(WaitAndSpawn());
	}

	private void SetActive(HealthPack p)
	{
		p.Activate(true);
		activeList.Add(p);
		healthPackList.Remove(p);
	}

	private IEnumerator WaitAndSpawn()
	{
		yield return new WaitForSeconds(cooldDownToRespawn);
		SetActive(healthPackList[Random.Range(0, healthPackList.Count)]);
	}
}
