using System;
using System.Collections.Generic;
using UnityEngine;

namespace Actor.Props
{
    public class PropsPoolManager : MonoBehaviour
    {
        public static PropsPoolManager instance;
        public Transform[] spawnPoints;
        public GameObject[] props;

        private List<Tuple<int, List<GameObject>>> _pool;

        private void Awake()
        {
            if (instance)
            {
                Destroy(gameObject);
                return;
            }

            instance = this;
            
            _pool = new List<Tuple<int, List<GameObject>>>(props.Length);

            for (int i = 0; i < props.Length; i++)
            {
               _pool.Add(new Tuple<int, List<GameObject>>(i, new List<GameObject>(5)));

               for (int j = 0; j < 5; j++)
               {
                   _pool[i].Item2[j] = Instantiate(props[i]);
                   _pool[i].Item2[j].SetActive(false);
               }
            }
        }
    }
}