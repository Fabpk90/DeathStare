﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Actor.Player;
using Actor.Props;
[RequireComponent(typeof(BoxCollider))]
public class DeathTrigger : MonoBehaviour
{
	private void OnTriggerEnter(Collider other)
	{
		PlayerHealth hp = other.transform.GetComponent<PlayerHealth>();
		if (hp)
		{
			Debug.Log(hp.name + " Falls");
			hp.Suicide();
		}
		Props p = other.GetComponent<Props>();
		if (p)
		{
			p.Respawn(p.respawnPoint.transform.position);
		}
		
	}
}
