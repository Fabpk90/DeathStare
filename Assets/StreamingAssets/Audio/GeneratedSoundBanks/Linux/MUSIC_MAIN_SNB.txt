Event	ID	Name			Wwise Object Path	Notes
	459653097	STINGERS_DS_Marta_L			\DS EVENTS\MUSIC EVENTS\STINGERS_DS_Marta_L	
	459653111	STINGERS_DS_Marta_R			\DS EVENTS\MUSIC EVENTS\STINGERS_DS_Marta_R	
	554142247	STINGERS_DS_Medusa_R			\DS EVENTS\MUSIC EVENTS\STINGERS_DS_Medusa_R	
	554142265	STINGERS_DS_Medusa_L			\DS EVENTS\MUSIC EVENTS\STINGERS_DS_Medusa_L	
	1290491939	STINGERS_DS_Don_L			\DS EVENTS\MUSIC EVENTS\STINGERS_DS_Don_L	
	1290491965	STINGERS_DS_Don_R			\DS EVENTS\MUSIC EVENTS\STINGERS_DS_Don_R	
	1564958083	Post_Music_All			\DS EVENTS\MUSIC EVENTS\Post_Music_All	
	1704734930	SEGMENTS_MainMusic			\DS EVENTS\MUSIC EVENTS\SEGMENTS_MainMusic	
	1926112901	SEGMENTS_DuelMusic			\DS EVENTS\MUSIC EVENTS\SEGMENTS_DuelMusic	
	2001785879	SEGMENTS_DS_Intensity			\DS EVENTS\MUSIC EVENTS\SEGMENTS_DS_Intensity	
	2011547304	STINGERS_DS_Stan_R			\DS EVENTS\MUSIC EVENTS\STINGERS_DS_Stan_R	
	2011547318	STINGERS_DS_Stan_L			\DS EVENTS\MUSIC EVENTS\STINGERS_DS_Stan_L	
	2299281128	STINGERS_Kill_P2			\DS EVENTS\MUSIC EVENTS\STINGERS_Kill_P2	
	2299281129	STINGERS_Kill_P3			\DS EVENTS\MUSIC EVENTS\STINGERS_Kill_P3	
	2299281131	STINGERS_Kill_P1			\DS EVENTS\MUSIC EVENTS\STINGERS_Kill_P1	
	2299281134	STINGERS_Kill_P4			\DS EVENTS\MUSIC EVENTS\STINGERS_Kill_P4	
	3167003290	Stop_AMB_Menu_Bed			\DS EVENTS\SFX EVENTS\AMBIANCES EVENTS\Main\Stop_AMB_Menu_Bed	
	4049065654	SetState_FightSilence			\DS EVENTS\MUSIC EVENTS\SetState_FightSilence	

State Group	ID	Name			Wwise Object Path	Notes
	1498827748	STATE_Music_Main			\DS STATES\MUSIC STATES\STATE_Music_Main	
	1702469306	STATE_Music_DS_Intensity			\DS STATES\MUSIC STATES\STATE_Music_DS_Intensity	
	2568415073	STATE_Music_DuelState_J4			\DS STATES\MUSIC STATES\STATE_Music_DuelState\STATE_Music_DuelState_J4	
	2568415076	STATE_Music_DuelState_J1			\DS STATES\MUSIC STATES\STATE_Music_DuelState\STATE_Music_DuelState_J1	
	2568415078	STATE_Music_DuelState_J3			\DS STATES\MUSIC STATES\STATE_Music_DuelState\STATE_Music_DuelState_J3	
	2568415079	STATE_Music_DuelState_J2			\DS STATES\MUSIC STATES\STATE_Music_DuelState\STATE_Music_DuelState_J2	
	3974334009	STATE_General_Winner			\DS STATES\GENERAL STATES\STATE_General_Winner	

State	ID	Name	State Group			Notes
	63077693	MainMenu_End	STATE_Music_Main			
	748895195	None	STATE_Music_Main			
	1585161430	Fight_Intro	STATE_Music_Main			
	3604647259	MainMenu	STATE_Music_Main			
	3762960553	Fight_Silence	STATE_Music_Main			
	4184794294	Outro	STATE_Music_Main			
	4258958663	Fight_End	STATE_Music_Main			
	545371365	Low	STATE_Music_DS_Intensity			
	748895195	None	STATE_Music_DS_Intensity			
	1182670505	Mid	STATE_Music_DS_Intensity			
	748895195	None	STATE_Music_DuelState_J4			
	2452206122	False	STATE_Music_DuelState_J4			
	3053630529	True	STATE_Music_DuelState_J4			
	748895195	None	STATE_Music_DuelState_J1			
	2452206122	False	STATE_Music_DuelState_J1			
	3053630529	True	STATE_Music_DuelState_J1			
	748895195	None	STATE_Music_DuelState_J3			
	2452206122	False	STATE_Music_DuelState_J3			
	3053630529	True	STATE_Music_DuelState_J3			
	748895195	None	STATE_Music_DuelState_J2			
	2452206122	False	STATE_Music_DuelState_J2			
	3053630529	True	STATE_Music_DuelState_J2			
	546651172	Don	STATE_General_Winner			
	553997901	Stan	STATE_General_Winner			
	748895195	None	STATE_General_Winner			
	1358509506	Marta	STATE_General_Winner			
	2609729838	Medusa	STATE_General_Winner			

Custom State	ID	Name	State Group	Owner		Notes
	65872737	False	STATE_Music_DuelState_J2	\Interactive Music Hierarchy\DS MUSIC\MAIN\Fight\SEGMENTS_DuelMusic\DuelMusic\Segments_Duel_CelliLoop		
	185289956	False	STATE_Music_DuelState_J4	\Interactive Music Hierarchy\DS MUSIC\MAIN\Fight\SEGMENTS_DuelMusic\DuelMusic\Segments_Duel_Violin2Loop		
	187310372	False	STATE_Music_DuelState_J3	\Interactive Music Hierarchy\DS MUSIC\MAIN\Fight\SEGMENTS_DuelMusic\DuelMusic\Segments_Duel_Violin1Loop		
	300534548	False	STATE_Music_DuelState_J1	\Interactive Music Hierarchy\DS MUSIC\MAIN\Fight\SEGMENTS_DuelMusic\DuelMusic\Segments_Duel_ViolasLoop		

Trigger	ID	Name			Wwise Object Path	Notes
	529325935	TRIGGER_DS_Stan_R			\DS TRIGGERS\MUSIC TRIGGERS\Stingers\DeathStare\TRIGGER_DS_Stan_R	
	529325937	TRIGGER_DS_Stan_L			\DS TRIGGERS\MUSIC TRIGGERS\Stingers\DeathStare\TRIGGER_DS_Stan_L	
	2046853094	TRIGGER_DS_Don_L			\DS TRIGGERS\MUSIC TRIGGERS\Stingers\DeathStare\TRIGGER_DS_Don_L	
	2046853112	TRIGGER_DS_Don_R			\DS TRIGGERS\MUSIC TRIGGERS\Stingers\DeathStare\TRIGGER_DS_Don_R	
	2839118456	TRIGGER_Kill_P1			\DS TRIGGERS\MUSIC TRIGGERS\Stingers\Kill\TRIGGER_Kill_P1	
	2839118458	TRIGGER_Kill_P3			\DS TRIGGERS\MUSIC TRIGGERS\Stingers\Kill\TRIGGER_Kill_P3	
	2839118459	TRIGGER_Kill_P2			\DS TRIGGERS\MUSIC TRIGGERS\Stingers\Kill\TRIGGER_Kill_P2	
	2839118461	TRIGGER_Kill_P4			\DS TRIGGERS\MUSIC TRIGGERS\Stingers\Kill\TRIGGER_Kill_P4	
	2960638048	TRIGGER_DS_Marta_L			\DS TRIGGERS\MUSIC TRIGGERS\Stingers\DeathStare\TRIGGER_DS_Marta_L	
	2960638078	TRIGGER_DS_Marta_R			\DS TRIGGERS\MUSIC TRIGGERS\Stingers\DeathStare\TRIGGER_DS_Marta_R	
	4277786702	TRIGGER_DS_Medusa_L			\DS TRIGGERS\MUSIC TRIGGERS\Stingers\DeathStare\TRIGGER_DS_Medusa_L	
	4277786704	TRIGGER_DS_Medusa_R			\DS TRIGGERS\MUSIC TRIGGERS\Stingers\DeathStare\TRIGGER_DS_Medusa_R	

In Memory Audio	ID	Name	Audio source file		Wwise Object Path	Notes	Data Size
	11282118	STINGERS_Kill_P2	D:\Documents\- ENJMIN\- PROJETS\H - Mini Projet\Repo\DeathStare\DeathStare_WwiseProject\.cache\Linux\SFX\Music\Stingers\Kills\STINGERS_Kill_P3_073B4D0C.wem		\Interactive Music Hierarchy\DS MUSIC\MAIN\Stingers\Kill\R\STINGERS_Kill_P2\STINGERS_Kill_P2		388474
	17309962	Segments_Fight_OutroLoop_Stan	D:\Documents\- ENJMIN\- PROJETS\H - Mini Projet\Repo\DeathStare\DeathStare_WwiseProject\.cache\Linux\SFX\Music\Outro\Segments_Fight_OutroLoop_Stan_073B4D0C.wem		\Interactive Music Hierarchy\DS MUSIC\MAIN\SEGMENTS_MainMusic\Outro\Outro_Stan\Segments_Fight_OutroLoop_Stan\Segments_Fight_OutroLoop_Stan		3696919
	84125637	Segments_Fight_OutroLaunch_Don_115bpm	D:\Documents\- ENJMIN\- PROJETS\H - Mini Projet\Repo\DeathStare\DeathStare_WwiseProject\.cache\Linux\SFX\Music\Outro\Don\Segments_Fight_OutroLaunch_Don_115bpm_073B4D0C.wem		\Interactive Music Hierarchy\DS MUSIC\MAIN\SEGMENTS_MainMusic\Outro\Outro_Don\Segments_Fight_OutroLaunch\Segments_Fight_OutroLaunch_Don_115bpm		524594
	95570234	Segments_Fight_OutroLaunch_Marta_118bpm	D:\Documents\- ENJMIN\- PROJETS\H - Mini Projet\Repo\DeathStare\DeathStare_WwiseProject\.cache\Linux\SFX\Music\Outro\Segments_Fight_OutroLaunch_Marta_118bpm_073B4D0C.wem		\Interactive Music Hierarchy\DS MUSIC\MAIN\SEGMENTS_MainMusic\Outro\Outro_Marta\Segments_Fight_OutroLaunch\Segments_Fight_OutroLaunch_Marta_118bpm		506756
	136646774	Stingers	D:\Documents\- ENJMIN\- PROJETS\H - Mini Projet\Repo\DeathStare\DeathStare_WwiseProject\.cache\Linux\SFX\Music\Stingers\DeathStare\STINGERS_DS_Stan_003_073B4D0C.wem		\Interactive Music Hierarchy\DS MUSIC\MAIN\Stingers\DeathStare\L\STINGERS_DS_Stan_L\Stingers		263480
	171571049	DrumKit	D:\Documents\- ENJMIN\- PROJETS\H - Mini Projet\Repo\DeathStare\DeathStare_WwiseProject\.cache\Linux\SFX\Music\Fight\Segments_Fight_EndLoop_001_Drumkit_001_073B4D0C.wem		\Interactive Music Hierarchy\DS MUSIC\MAIN\SEGMENTS_MainMusic\Fighting\Segments_Fight_End2\DrumKit		7022
	225297425	Segments_Fight_OutroLoop_Marta_118bpm	D:\Documents\- ENJMIN\- PROJETS\H - Mini Projet\Repo\DeathStare\DeathStare_WwiseProject\.cache\Linux\SFX\Music\Outro\Segments_Fight_OutroLoop_Marta_118bpm_073B4D0C.wem		\Interactive Music Hierarchy\DS MUSIC\MAIN\SEGMENTS_MainMusic\Outro\Outro_Marta\Segments_Fight_OutroLoop\Segments_Fight_OutroLoop_Marta_118bpm		3638645
	235583452	Segments_Fight_OutroLoop_Don_115bpm_VA	D:\Documents\- ENJMIN\- PROJETS\H - Mini Projet\Repo\DeathStare\DeathStare_WwiseProject\.cache\Linux\SFX\Music\Outro\Segments_Fight_OutroLoop_Don_115bpm_VA_073B4D0C.wem		\Interactive Music Hierarchy\DS MUSIC\MAIN\SEGMENTS_MainMusic\Outro\Outro_Don\Segments_Fight_OutroLoop\Segments_Fight_OutroLoop_Don_115bpm_VA		3443750
	246215840	Intro	D:\Documents\- ENJMIN\- PROJETS\H - Mini Projet\Repo\DeathStare\DeathStare_WwiseProject\.cache\Linux\SFX\Music\Intro\Segments_Fight_Intro_002_073B4D0C.wem		\Interactive Music Hierarchy\DS MUSIC\MAIN\SEGMENTS_MainMusic\Intro\Intro\Intro		677722
	285054235	Segments_Fight_EndLoop_001_Banjo_000	D:\Documents\- ENJMIN\- PROJETS\H - Mini Projet\Repo\DeathStare\DeathStare_WwiseProject\.cache\Linux\SFX\Music\Fight\Segments_Fight_EndLoop_001_Banjo_000_073B4D0C.wem		\Interactive Music Hierarchy\DS MUSIC\MAIN\SEGMENTS_MainMusic\Fighting\Segments_Fight_End2\Segments_Fight_EndLoop_001_Banjo_000		7273
	300778837	Stingers	D:\Documents\- ENJMIN\- PROJETS\H - Mini Projet\Repo\DeathStare\DeathStare_WwiseProject\.cache\Linux\SFX\Music\Stingers\DeathStare\STINGERS_DS_Medusa_002_073B4D0C.wem		\Interactive Music Hierarchy\DS MUSIC\MAIN\Stingers\DeathStare\L\STINGERS_DS_Medusa_L\Stingers		96761
	323243085	Segments_Fight_EndLoop_001_Accordeon_000	D:\Documents\- ENJMIN\- PROJETS\H - Mini Projet\Repo\DeathStare\DeathStare_WwiseProject\.cache\Linux\SFX\Music\Fight\Segments_Fight_EndLoop_001_Accordeon_000_073B4D0C.wem		\Interactive Music Hierarchy\DS MUSIC\MAIN\SEGMENTS_MainMusic\Fighting\Segments_Fight_End2\Segments_Fight_EndLoop_001_Accordeon_000		529
	327829395	Stingers	D:\Documents\- ENJMIN\- PROJETS\H - Mini Projet\Repo\DeathStare\DeathStare_WwiseProject\.cache\Linux\SFX\Music\Stingers\DeathStare\STINGERS_DS_Marta_002_073B4D0C.wem		\Interactive Music Hierarchy\DS MUSIC\MAIN\Stingers\DeathStare\L\STINGERS_DS_Marta_L\Stingers		289902
	329916654	Stingers	D:\Documents\- ENJMIN\- PROJETS\H - Mini Projet\Repo\DeathStare\DeathStare_WwiseProject\.cache\Linux\SFX\Music\Stingers\DeathStare\STINGERS_DS_Marta_003_073B4D0C.wem		\Interactive Music Hierarchy\DS MUSIC\MAIN\Stingers\DeathStare\L\STINGERS_DS_Marta_L\Stingers		287469
	355239733	Segments_Menu_MainLaunch	D:\Documents\- ENJMIN\- PROJETS\H - Mini Projet\Repo\DeathStare\DeathStare_WwiseProject\.cache\Linux\SFX\Music\MainMenu\Segments_Menu_MainLaunch_073B4D0C.wem		\Interactive Music Hierarchy\DS MUSIC\MAIN\SEGMENTS_MainMusic\MainMenu\Segments_Menu_MainLaunch\Segments_Menu_MainLaunch		12783641
	363596962	STINGERS_Kill_P3	D:\Documents\- ENJMIN\- PROJETS\H - Mini Projet\Repo\DeathStare\DeathStare_WwiseProject\.cache\Linux\SFX\Music\Stingers\Kills\STINGERS_Kill_P2_073B4D0C.wem		\Interactive Music Hierarchy\DS MUSIC\MAIN\Stingers\Kill\L\STINGERS_Kill_P3\STINGERS_Kill_P3		373263
	371607636	Stingers	D:\Documents\- ENJMIN\- PROJETS\H - Mini Projet\Repo\DeathStare\DeathStare_WwiseProject\.cache\Linux\SFX\Music\Stingers\DeathStare\STINGERS_DS_Stan_002_073B4D0C.wem		\Interactive Music Hierarchy\DS MUSIC\MAIN\Stingers\DeathStare\L\STINGERS_DS_Stan_L\Stingers		222319
	425417005	AccGtr	D:\Documents\- ENJMIN\- PROJETS\H - Mini Projet\Repo\DeathStare\DeathStare_WwiseProject\.cache\Linux\SFX\Music\Fight\Segments_Fight_EndLoop_001_AccGtr_002_073B4D0C.wem		\Interactive Music Hierarchy\DS MUSIC\MAIN\SEGMENTS_MainMusic\Fighting\Segments_Fight_End2\AccGtr		7290
	427637616	Segments_Fight_OutroLoop_Medusa	D:\Documents\- ENJMIN\- PROJETS\H - Mini Projet\Repo\DeathStare\DeathStare_WwiseProject\.cache\Linux\SFX\Music\Outro\Segments_Fight_OutroLoop_Medusa_115bpm_073B4D0C.wem		\Interactive Music Hierarchy\DS MUSIC\MAIN\SEGMENTS_MainMusic\Outro\Outro_Medusa\Segments_Fight_OutroLoop_Medusa\Segments_Fight_OutroLoop_Medusa		3510655
	467894095	Stingers	D:\Documents\- ENJMIN\- PROJETS\H - Mini Projet\Repo\DeathStare\DeathStare_WwiseProject\.cache\Linux\SFX\Music\Stingers\DeathStare\STINGERS_DS_Stan_001_073B4D0C.wem		\Interactive Music Hierarchy\DS MUSIC\MAIN\Stingers\DeathStare\L\STINGERS_DS_Stan_L\Stingers		206790
	475265180	Stingers	D:\Documents\- ENJMIN\- PROJETS\H - Mini Projet\Repo\DeathStare\DeathStare_WwiseProject\.cache\Linux\SFX\Music\Stingers\DeathStare\STINGERS_DS_Medusa_001_073B4D0C.wem		\Interactive Music Hierarchy\DS MUSIC\MAIN\Stingers\DeathStare\L\STINGERS_DS_Medusa_L\Stingers		153921
	492318781	Stingers	D:\Documents\- ENJMIN\- PROJETS\H - Mini Projet\Repo\DeathStare\DeathStare_WwiseProject\.cache\Linux\SFX\Music\Stingers\DeathStare\STINGERS_DS_Don_001_073B4D0C.wem		\Interactive Music Hierarchy\DS MUSIC\MAIN\Stingers\DeathStare\L\STINGERS_DS_Don_L\Stingers		215943
	500589853	Stingers	D:\Documents\- ENJMIN\- PROJETS\H - Mini Projet\Repo\DeathStare\DeathStare_WwiseProject\.cache\Linux\SFX\Music\Stingers\DeathStare\STINGERS_DS_Don_002_073B4D0C.wem		\Interactive Music Hierarchy\DS MUSIC\MAIN\Stingers\DeathStare\L\STINGERS_DS_Don_L\Stingers		187782
	500983585	Segments_Fight_OutroLaunch	D:\Documents\- ENJMIN\- PROJETS\H - Mini Projet\Repo\DeathStare\DeathStare_WwiseProject\.cache\Linux\SFX\Music\Outro\Segments_Fight_OutroLaunch_073B4D0C.wem		\Interactive Music Hierarchy\DS MUSIC\MAIN\SEGMENTS_MainMusic\Outro\Outro_Stan\Segments_Fight_OutroLaunch\Segments_Fight_OutroLaunch		397314
	519564728	CombatMusic_Test1_Launch	D:\Documents\- ENJMIN\- PROJETS\H - Mini Projet\Repo\DeathStare\DeathStare_WwiseProject\.cache\Linux\SFX\Music\CombatMusic_Test1_Launch_073B4D0C.wem		\Interactive Music Hierarchy\DS MUSIC\MAIN\SEGMENTS_MainMusic\Fighting\CombatMusic_Test1_Launch\CombatMusic_Test1_Launch		7187
	530982020	STINGERS_Kill_P1	D:\Documents\- ENJMIN\- PROJETS\H - Mini Projet\Repo\DeathStare\DeathStare_WwiseProject\.cache\Linux\SFX\Music\Stingers\Kills\STINGERS_Kill_P4_073B4D0C.wem		\Interactive Music Hierarchy\DS MUSIC\MAIN\Stingers\Kill\L\STINGERS_Kill_P1\STINGERS_Kill_P1		418347
	540839411	Intro	D:\Documents\- ENJMIN\- PROJETS\H - Mini Projet\Repo\DeathStare\DeathStare_WwiseProject\.cache\Linux\SFX\Music\Intro\Segments_Fight_Intro_000_073B4D0C.wem		\Interactive Music Hierarchy\DS MUSIC\MAIN\SEGMENTS_MainMusic\Intro\Intro\Intro		672617
	545547126	ElecGtr	D:\Documents\- ENJMIN\- PROJETS\H - Mini Projet\Repo\DeathStare\DeathStare_WwiseProject\.cache\Linux\SFX\Music\Fight\Segments_Fight_EndLoop_001_ElecGtr_001_073B4D0C.wem		\Interactive Music Hierarchy\DS MUSIC\MAIN\SEGMENTS_MainMusic\Fighting\Segments_Fight_End2\ElecGtr		8205
	610983406	Segments_Fight_OutroLaunch_Medusa_115bpm	D:\Documents\- ENJMIN\- PROJETS\H - Mini Projet\Repo\DeathStare\DeathStare_WwiseProject\.cache\Linux\SFX\Music\Outro\Segments_Fight_OutroLaunch_Medusa_115bpm_073B4D0C.wem		\Interactive Music Hierarchy\DS MUSIC\MAIN\SEGMENTS_MainMusic\Outro\Outro_Medusa\Segments_Fight_OutroLaunch\Segments_Fight_OutroLaunch_Medusa_115bpm		447043
	624186810	Bass	D:\Documents\- ENJMIN\- PROJETS\H - Mini Projet\Repo\DeathStare\DeathStare_WwiseProject\.cache\Linux\SFX\Music\Fight\Segments_Fight_EndLoop_001_Bass_001_073B4D0C.wem		\Interactive Music Hierarchy\DS MUSIC\MAIN\SEGMENTS_MainMusic\Fighting\Segments_Fight_End2\Bass		5198
	635846287	AccGtr	D:\Documents\- ENJMIN\- PROJETS\H - Mini Projet\Repo\DeathStare\DeathStare_WwiseProject\.cache\Linux\SFX\Music\Fight\Segments_Fight_EndLoop_001_AccGtr_000_073B4D0C.wem		\Interactive Music Hierarchy\DS MUSIC\MAIN\SEGMENTS_MainMusic\Fighting\Segments_Fight_End2\AccGtr		7653
	640019073	Stingers	D:\Documents\- ENJMIN\- PROJETS\H - Mini Projet\Repo\DeathStare\DeathStare_WwiseProject\.cache\Linux\SFX\Music\Stingers\DeathStare\STINGERS_DS_Stan_000_073B4D0C.wem		\Interactive Music Hierarchy\DS MUSIC\MAIN\Stingers\DeathStare\L\STINGERS_DS_Stan_L\Stingers		250487
	648061523	DrumKit	D:\Documents\- ENJMIN\- PROJETS\H - Mini Projet\Repo\DeathStare\DeathStare_WwiseProject\.cache\Linux\SFX\Music\Fight\Segments_Fight_EndLoop_001_Drumkit_002_073B4D0C.wem		\Interactive Music Hierarchy\DS MUSIC\MAIN\SEGMENTS_MainMusic\Fighting\Segments_Fight_End2\DrumKit		6909
	666321469	Intro	D:\Documents\- ENJMIN\- PROJETS\H - Mini Projet\Repo\DeathStare\DeathStare_WwiseProject\.cache\Linux\SFX\Music\Intro\Segments_Fight_Intro_001_073B4D0C.wem		\Interactive Music Hierarchy\DS MUSIC\MAIN\SEGMENTS_MainMusic\Intro\Intro\Intro		686894
	668521458	STINGERS_Kill_P4	D:\Documents\- ENJMIN\- PROJETS\H - Mini Projet\Repo\DeathStare\DeathStare_WwiseProject\.cache\Linux\SFX\Music\Stingers\Kills\STINGERS_Kill_P1_073B4D0C.wem		\Interactive Music Hierarchy\DS MUSIC\MAIN\Stingers\Kill\R\STINGERS_Kill_P4\STINGERS_Kill_P4		387311
	686294202	Segments_Menu_MainLoop	D:\Documents\- ENJMIN\- PROJETS\H - Mini Projet\Repo\DeathStare\DeathStare_WwiseProject\.cache\Linux\SFX\Music\MainMenu\Segments_Menu_MainLoop_073B4D0C.wem		\Interactive Music Hierarchy\DS MUSIC\MAIN\SEGMENTS_MainMusic\MainMenu\Segments_Menu_MainLoop\Segments_Menu_MainLoop		12915156
	698480867	Stingers	D:\Documents\- ENJMIN\- PROJETS\H - Mini Projet\Repo\DeathStare\DeathStare_WwiseProject\.cache\Linux\SFX\Music\Stingers\DeathStare\STINGERS_DS_Stan_004_073B4D0C.wem		\Interactive Music Hierarchy\DS MUSIC\MAIN\Stingers\DeathStare\L\STINGERS_DS_Stan_L\Stingers		269173
	714366655	ElecGtr	D:\Documents\- ENJMIN\- PROJETS\H - Mini Projet\Repo\DeathStare\DeathStare_WwiseProject\.cache\Linux\SFX\Music\Fight\Segments_Fight_EndLoop_001_ElecGtr_002_073B4D0C.wem		\Interactive Music Hierarchy\DS MUSIC\MAIN\SEGMENTS_MainMusic\Fighting\Segments_Fight_End2\ElecGtr		7136
	738237128	AccGtr	D:\Documents\- ENJMIN\- PROJETS\H - Mini Projet\Repo\DeathStare\DeathStare_WwiseProject\.cache\Linux\SFX\Music\Fight\Segments_Fight_EndLoop_001_AccGtr_001_073B4D0C.wem		\Interactive Music Hierarchy\DS MUSIC\MAIN\SEGMENTS_MainMusic\Fighting\Segments_Fight_End2\AccGtr		7298
	778420763	Stingers	D:\Documents\- ENJMIN\- PROJETS\H - Mini Projet\Repo\DeathStare\DeathStare_WwiseProject\.cache\Linux\SFX\Music\Stingers\DeathStare\STINGERS_DS_Medusa_003_073B4D0C.wem		\Interactive Music Hierarchy\DS MUSIC\MAIN\Stingers\DeathStare\L\STINGERS_DS_Medusa_L\Stingers		122278
	794660303	Stingers	D:\Documents\- ENJMIN\- PROJETS\H - Mini Projet\Repo\DeathStare\DeathStare_WwiseProject\.cache\Linux\SFX\Music\Stingers\DeathStare\STINGERS_DS_Marta_001_073B4D0C.wem		\Interactive Music Hierarchy\DS MUSIC\MAIN\Stingers\DeathStare\L\STINGERS_DS_Marta_L\Stingers		293334
	795413760	Segments_DS_Intensity_001	D:\Documents\- ENJMIN\- PROJETS\H - Mini Projet\Repo\DeathStare\DeathStare_WwiseProject\.cache\Linux\SFX\Music\DSIntensity\Segments_DS_Intensity_001_073B4D0C.wem		\Interactive Music Hierarchy\DS MUSIC\MAIN\Fight\SEGMENTS_DS_Intensity\Level2\Segments_DS_Intensity_001\Segments_DS_Intensity_001		498170
	795878512	Segments_DS_Intensity_000	D:\Documents\- ENJMIN\- PROJETS\H - Mini Projet\Repo\DeathStare\DeathStare_WwiseProject\.cache\Linux\SFX\Music\DSIntensity\Segments_DS_Intensity_000_073B4D0C.wem		\Interactive Music Hierarchy\DS MUSIC\MAIN\Fight\SEGMENTS_DS_Intensity\Silence\Segments_DS_Intensity_000\Segments_DS_Intensity_000		496418
	841242472	Stingers	D:\Documents\- ENJMIN\- PROJETS\H - Mini Projet\Repo\DeathStare\DeathStare_WwiseProject\.cache\Linux\SFX\Music\Stingers\DeathStare\STINGERS_DS_Don_000_073B4D0C.wem		\Interactive Music Hierarchy\DS MUSIC\MAIN\Stingers\DeathStare\L\STINGERS_DS_Don_L\Stingers		253834
	852245913	DrumKit	D:\Documents\- ENJMIN\- PROJETS\H - Mini Projet\Repo\DeathStare\DeathStare_WwiseProject\.cache\Linux\SFX\Music\Fight\Segments_Fight_EndLoop_001_Drumkit_000_073B4D0C.wem		\Interactive Music Hierarchy\DS MUSIC\MAIN\SEGMENTS_MainMusic\Fighting\Segments_Fight_End2\DrumKit		7284
	856576358	Stingers	D:\Documents\- ENJMIN\- PROJETS\H - Mini Projet\Repo\DeathStare\DeathStare_WwiseProject\.cache\Linux\SFX\Music\Stingers\DeathStare\STINGERS_DS_Marta_000_073B4D0C.wem		\Interactive Music Hierarchy\DS MUSIC\MAIN\Stingers\DeathStare\L\STINGERS_DS_Marta_L\Stingers		294416
	867488535	Stingers	D:\Documents\- ENJMIN\- PROJETS\H - Mini Projet\Repo\DeathStare\DeathStare_WwiseProject\.cache\Linux\SFX\Music\Stingers\DeathStare\STINGERS_DS_Don_003_073B4D0C.wem		\Interactive Music Hierarchy\DS MUSIC\MAIN\Stingers\DeathStare\L\STINGERS_DS_Don_L\Stingers		173593
	877440624	DrumKit	D:\Documents\- ENJMIN\- PROJETS\H - Mini Projet\Repo\DeathStare\DeathStare_WwiseProject\.cache\Linux\SFX\Music\Fight\Segments_Fight_EndLoop_001_Drumkit_003_073B4D0C.wem		\Interactive Music Hierarchy\DS MUSIC\MAIN\SEGMENTS_MainMusic\Silence\TempoTrack\DrumKit		790800
	892660261	CombatMusic_Test1_Loop	D:\Documents\- ENJMIN\- PROJETS\H - Mini Projet\Repo\DeathStare\DeathStare_WwiseProject\.cache\Linux\SFX\Music\CombatMusic_Test1_Loop_073B4D0C.wem		\Interactive Music Hierarchy\DS MUSIC\MAIN\SEGMENTS_MainMusic\Fighting\CombatMusic_Test1_Loop\CombatMusic_Test1_Loop		9963
	903170528	ElecGtr	D:\Documents\- ENJMIN\- PROJETS\H - Mini Projet\Repo\DeathStare\DeathStare_WwiseProject\.cache\Linux\SFX\Music\Fight\Segments_Fight_EndLoop_001_ElecGtr_000_073B4D0C.wem		\Interactive Music Hierarchy\DS MUSIC\MAIN\SEGMENTS_MainMusic\Fighting\Segments_Fight_End2\ElecGtr		6998
	932992452	Bass	D:\Documents\- ENJMIN\- PROJETS\H - Mini Projet\Repo\DeathStare\DeathStare_WwiseProject\.cache\Linux\SFX\Music\Fight\Segments_Fight_EndLoop_001_Bass_000_073B4D0C.wem		\Interactive Music Hierarchy\DS MUSIC\MAIN\SEGMENTS_MainMusic\Fighting\Segments_Fight_End2\Bass		5463
	990135640	Intro	D:\Documents\- ENJMIN\- PROJETS\H - Mini Projet\Repo\DeathStare\DeathStare_WwiseProject\.cache\Linux\SFX\Music\Intro\Segments_Fight_Intro_003_073B4D0C.wem		\Interactive Music Hierarchy\DS MUSIC\MAIN\SEGMENTS_MainMusic\Intro\Intro\Intro		656345
	1032434855	Stingers	D:\Documents\- ENJMIN\- PROJETS\H - Mini Projet\Repo\DeathStare\DeathStare_WwiseProject\.cache\Linux\SFX\Music\Stingers\DeathStare\STINGERS_DS_Medusa_000_073B4D0C.wem		\Interactive Music Hierarchy\DS MUSIC\MAIN\Stingers\DeathStare\L\STINGERS_DS_Medusa_L\Stingers		164068
	1065694909	Segments_Fight_EndLoop_001_Launch	D:\Documents\- ENJMIN\- PROJETS\H - Mini Projet\Repo\DeathStare\DeathStare_WwiseProject\.cache\Linux\SFX\Music\Fight\Segments_Fight_EndLoop_001_Launch_073B4D0C.wem		\Interactive Music Hierarchy\DS MUSIC\MAIN\SEGMENTS_MainMusic\Fighting\Segments_Fight_End2_Launch\Segments_Fight_EndLoop_001_Launch		7189

Streamed Audio	ID	Name	Audio source file	Generated audio file	Wwise Object Path	Notes
	19652934	Segments_Duel_Violin1Loop	D:\Documents\- ENJMIN\- PROJETS\H - Mini Projet\Repo\DeathStare\DeathStare_WwiseProject\.cache\Linux\SFX\Music\Duel\Segments_Duel_Violin1Loop_073B4D0C.wem	19652934.wem	\Interactive Music Hierarchy\DS MUSIC\MAIN\Fight\SEGMENTS_DuelMusic\DuelMusic\Segments_Duel_Violin1Loop	
	46506750	Segments_Duel_Violin2Loop	D:\Documents\- ENJMIN\- PROJETS\H - Mini Projet\Repo\DeathStare\DeathStare_WwiseProject\.cache\Linux\SFX\Music\Duel\Segments_Duel_Violin2Loop_073B4D0C.wem	46506750.wem	\Interactive Music Hierarchy\DS MUSIC\MAIN\Fight\SEGMENTS_DuelMusic\DuelMusic\Segments_Duel_Violin2Loop	
	171571049	DrumKit	D:\Documents\- ENJMIN\- PROJETS\H - Mini Projet\Repo\DeathStare\DeathStare_WwiseProject\.cache\Linux\SFX\Music\Fight\Segments_Fight_EndLoop_001_Drumkit_001_073B4D0C.wem	171571049.wem	\Interactive Music Hierarchy\DS MUSIC\MAIN\SEGMENTS_MainMusic\Fighting\Segments_Fight_End2\DrumKit	
	285054235	Segments_Fight_EndLoop_001_Banjo_000	D:\Documents\- ENJMIN\- PROJETS\H - Mini Projet\Repo\DeathStare\DeathStare_WwiseProject\.cache\Linux\SFX\Music\Fight\Segments_Fight_EndLoop_001_Banjo_000_073B4D0C.wem	285054235.wem	\Interactive Music Hierarchy\DS MUSIC\MAIN\SEGMENTS_MainMusic\Fighting\Segments_Fight_End2\Segments_Fight_EndLoop_001_Banjo_000	
	323243085	Segments_Fight_EndLoop_001_Accordeon_000	D:\Documents\- ENJMIN\- PROJETS\H - Mini Projet\Repo\DeathStare\DeathStare_WwiseProject\.cache\Linux\SFX\Music\Fight\Segments_Fight_EndLoop_001_Accordeon_000_073B4D0C.wem	323243085.wem	\Interactive Music Hierarchy\DS MUSIC\MAIN\SEGMENTS_MainMusic\Fighting\Segments_Fight_End2\Segments_Fight_EndLoop_001_Accordeon_000	
	353834140	Segments_Duel_CelliLoop	D:\Documents\- ENJMIN\- PROJETS\H - Mini Projet\Repo\DeathStare\DeathStare_WwiseProject\.cache\Linux\SFX\Music\Duel\Segments_Duel_CelliLoop_073B4D0C.wem	353834140.wem	\Interactive Music Hierarchy\DS MUSIC\MAIN\Fight\SEGMENTS_DuelMusic\DuelMusic\Segments_Duel_CelliLoop	
	425417005	AccGtr	D:\Documents\- ENJMIN\- PROJETS\H - Mini Projet\Repo\DeathStare\DeathStare_WwiseProject\.cache\Linux\SFX\Music\Fight\Segments_Fight_EndLoop_001_AccGtr_002_073B4D0C.wem	425417005.wem	\Interactive Music Hierarchy\DS MUSIC\MAIN\SEGMENTS_MainMusic\Fighting\Segments_Fight_End2\AccGtr	
	519564728	CombatMusic_Test1_Launch	D:\Documents\- ENJMIN\- PROJETS\H - Mini Projet\Repo\DeathStare\DeathStare_WwiseProject\.cache\Linux\SFX\Music\CombatMusic_Test1_Launch_073B4D0C.wem	519564728.wem	\Interactive Music Hierarchy\DS MUSIC\MAIN\SEGMENTS_MainMusic\Fighting\CombatMusic_Test1_Launch\CombatMusic_Test1_Launch	
	545547126	ElecGtr	D:\Documents\- ENJMIN\- PROJETS\H - Mini Projet\Repo\DeathStare\DeathStare_WwiseProject\.cache\Linux\SFX\Music\Fight\Segments_Fight_EndLoop_001_ElecGtr_001_073B4D0C.wem	545547126.wem	\Interactive Music Hierarchy\DS MUSIC\MAIN\SEGMENTS_MainMusic\Fighting\Segments_Fight_End2\ElecGtr	
	624186810	Bass	D:\Documents\- ENJMIN\- PROJETS\H - Mini Projet\Repo\DeathStare\DeathStare_WwiseProject\.cache\Linux\SFX\Music\Fight\Segments_Fight_EndLoop_001_Bass_001_073B4D0C.wem	624186810.wem	\Interactive Music Hierarchy\DS MUSIC\MAIN\SEGMENTS_MainMusic\Fighting\Segments_Fight_End2\Bass	
	635846287	AccGtr	D:\Documents\- ENJMIN\- PROJETS\H - Mini Projet\Repo\DeathStare\DeathStare_WwiseProject\.cache\Linux\SFX\Music\Fight\Segments_Fight_EndLoop_001_AccGtr_000_073B4D0C.wem	635846287.wem	\Interactive Music Hierarchy\DS MUSIC\MAIN\SEGMENTS_MainMusic\Fighting\Segments_Fight_End2\AccGtr	
	648061523	DrumKit	D:\Documents\- ENJMIN\- PROJETS\H - Mini Projet\Repo\DeathStare\DeathStare_WwiseProject\.cache\Linux\SFX\Music\Fight\Segments_Fight_EndLoop_001_Drumkit_002_073B4D0C.wem	648061523.wem	\Interactive Music Hierarchy\DS MUSIC\MAIN\SEGMENTS_MainMusic\Fighting\Segments_Fight_End2\DrumKit	
	714366655	ElecGtr	D:\Documents\- ENJMIN\- PROJETS\H - Mini Projet\Repo\DeathStare\DeathStare_WwiseProject\.cache\Linux\SFX\Music\Fight\Segments_Fight_EndLoop_001_ElecGtr_002_073B4D0C.wem	714366655.wem	\Interactive Music Hierarchy\DS MUSIC\MAIN\SEGMENTS_MainMusic\Fighting\Segments_Fight_End2\ElecGtr	
	738237128	AccGtr	D:\Documents\- ENJMIN\- PROJETS\H - Mini Projet\Repo\DeathStare\DeathStare_WwiseProject\.cache\Linux\SFX\Music\Fight\Segments_Fight_EndLoop_001_AccGtr_001_073B4D0C.wem	738237128.wem	\Interactive Music Hierarchy\DS MUSIC\MAIN\SEGMENTS_MainMusic\Fighting\Segments_Fight_End2\AccGtr	
	852245913	DrumKit	D:\Documents\- ENJMIN\- PROJETS\H - Mini Projet\Repo\DeathStare\DeathStare_WwiseProject\.cache\Linux\SFX\Music\Fight\Segments_Fight_EndLoop_001_Drumkit_000_073B4D0C.wem	852245913.wem	\Interactive Music Hierarchy\DS MUSIC\MAIN\SEGMENTS_MainMusic\Fighting\Segments_Fight_End2\DrumKit	
	877440624	DrumKit	D:\Documents\- ENJMIN\- PROJETS\H - Mini Projet\Repo\DeathStare\DeathStare_WwiseProject\.cache\Linux\SFX\Music\Fight\Segments_Fight_EndLoop_001_Drumkit_003_073B4D0C.wem	877440624.wem	\Interactive Music Hierarchy\DS MUSIC\MAIN\SEGMENTS_MainMusic\Fighting\Segments_Fight_End2\DrumKit	
	892660261	CombatMusic_Test1_Loop	D:\Documents\- ENJMIN\- PROJETS\H - Mini Projet\Repo\DeathStare\DeathStare_WwiseProject\.cache\Linux\SFX\Music\CombatMusic_Test1_Loop_073B4D0C.wem	892660261.wem	\Interactive Music Hierarchy\DS MUSIC\MAIN\SEGMENTS_MainMusic\Fighting\CombatMusic_Test1_Loop\CombatMusic_Test1_Loop	
	903170528	ElecGtr	D:\Documents\- ENJMIN\- PROJETS\H - Mini Projet\Repo\DeathStare\DeathStare_WwiseProject\.cache\Linux\SFX\Music\Fight\Segments_Fight_EndLoop_001_ElecGtr_000_073B4D0C.wem	903170528.wem	\Interactive Music Hierarchy\DS MUSIC\MAIN\SEGMENTS_MainMusic\Fighting\Segments_Fight_End2\ElecGtr	
	932992452	Bass	D:\Documents\- ENJMIN\- PROJETS\H - Mini Projet\Repo\DeathStare\DeathStare_WwiseProject\.cache\Linux\SFX\Music\Fight\Segments_Fight_EndLoop_001_Bass_000_073B4D0C.wem	932992452.wem	\Interactive Music Hierarchy\DS MUSIC\MAIN\SEGMENTS_MainMusic\Fighting\Segments_Fight_End2\Bass	
	1040627007	Segments_Duel_ViolasLoop	D:\Documents\- ENJMIN\- PROJETS\H - Mini Projet\Repo\DeathStare\DeathStare_WwiseProject\.cache\Linux\SFX\Music\Duel\Segments_Duel_ViolasLoop_073B4D0C.wem	1040627007.wem	\Interactive Music Hierarchy\DS MUSIC\MAIN\Fight\SEGMENTS_DuelMusic\DuelMusic\Segments_Duel_ViolasLoop	
	1065694909	Segments_Fight_EndLoop_001_Launch	D:\Documents\- ENJMIN\- PROJETS\H - Mini Projet\Repo\DeathStare\DeathStare_WwiseProject\.cache\Linux\SFX\Music\Fight\Segments_Fight_EndLoop_001_Launch_073B4D0C.wem	1065694909.wem	\Interactive Music Hierarchy\DS MUSIC\MAIN\SEGMENTS_MainMusic\Fighting\Segments_Fight_End2_Launch\Segments_Fight_EndLoop_001_Launch	

