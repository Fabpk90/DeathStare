Event	ID	Name			Wwise Object Path	Notes
	59600196	Stop_All_Soft			\DS EVENTS\GENERAL EVENTS\Stop_All_Soft	
	3679762312	Resume_All			\DS EVENTS\GENERAL EVENTS\Resume_All	
	3864097025	Pause_All			\DS EVENTS\GENERAL EVENTS\Pause_All	

Game Parameter	ID	Name			Wwise Object Path	Notes
	475975533	RTPC_StereoWidth			\DS RTPC\MIX RTPC\Settings\RTPC_StereoWidth	
	579984592	RTPC_Effects_Volume			\DS RTPC\MIX RTPC\Settings\RTPC_Effects_Volume	
	888992730	RTPC_Prior_Meter_03_Fast			\DS RTPC\MIX RTPC\Meters\RTPC_Prior_Meter_03_Fast	
	1080068730	RTPC_UI_Volume			\DS RTPC\MIX RTPC\Settings\RTPC_UI_Volume	
	1596647065	RTPC_Music_Volume			\DS RTPC\MIX RTPC\Settings\RTPC_Music_Volume	
	1676809991	RTPC_Ambiances_Volume			\DS RTPC\MIX RTPC\Settings\RTPC_Ambiances_Volume	
	1998977308	RTPC_Voice_Volume			\DS RTPC\MIX RTPC\Settings\RTPC_Voice_Volume	
	2564988978	RTPC_Master_Volume			\DS RTPC\MIX RTPC\Settings\RTPC_Master_Volume	
	2568571271	RTPC_Prior_Meter_04_Fast			\DS RTPC\MIX RTPC\Meters\RTPC_Prior_Meter_04_Fast	
	2776054680	RTPC_Foleys_Volume			\DS RTPC\MIX RTPC\Settings\RTPC_Foleys_Volume	
	3202337757	RTPC_DS_Volume			\DS RTPC\MIX RTPC\Settings\RTPC_DS_Volume	
	3334746462	RTPC_Prior_Meter_04_Slow			\DS RTPC\MIX RTPC\Meters\RTPC_Prior_Meter_04_Slow	
	3364075793	RTPC_Prior_Meter_05_Slow			\DS RTPC\MIX RTPC\Meters\RTPC_Prior_Meter_05_Slow	
	3366444199	RTPC_Prior_Meter_03_Slow			\DS RTPC\MIX RTPC\Meters\RTPC_Prior_Meter_03_Slow	
	3928341384	RTPC_Prior_Meter_05_Fast			\DS RTPC\MIX RTPC\Meters\RTPC_Prior_Meter_05_Fast	

Audio Bus	ID	Name			Wwise Object Path	Notes
	158871344	SFX_Effect_Prior03_Slow			\DS MIX\Master\SFX\SFX_Effects\SFX_Effect_Prior03_Slow	
	220327233	MUSIC_Prior_02_Fast			\DS MIX\Master\Music\MUSIC_Prior_02\MUSIC_Prior_02_Fast	
	393239870	SFX			\DS MIX\Master\SFX	
	599938019	MUSIC_MainMenu			\DS MIX\Master\Music\MUSIC_Prior_02\MUSIC_Prior_02_Slow\MUSIC_MainMenu	
	660584193	SFX_Foleys_Prior01			\DS MIX\Master\SFX\SFX_Foleys\SFX_Foleys_Prior01	
	660584194	SFX_Foleys_Prior02			\DS MIX\Master\SFX\SFX_Foleys\SFX_Foleys_Prior02	
	660584195	SFX_Foleys_Prior03			\DS MIX\Master\SFX\SFX_Foleys\SFX_Foleys_Prior03	
	707518423	PriorAux			\DS MIX\Master\PriorAux	
	851001452	MUSIC_Prior_02_Slow			\DS MIX\Master\Music\MUSIC_Prior_02\MUSIC_Prior_02_Slow	
	933472508	SFX_Effect_Prior04_Fast			\DS MIX\Master\SFX\SFX_Effects\SFX_Effect_Prior04_Fast	
	983310469	Aux			\DS MIX\Master\Aux	
	984952705	MUSIC_Intro			\DS MIX\Master\Music\MUSIC_Prior_02\MUSIC_Prior_02_Slow\MUSIC_Intro	
	1258652135	StingersKillsR			\DS MIX\Master\Music\MUSIC_Prior_05_Fast\StingersKills\StingersKillsR	
	1258652153	StingersKillsL			\DS MIX\Master\Music\MUSIC_Prior_05_Fast\StingersKills\StingersKillsL	
	1457905285	VO_Prior04			\DS MIX\Master\VO\VO_Prior04	
	1534528548	VO			\DS MIX\Master\VO	
	1668764311	MUSIC_Prior_03_Slow			\DS MIX\Master\Music\MUSIC_Prior_03_Slow	
	1767886531	StingersDS			\DS MIX\Master\Music\MUSIC_Prior_03_Fast\StingersDS	
	1934198145	MUSIC_Prior_05_Slow			\DS MIX\Master\Music\MUSIC_Prior_05_Slow	
	1966498141	SFX_Effect_Prior03_Fast			\DS MIX\Master\SFX\SFX_Effects\SFX_Effect_Prior03_Fast	
	2324388809	SFX_Effects			\DS MIX\Master\SFX\SFX_Effects	
	2359468234	MUSIC_Prior_03_Fast			\DS MIX\Master\Music\MUSIC_Prior_03_Fast	
	2383096572	SFX_Ambiances			\DS MIX\Master\SFX\SFX_Ambiances	
	2765257867	StingersDSR			\DS MIX\Master\Music\MUSIC_Prior_03_Fast\StingersDS\StingersDSR	
	2765257877	StingersDSL			\DS MIX\Master\Music\MUSIC_Prior_03_Fast\StingersDS\StingersDSL	
	2864885076	SFX_Amb_Prior02			\DS MIX\Master\SFX\SFX_Ambiances\SFX_Amb_Prior02	
	2864885079	SFX_Amb_Prior01			\DS MIX\Master\SFX\SFX_Ambiances\SFX_Amb_Prior01	
	2995587383	StingersKills			\DS MIX\Master\Music\MUSIC_Prior_05_Fast\StingersKills	
	3183352817	SFX_Foleys			\DS MIX\Master\SFX\SFX_Foleys	
	3191610174	MUSIC_Outro			\DS MIX\Master\Music\MUSIC_Prior_03_Slow\MUSIC_Outro	
	3600354836	MUSIC_Prior_02			\DS MIX\Master\Music\MUSIC_Prior_02	
	3614830008	MUSIC_Prior_05_Fast			\DS MIX\Master\Music\MUSIC_Prior_05_Fast	
	3862737079	SFX_UI			\DS MIX\Master\SFX\SFX_UI	
	3942676432	SFX_UI_Prior_05			\DS MIX\Master\SFX\SFX_UI\SFX_UI_Prior_05	
	3942676433	SFX_UI_Prior_04			\DS MIX\Master\SFX\SFX_UI\SFX_UI_Prior_04	
	3991942870	Music			\DS MIX\Master\Music	
	4056684167	Master			\DS MIX\Master	

Auxiliary Bus	ID	Name			Wwise Object Path	Notes
	226212192	RVB_RoomMedium			\DS MIX\Master\Aux\RVB_RoomMedium	
	429702506	RVB_RoomSmall			\DS MIX\Master\Aux\RVB_RoomSmall	
	444410022	Prior_04_Slow			\DS MIX\Master\PriorAux\Prior_04_Slow	
	491446543	Prior_04_Fast			\DS MIX\Master\PriorAux\Prior_04_Fast	
	808284948	Prior_01_Fast			\DS MIX\Master\PriorAux\Prior_01_Fast	
	989439445	Prior_01_Slow			\DS MIX\Master\PriorAux\Prior_01_Slow	
	1369990425	Prior_02_Fast			\DS MIX\Master\PriorAux\Prior_02_Fast	
	1692131274	RVB_Tunnel			\DS MIX\Master\Aux\RVB_Tunnel	
	1834964976	Prior_05_Fast			\DS MIX\Master\PriorAux\Prior_05_Fast	
	2709619631	Prior_03_Slow			\DS MIX\Master\PriorAux\Prior_03_Slow	
	3139664290	Prior_03_Fast			\DS MIX\Master\PriorAux\Prior_03_Fast	
	3148728066	RVB_Corridor			\DS MIX\Master\Aux\RVB_Corridor	
	3797936356	Prior_02_Slow			\DS MIX\Master\PriorAux\Prior_02_Slow	
	3801984506	RVB_Exterior			\DS MIX\Master\Aux\RVB_Exterior	
	4001587643	RVB_Cave			\DS MIX\Master\Aux\RVB_Cave	
	4188538041	Prior_05_Slow			\DS MIX\Master\PriorAux\Prior_05_Slow	

Effect plug-ins	ID	Name	Type				Notes
	4496003	Dirty_Washing_Machine	Wwise RoomVerb			
	12935118	Wwise_Convolution_Reverb_(Custom)	Wwise Convolution Reverb			
	48620415	Have_U_Ever_Been_Outside	Wwise RoomVerb			
	64597246	Wwise_Convolution_Reverb_(Custom)	Wwise Convolution Reverb			
	99720212	Wwise_Meter_(Custom)	Wwise Meter			
	192556655	Wwise_Meter_(Custom)	Wwise Meter			
	291040623	Wwise_Meter_(Custom)	Wwise Meter			
	400317747	Hall_Large_TooBright	Wwise RoomVerb			
	410057404	Wwise_Convolution_Reverb_(Custom)	Wwise Convolution Reverb			
	415841907	Space_Garage	Wwise RoomVerb			
	435244789	Wwise_Meter_(Custom)	Wwise Meter			
	484890516	Church_Small_Wood	Wwise RoomVerb			
	506891147	Plate_Small	Wwise RoomVerb			
	523088863	Wwise_Meter_(Custom)	Wwise Meter			
	547235303	Wwise_Meter_(Custom)	Wwise Meter			
	575369714	Wwise_Convolution_Reverb_(Custom)	Wwise Convolution Reverb			
	602546716	Room_Large	Wwise RoomVerb			
	636204970	Wwise_Meter_(Custom)	Wwise Meter			
	644221823	Wwise_Convolution_Reverb_(Custom)	Wwise Convolution Reverb			
	701863527	Inside_My_Head	Wwise RoomVerb			
	748015649	Wwise_Recorder_(Custom)	Wwise Recorder			
	997646041	Wwise_Meter_(Custom)	Wwise Meter			
	1035412554	Wwise_Meter_(Custom)	Wwise Meter			
	1055390835	Wwise_Convolution_Reverb_(Custom)	Wwise Convolution Reverb			
	1059614668	Wwise_Meter_(Custom)	Wwise Meter			
	1144018367	Plate_Large	Wwise RoomVerb			
	1159390747	Room_Medium_Tiled	Wwise RoomVerb			
	1203617187	Machine_Gun_Hangar	Wwise RoomVerb			
	1520064353	Hall_Medium_Bright	Wwise RoomVerb			
	1706684651	Plate_Medium	Wwise RoomVerb			
	1990128658	Room_Medium	Wwise RoomVerb			
	2038996943	Plate_Sizzle	Wwise RoomVerb			
	2171127646	Hall_Conversation	Wwise RoomVerb			
	2265839006	Not_A_Yellow_Submarine	Wwise RoomVerb			
	2513915904	Hall_Large	Wwise RoomVerb			
	2648499661	Hall_Medium_Dark	Wwise RoomVerb			
	2791153688	Hall_Small	Wwise RoomVerb			
	2849147824	Medium	Wwise RoomVerb			
	2891462749	Room_Medium_High_Absorbtion	Wwise RoomVerb			
	2900412812	Robotic_Trash	Wwise RoomVerb			
	2945072214	My_Closet	Wwise RoomVerb			
	2949243095	Hall_Small_Bright	Wwise RoomVerb			
	2995775460	Room_Small	Wwise RoomVerb			
	3036946058	Inside_Your_Head	Wwise RoomVerb			
	3164629247	Holy	Wwise RoomVerb			
	3216246379	Space_Dreaming	Wwise RoomVerb			
	3290519051	Aluminium_Tank	Wwise RoomVerb			
	3407225994	Phone_Booth	Wwise RoomVerb			
	3442768367	Hall_Large_Dark	Wwise RoomVerb			
	3451596814	Hall_Medium	Wwise RoomVerb			
	3559982155	Hall_Stop_Screaming	Wwise RoomVerb			
	3701691487	Hall_Large_Bright	Wwise RoomVerb			
	3900114836	Metal_Hangar	Wwise RoomVerb			
	4113267698	Hall_Guns	Wwise RoomVerb			

Plug-in Media	ID	Name	Source file	Type	Wwise Object Path	Notes	Data Size
	19306234	Wwise_Convolution_Reverb_(Custom)	D:\Documents\- ENJMIN\- PROJETS\H - Mini Projet\Repo\DeathStare\DeathStare_WwiseProject\.cache\Linux\Plugins\Wwise Convolution Reverb\Valley, Low, Forest 30m_8149B041.wem	Wwise Convolution Reverb (Custom)	\Master-Mixer Hierarchy\DS MIX\Master\Aux\RVB_Exterior\Wwise_Convolution_Reverb_(Custom)		400784
	25909241	Wwise_Convolution_Reverb_(Custom)	D:\Documents\- ENJMIN\- PROJETS\H - Mini Projet\Repo\DeathStare\DeathStare_WwiseProject\.cache\Linux\Plugins\Wwise Convolution Reverb\Chamber 3_515A5518.wem	Wwise Convolution Reverb (Custom)	\Master-Mixer Hierarchy\DS MIX\Master\Aux\RVB_Tunnel\Wwise_Convolution_Reverb_(Custom)		467120
	238217046	Wwise_Convolution_Reverb_(Custom)	D:\Documents\- ENJMIN\- PROJETS\H - Mini Projet\Repo\DeathStare\DeathStare_WwiseProject\.cache\Linux\Plugins\Wwise Convolution Reverb\Forest, Small Trees 20m_1D35CBEE.wem	Wwise Convolution Reverb (Custom)	\Master-Mixer Hierarchy\DS MIX\Master\Aux\RVB_Corridor\Wwise_Convolution_Reverb_(Custom)		1089856
	541288720	Wwise_Convolution_Reverb_(Custom)	D:\Documents\- ENJMIN\- PROJETS\H - Mini Projet\Repo\DeathStare\DeathStare_WwiseProject\.cache\Linux\Plugins\Wwise Convolution Reverb\Room 1_04211D92.wem	Wwise Convolution Reverb (Custom)	\Master-Mixer Hierarchy\DS MIX\Master\Aux\RVB_RoomSmall\Wwise_Convolution_Reverb_(Custom)		229488
	541288721	Wwise_Convolution_Reverb_(Custom)	D:\Documents\- ENJMIN\- PROJETS\H - Mini Projet\Repo\DeathStare\DeathStare_WwiseProject\.cache\Linux\Plugins\Wwise Convolution Reverb\Room 2_87FD2C49.wem	Wwise Convolution Reverb (Custom)	\Master-Mixer Hierarchy\DS MIX\Master\Aux\RVB_RoomMedium\Wwise_Convolution_Reverb_(Custom)		360592
	541288722	Wwise_Convolution_Reverb_(Custom)	D:\Documents\- ENJMIN\- PROJETS\H - Mini Projet\Repo\DeathStare\DeathStare_WwiseProject\.cache\Linux\Plugins\Wwise Convolution Reverb\Chamber 1_44F99621.wem	Wwise Convolution Reverb (Custom)	\Master-Mixer Hierarchy\DS MIX\Master\Aux\RVB_Cave\Wwise_Convolution_Reverb_(Custom)		393360

