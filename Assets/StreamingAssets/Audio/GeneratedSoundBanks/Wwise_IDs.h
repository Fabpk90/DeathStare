/////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Audiokinetic Wwise generated include file. Do not edit.
//
/////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef __WWISE_IDS_H__
#define __WWISE_IDS_H__

#include <AK/SoundEngine/Common/AkTypes.h>

namespace AK
{
    namespace EVENTS
    {
        static const AkUniqueID AMB_BED_SEA = 246007687U;
        static const AkUniqueID AMB_BED_WAVES = 2193181892U;
        static const AkUniqueID AMB_BED_WIND = 151163884U;
        static const AkUniqueID AMB_LOCAMB_SALOON_WIND = 1930531736U;
        static const AkUniqueID AMB_LOCAMB_SALOON_WOOD = 1409543105U;
        static const AkUniqueID AMB_LOCAMB_TEMPLE_STONE = 2895908810U;
        static const AkUniqueID AMB_LOCAMB_TEMPLE_TWINKLES = 478049284U;
        static const AkUniqueID AMB_LOCAMB_TEMPLE_VOICES = 3923815286U;
        static const AkUniqueID AMB_LOCAMB_TUNNEL_CRACKLES = 3729987826U;
        static const AkUniqueID AMB_LOCAMB_TUNNEL_WATER = 3235922133U;
        static const AkUniqueID AMB_LOCAMB_TUNNEL_WIND_01 = 1627070216U;
        static const AkUniqueID AMB_LOCAMB_TUNNEL_WIND_02 = 1627070219U;
        static const AkUniqueID AMB_LOCAMB_TUNNEL_WIND_03 = 1627070218U;
        static const AkUniqueID AMB_LOCAMB_TUNNEL_WOOD = 1019636375U;
        static const AkUniqueID AMB_MENU_BED = 2900892803U;
        static const AkUniqueID AMB_SP_FOLIAGE = 3964420051U;
        static const AkUniqueID AMB_SP_STONE = 2816467889U;
        static const AkUniqueID AMB_SP_TORCH = 1492342808U;
        static const AkUniqueID AMB_SP_WATER = 3897627331U;
        static const AkUniqueID AMB_SP_WOOD = 1104827821U;
        static const AkUniqueID AMB_SSHOTS_LONG = 416895869U;
        static const AkUniqueID AMB_SSHOTS_SHORTS = 1929281362U;
        static const AkUniqueID EFFECTS_CHAR_DAMAGE = 2897623584U;
        static const AkUniqueID EFFECTS_CHAR_DUEL = 1940990649U;
        static const AkUniqueID EFFECTS_CHAR_ENTERSTARING = 916024413U;
        static const AkUniqueID EFFECTS_CHAR_EXITSTARING = 3646545493U;
        static const AkUniqueID EFFECTS_CHAR_HITMARKER = 3848322560U;
        static const AkUniqueID EFFECTS_CHAR_MEDICKIT = 1330443465U;
        static const AkUniqueID EFFECTS_CHAR_OUTOFSTAMINA = 2487704977U;
        static const AkUniqueID EFFECTS_CHAR_REPULSE = 2782163419U;
        static const AkUniqueID EFFECTS_CHAR_RESPAWN = 3761455109U;
        static const AkUniqueID EFFECTS_CHAR_STAMINALOW = 2817885444U;
        static const AkUniqueID EFFECTS_CHAR_STARING = 4015725077U;
        static const AkUniqueID EFFECTS_CHAR_VISIBLE = 630278747U;
        static const AkUniqueID FOLEYS_CHAR_DEATH = 2031942103U;
        static const AkUniqueID FOLEYS_CHAR_JUMP_LANDING = 1903503235U;
        static const AkUniqueID FOLEYS_CHAR_JUMP_TAKEOFF = 567349118U;
        static const AkUniqueID FOLEYS_CHAR_RUN_FOOTSTEPS = 1039646526U;
        static const AkUniqueID FOLEYS_CHAR_RUN_PRESENCE = 2315528594U;
        static const AkUniqueID FOLEYS_CHAR_WALK_FOOTSTEPS = 2350611728U;
        static const AkUniqueID FOLEYS_CHAR_WALK_PRESENCE = 401042956U;
        static const AkUniqueID FOLEYS_PROPS_BELL = 1740293362U;
        static const AkUniqueID FOLEYS_PROPS_DOOR_WOOD_OPEN = 3849781330U;
        static const AkUniqueID FOLEYS_PROPS_THING_HIT = 199452813U;
        static const AkUniqueID PAUSE_ALL = 3864097025U;
        static const AkUniqueID POST_MUSIC_ALL = 1564958083U;
        static const AkUniqueID RESUME_ALL = 3679762312U;
        static const AkUniqueID SEGMENTS_DS_INTENSITY = 2001785879U;
        static const AkUniqueID SEGMENTS_DUELMUSIC = 1926112901U;
        static const AkUniqueID SEGMENTS_MAINMUSIC = 1704734930U;
        static const AkUniqueID SETSTATE_FIGHTSILENCE = 4049065654U;
        static const AkUniqueID STINGERS_DS_DON_L = 1290491939U;
        static const AkUniqueID STINGERS_DS_DON_R = 1290491965U;
        static const AkUniqueID STINGERS_DS_MARTA_L = 459653097U;
        static const AkUniqueID STINGERS_DS_MARTA_R = 459653111U;
        static const AkUniqueID STINGERS_DS_MEDUSA_L = 554142265U;
        static const AkUniqueID STINGERS_DS_MEDUSA_R = 554142247U;
        static const AkUniqueID STINGERS_DS_STAN_L = 2011547318U;
        static const AkUniqueID STINGERS_DS_STAN_R = 2011547304U;
        static const AkUniqueID STINGERS_KILL_P1 = 2299281131U;
        static const AkUniqueID STINGERS_KILL_P2 = 2299281128U;
        static const AkUniqueID STINGERS_KILL_P3 = 2299281129U;
        static const AkUniqueID STINGERS_KILL_P4 = 2299281134U;
        static const AkUniqueID STOP_ALL_SOFT = 59600196U;
        static const AkUniqueID STOP_AMB_MENU_BED = 3167003290U;
        static const AkUniqueID STOP_EFFECTS_CHAR_DAMAGE = 3978900703U;
        static const AkUniqueID STOP_EFFECTS_CHAR_STARING = 1985231276U;
        static const AkUniqueID UI_HUD_SCORE_UP = 3352394338U;
        static const AkUniqueID UI_MENU_CHARSELECT_CANCEL = 1494216063U;
        static const AkUniqueID UI_MENU_CHARSELECT_SELECT = 867372329U;
        static const AkUniqueID UI_MENU_CHARSELECT_SUBMITNEXT = 1019056678U;
        static const AkUniqueID UI_MENU_CHARSELECT_SUBMITPLAY = 3819465707U;
        static const AkUniqueID UI_MENU_CHARSELECT_SUBMITPREVIOUS = 1433958090U;
        static const AkUniqueID UI_MENU_CHARSELECT_SUBMITREADY = 1176590006U;
        static const AkUniqueID UI_MENU_MAIN_CANCEL = 547270322U;
        static const AkUniqueID UI_MENU_MAIN_SELECT = 3019098628U;
        static const AkUniqueID UI_MENU_MAIN_SUBMIT = 3256601260U;
        static const AkUniqueID UI_MENU_OPTIONS_CANCEL = 2984197605U;
        static const AkUniqueID UI_MENU_OPTIONS_SELECTBUTTON = 543023487U;
        static const AkUniqueID UI_MENU_OPTIONS_SELECTSLIDER = 4191133112U;
        static const AkUniqueID UI_MENU_OPTIONS_SUBMITBINDING = 657522650U;
        static const AkUniqueID UI_MENU_OPTIONS_SUBMITINVERT = 142641351U;
        static const AkUniqueID UI_MENU_OPTIONS_SUBMITSLIDER = 3201583160U;
        static const AkUniqueID UI_MENU_OPTIONS_SUBMITVALID = 3382532695U;
        static const AkUniqueID VO_CHAR_BARKS_DAMAGE = 3488381173U;
        static const AkUniqueID VO_CHAR_BARKS_DEATH = 3151326768U;
        static const AkUniqueID VO_CHAR_BARKS_JUMP = 3299551614U;
        static const AkUniqueID VO_CHAR_PUNCHLINE_KILL = 4274014911U;
        static const AkUniqueID VO_CHAR_PUNCHLINE_VICTORY = 4071363987U;
    } // namespace EVENTS

    namespace STATES
    {
        namespace STATE_GENERAL_BELLPLAYER
        {
            static const AkUniqueID GROUP = 98833232U;

            namespace STATE
            {
                static const AkUniqueID NONE = 748895195U;
                static const AkUniqueID PLAYER1 = 2188949039U;
                static const AkUniqueID PLAYER2 = 2188949036U;
                static const AkUniqueID PLAYER3 = 2188949037U;
                static const AkUniqueID PLAYER4 = 2188949034U;
            } // namespace STATE
        } // namespace STATE_GENERAL_BELLPLAYER

        namespace STATE_GENERAL_WINNER
        {
            static const AkUniqueID GROUP = 3974334009U;

            namespace STATE
            {
                static const AkUniqueID DON = 546651172U;
                static const AkUniqueID MARTA = 1358509506U;
                static const AkUniqueID MEDUSA = 2609729838U;
                static const AkUniqueID NONE = 748895195U;
                static const AkUniqueID STAN = 553997901U;
            } // namespace STATE
        } // namespace STATE_GENERAL_WINNER

        namespace STATE_MUSIC_DS_INTENSITY
        {
            static const AkUniqueID GROUP = 1702469306U;

            namespace STATE
            {
                static const AkUniqueID LOW = 545371365U;
                static const AkUniqueID MID = 1182670505U;
                static const AkUniqueID NONE = 748895195U;
            } // namespace STATE
        } // namespace STATE_MUSIC_DS_INTENSITY

        namespace STATE_MUSIC_DUELSTATE_J1
        {
            static const AkUniqueID GROUP = 2568415076U;

            namespace STATE
            {
                static const AkUniqueID FALSE = 2452206122U;
                static const AkUniqueID NONE = 748895195U;
                static const AkUniqueID TRUE = 3053630529U;
            } // namespace STATE
        } // namespace STATE_MUSIC_DUELSTATE_J1

        namespace STATE_MUSIC_DUELSTATE_J2
        {
            static const AkUniqueID GROUP = 2568415079U;

            namespace STATE
            {
                static const AkUniqueID FALSE = 2452206122U;
                static const AkUniqueID NONE = 748895195U;
                static const AkUniqueID TRUE = 3053630529U;
            } // namespace STATE
        } // namespace STATE_MUSIC_DUELSTATE_J2

        namespace STATE_MUSIC_DUELSTATE_J3
        {
            static const AkUniqueID GROUP = 2568415078U;

            namespace STATE
            {
                static const AkUniqueID FALSE = 2452206122U;
                static const AkUniqueID NONE = 748895195U;
                static const AkUniqueID TRUE = 3053630529U;
            } // namespace STATE
        } // namespace STATE_MUSIC_DUELSTATE_J3

        namespace STATE_MUSIC_DUELSTATE_J4
        {
            static const AkUniqueID GROUP = 2568415073U;

            namespace STATE
            {
                static const AkUniqueID FALSE = 2452206122U;
                static const AkUniqueID NONE = 748895195U;
                static const AkUniqueID TRUE = 3053630529U;
            } // namespace STATE
        } // namespace STATE_MUSIC_DUELSTATE_J4

        namespace STATE_MUSIC_MAIN
        {
            static const AkUniqueID GROUP = 1498827748U;

            namespace STATE
            {
                static const AkUniqueID FIGHT_END = 4258958663U;
                static const AkUniqueID FIGHT_INTRO = 1585161430U;
                static const AkUniqueID FIGHT_SILENCE = 3762960553U;
                static const AkUniqueID MAINMENU = 3604647259U;
                static const AkUniqueID MAINMENU_END = 63077693U;
                static const AkUniqueID NONE = 748895195U;
                static const AkUniqueID OUTRO = 4184794294U;
            } // namespace STATE
        } // namespace STATE_MUSIC_MAIN

    } // namespace STATES

    namespace SWITCHES
    {
        namespace SWITCHES_FOLEYS_SURFACES
        {
            static const AkUniqueID GROUP = 2089079731U;

            namespace SWITCH
            {
                static const AkUniqueID COBBLE = 3135525842U;
                static const AkUniqueID CONCRETE = 841620460U;
                static const AkUniqueID DIRT = 2195636714U;
                static const AkUniqueID GRASS = 4248645337U;
                static const AkUniqueID MIX = 1182670517U;
                static const AkUniqueID SAND = 803837735U;
                static const AkUniqueID WOOD = 2058049674U;
            } // namespace SWITCH
        } // namespace SWITCHES_FOLEYS_SURFACES

        namespace SWITCHES_FOLEYS_THINGS
        {
            static const AkUniqueID GROUP = 1092960256U;

            namespace SWITCH
            {
                static const AkUniqueID BOX = 546945280U;
            } // namespace SWITCH
        } // namespace SWITCHES_FOLEYS_THINGS

        namespace SWITCHES_GENERAL_CHARACTER
        {
            static const AkUniqueID GROUP = 3424733800U;

            namespace SWITCH
            {
                static const AkUniqueID DON = 546651172U;
                static const AkUniqueID MARTA = 1358509506U;
                static const AkUniqueID MEDUSA = 2609729838U;
                static const AkUniqueID STANISLAS = 1986576095U;
            } // namespace SWITCH
        } // namespace SWITCHES_GENERAL_CHARACTER

        namespace SWITCHES_GENERAL_PLAYER
        {
            static const AkUniqueID GROUP = 853932152U;

            namespace SWITCH
            {
                static const AkUniqueID P1 = 1635194252U;
                static const AkUniqueID P2 = 1635194255U;
                static const AkUniqueID P3 = 1635194254U;
                static const AkUniqueID P4 = 1635194249U;
            } // namespace SWITCH
        } // namespace SWITCHES_GENERAL_PLAYER

    } // namespace SWITCHES

    namespace GAME_PARAMETERS
    {
        static const AkUniqueID RTPC_AMBIANCES_VOLUME = 1676809991U;
        static const AkUniqueID RTPC_CHARACTER_HEALTH = 3664139661U;
        static const AkUniqueID RTPC_CHARACTER_STAMINA = 655017018U;
        static const AkUniqueID RTPC_DS_VOLUME = 3202337757U;
        static const AkUniqueID RTPC_EFFECTS_VOLUME = 579984592U;
        static const AkUniqueID RTPC_FOLEYS_VOLUME = 2776054680U;
        static const AkUniqueID RTPC_MASTER_VOLUME = 2564988978U;
        static const AkUniqueID RTPC_MUSIC_VOLUME = 1596647065U;
        static const AkUniqueID RTPC_PRIOR_METER_01_FAST = 3815619980U;
        static const AkUniqueID RTPC_PRIOR_METER_01_SLOW = 3762126541U;
        static const AkUniqueID RTPC_PRIOR_METER_02_FAST = 3834532369U;
        static const AkUniqueID RTPC_PRIOR_METER_02_SLOW = 2264627196U;
        static const AkUniqueID RTPC_PRIOR_METER_03_FAST = 888992730U;
        static const AkUniqueID RTPC_PRIOR_METER_03_SLOW = 3366444199U;
        static const AkUniqueID RTPC_PRIOR_METER_04_FAST = 2568571271U;
        static const AkUniqueID RTPC_PRIOR_METER_04_SLOW = 3334746462U;
        static const AkUniqueID RTPC_PRIOR_METER_05_FAST = 3928341384U;
        static const AkUniqueID RTPC_PRIOR_METER_05_SLOW = 3364075793U;
        static const AkUniqueID RTPC_STEREOWIDTH = 475975533U;
        static const AkUniqueID RTPC_UI_VOLUME = 1080068730U;
        static const AkUniqueID RTPC_VOICE_VOLUME = 1998977308U;
    } // namespace GAME_PARAMETERS

    namespace TRIGGERS
    {
        static const AkUniqueID TRIGGER_DS_DON_L = 2046853094U;
        static const AkUniqueID TRIGGER_DS_DON_R = 2046853112U;
        static const AkUniqueID TRIGGER_DS_MARTA_L = 2960638048U;
        static const AkUniqueID TRIGGER_DS_MARTA_R = 2960638078U;
        static const AkUniqueID TRIGGER_DS_MEDUSA_L = 4277786702U;
        static const AkUniqueID TRIGGER_DS_MEDUSA_R = 4277786704U;
        static const AkUniqueID TRIGGER_DS_STAN_L = 529325937U;
        static const AkUniqueID TRIGGER_DS_STAN_R = 529325935U;
        static const AkUniqueID TRIGGER_DUELMUSIC = 3444518989U;
        static const AkUniqueID TRIGGER_KILL_P1 = 2839118456U;
        static const AkUniqueID TRIGGER_KILL_P2 = 2839118459U;
        static const AkUniqueID TRIGGER_KILL_P3 = 2839118458U;
        static const AkUniqueID TRIGGER_KILL_P4 = 2839118461U;
    } // namespace TRIGGERS

    namespace BANKS
    {
        static const AkUniqueID INIT = 1355168291U;
        static const AkUniqueID AMB_FIGHT_SNB = 1428369830U;
        static const AkUniqueID AMB_MAIN_SNB = 1295132815U;
        static const AkUniqueID AMB_MENU_SNB = 2819711853U;
        static const AkUniqueID EFFECTS_FIGHT_SNB = 959317692U;
        static const AkUniqueID EFFECTS_MAIN_SNB = 4031105461U;
        static const AkUniqueID EFFECTS_MENU_SNB = 3414471363U;
        static const AkUniqueID FOLEYS_FIGHT_SNB = 199112724U;
        static const AkUniqueID FOLEYS_MAIN_SNB = 647620205U;
        static const AkUniqueID FOLEYS_MENU_SNB = 63598827U;
        static const AkUniqueID GENERAL_SNB = 456350585U;
        static const AkUniqueID MUSIC_FIGHT_SNB = 2377901711U;
        static const AkUniqueID MUSIC_MAIN_SNB = 2058607680U;
        static const AkUniqueID MUSIC_MENU_SNB = 2629380614U;
        static const AkUniqueID SFX_SNB = 26215100U;
        static const AkUniqueID UI_FIGHT_SNB = 3254103910U;
        static const AkUniqueID UI_MAIN_SNB = 1370904655U;
        static const AkUniqueID UI_MENU_SNB = 406961709U;
        static const AkUniqueID VOICE_FIGHT_SNB = 3761019124U;
        static const AkUniqueID VOICE_MAIN_SNB = 4029048141U;
        static const AkUniqueID VOICE_MENU_SNB = 4232638923U;
        static const AkUniqueID VOICE_SNB = 133924915U;
    } // namespace BANKS

    namespace BUSSES
    {
        static const AkUniqueID AUX = 983310469U;
        static const AkUniqueID MASTER = 4056684167U;
        static const AkUniqueID MASTER_AUDIO_BUS = 3803692087U;
        static const AkUniqueID MUSIC = 3991942870U;
        static const AkUniqueID MUSIC_INTRO = 984952705U;
        static const AkUniqueID MUSIC_MAINMENU = 599938019U;
        static const AkUniqueID MUSIC_OUTRO = 3191610174U;
        static const AkUniqueID MUSIC_PRIOR_02 = 3600354836U;
        static const AkUniqueID MUSIC_PRIOR_02_FAST = 220327233U;
        static const AkUniqueID MUSIC_PRIOR_02_SLOW = 851001452U;
        static const AkUniqueID MUSIC_PRIOR_03_FAST = 2359468234U;
        static const AkUniqueID MUSIC_PRIOR_03_SLOW = 1668764311U;
        static const AkUniqueID MUSIC_PRIOR_05_FAST = 3614830008U;
        static const AkUniqueID MUSIC_PRIOR_05_SLOW = 1934198145U;
        static const AkUniqueID PRIORAUX = 707518423U;
        static const AkUniqueID SFX = 393239870U;
        static const AkUniqueID SFX_AMB_PRIOR01 = 2864885079U;
        static const AkUniqueID SFX_AMB_PRIOR02 = 2864885076U;
        static const AkUniqueID SFX_AMBIANCES = 2383096572U;
        static const AkUniqueID SFX_EFFECT_PRIOR03_FAST = 1966498141U;
        static const AkUniqueID SFX_EFFECT_PRIOR03_SLOW = 158871344U;
        static const AkUniqueID SFX_EFFECT_PRIOR04_FAST = 933472508U;
        static const AkUniqueID SFX_EFFECTS = 2324388809U;
        static const AkUniqueID SFX_FOLEYS = 3183352817U;
        static const AkUniqueID SFX_FOLEYS_PRIOR01 = 660584193U;
        static const AkUniqueID SFX_FOLEYS_PRIOR02 = 660584194U;
        static const AkUniqueID SFX_FOLEYS_PRIOR03 = 660584195U;
        static const AkUniqueID SFX_UI = 3862737079U;
        static const AkUniqueID SFX_UI_PRIOR_04 = 3942676433U;
        static const AkUniqueID SFX_UI_PRIOR_05 = 3942676432U;
        static const AkUniqueID STINGERSDS = 1767886531U;
        static const AkUniqueID STINGERSDSL = 2765257877U;
        static const AkUniqueID STINGERSDSR = 2765257867U;
        static const AkUniqueID STINGERSKILLS = 2995587383U;
        static const AkUniqueID STINGERSKILLSL = 1258652153U;
        static const AkUniqueID STINGERSKILLSR = 1258652135U;
        static const AkUniqueID VO = 1534528548U;
        static const AkUniqueID VO_PRIOR04 = 1457905285U;
    } // namespace BUSSES

    namespace AUX_BUSSES
    {
        static const AkUniqueID PRIOR_01_FAST = 808284948U;
        static const AkUniqueID PRIOR_01_SLOW = 989439445U;
        static const AkUniqueID PRIOR_02_FAST = 1369990425U;
        static const AkUniqueID PRIOR_02_SLOW = 3797936356U;
        static const AkUniqueID PRIOR_03_FAST = 3139664290U;
        static const AkUniqueID PRIOR_03_SLOW = 2709619631U;
        static const AkUniqueID PRIOR_04_FAST = 491446543U;
        static const AkUniqueID PRIOR_04_SLOW = 444410022U;
        static const AkUniqueID PRIOR_05_FAST = 1834964976U;
        static const AkUniqueID PRIOR_05_SLOW = 4188538041U;
        static const AkUniqueID RVB_CAVE = 4001587643U;
        static const AkUniqueID RVB_CORRIDOR = 3148728066U;
        static const AkUniqueID RVB_EXTERIOR = 3801984506U;
        static const AkUniqueID RVB_ROOMMEDIUM = 226212192U;
        static const AkUniqueID RVB_ROOMSMALL = 429702506U;
        static const AkUniqueID RVB_TUNNEL = 1692131274U;
    } // namespace AUX_BUSSES

    namespace AUDIO_DEVICES
    {
        static const AkUniqueID NO_OUTPUT = 2317455096U;
        static const AkUniqueID SYSTEM = 3859886410U;
    } // namespace AUDIO_DEVICES

}// namespace AK

#endif // __WWISE_IDS_H__
